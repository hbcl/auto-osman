% Author: Lauro Ojeda, 2011-2015
restoredefaultpath
% addpath('C:\Users\osman\Desktop\pilotWalking\hbcl-imu-data-analysis-17bd9a609b8f\hbcl-imu-data-analysis-17bd9a609b8f\stride_estimation')
addpath('D:\spare\stochasticTerrainUnevenWalking\somedata')
addpath('D:\spare\SteppingOnFoamImu\stride_estimation')
% addpath('C:\Program Files\Motion Studio\workspace\MyProject')
addpath('D:\spare\stochasticTerrainUnevenWalking\somedata\pilotDarian2')

%% Two APDM IMUs data processing simultaneously
clear all; close all
% Load IMU information from a file
% [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_New_apdm('20180214-100302_osmanNewApdmPilot_sensor_1284_label_Left.h5','20180214-100304_osmanNewApdmPilot_sensor_1310_label_Right.h5');
[left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm('20180207-160206_darianPilotUneven2_sensor_779_label_Left.h5','20180207-160207_darianPilotUneven2_sensor_1027_label_Right.h5');


% filePath = '20180214-100302_osmanNewApdmPilot_sensor_1284_label_Left.h5';
% info = h5info(filePath);
% data = h5read(filePath, [info.Groups(2).Groups(1).Name,'/Accelerometer'])
% time = h5read(filePath, [info.Groups(2).Groups(1).Name,'/Time'])
% sampleRate = h5readatt(filePath, info.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
% figure; plot(data')
 
% fileFormat = h5readatt(name , '/', 'FileFormatVersion');
% 
% if fileFormat < 5
%     error('TruncateHDF only works on fileFormat versions 5+');
% end
% 
% sensors = h5info(name, '/Sensors')
% 
% acc = h5read(name, [sensors.Name '/Accelerometer'])
% 
% [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm('20171003-101327_pilotOsman3_sensor_779_label_Left.h5', '20171003-101330_pilotOsman3_sensor_1027_label_Right.h5');
% [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm('20171001-175513_pilotDarian2_sensor_779_label_Left.h5','20171001-175516_pilotDarian2_sensor_1027_label_Right.h5');
% [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm('20171003-174056_checkGravity_sensor_779_label_Left_Gravity.h5', '20171003-174058_checkGravity_sensor_1027_label_Right_Gravity.h5');

% load 20171001-175513_pilotDarian2_sensor_779_label_Leftnewmat
% % load 20170929-114350_pilotMichael1_sensor_1027_label_Leftnew2
% left_Wb = W; left_Ab = A;
% load 20171001-175516_pilotDarian2_sensor_1027_label_Rightnewmat
% % load 20170929-114344_pilotMichael1_sensor_779_label_Rightnew2
% right_Wb = W; right_Ab = A;
% t = (1:size(left_Wb,1))*PERIOD;
% plot(t, left_Wb)

% Plot signals to help determining the SECTION that corresponds to the experiment
% getdata(left_Wb,left_Ab,PERIOD);

% Define the section (in seconds) that will need to be processed and segment the IMU data accordingly
% michael: walking [900 1060], running [1050 1250], [1220 1340], [1340 1450]
% darian

SECTION = [290 350];
[left_Wb,left_Ab] = getdata(left_Wb,left_Ab,PERIOD,SECTION);
[right_Wb,right_Ab] = getdata(right_Wb,right_Ab,PERIOD,SECTION);

%%
% Process data from the two IMUs simultaneously 
[left_walk_info,right_walk_info] = compute_pos_two_imus(left_Wb,left_Ab,right_Wb,right_Ab,PERIOD);


% Perform inertial mechanization
% left_walk_info = compute_pos(left_Wb,left_Ab,PERIOD);
% right_walk_info = compute_pos(right_Wb,right_Ab,PERIOD);

% Segment steps
out_L = [];%[23 26]
out_R = out_L
left_strides = stride_segmentation(left_walk_info,PERIOD, out_L);
right_strides = stride_segmentation(right_walk_info,PERIOD, out_R);  

% Plot results for left and right foot
figure;
subplot(1,2,1);
plt_ltrl_frwd_strides(left_strides,0);
subplot(1,2,2);
plt_ltrl_frwd_strides(right_strides,0);
matchaxes;

figure;
subplot(2,1,1)
plt_frwd_elev_strides(left_strides,0);
subplot(2,1,2)
plt_frwd_elev_strides(right_strides,0);
matchaxes;

figure;
subplot(1,2,1)
plt_stride_var(left_strides,0);
subplot(1,2,2)
plt_stride_var(right_strides,0);
matchaxes;

