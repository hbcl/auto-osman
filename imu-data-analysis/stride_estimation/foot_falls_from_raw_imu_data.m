function [ foot_fall_frame_mask ] ... % Column vector of Booleans (n_frames)
  = foot_falls_from_raw_imu_data( ...
      sampling_period,               ... % Double, the IMU sampling period
      stationary_period_params,      ... % StationaryPeriodParams
      foot_fall_params,              ... % FootFallDetectionParams
      imu_local_angular_velocities,  ... % Matrix of Doubles (n_frames x 3)
      imu_local_linear_accelerations ... % Matrix of Doubles (n_frames x 3)
    )

% We want to partition the trace into time intervals, the boundaries of which
% should coincide with foot falls.

% This partitioning process begins by labelling each time point as either
% "stationary" or "non-stationary", where a point is stationary if the
% magnitudes of the angular velocity and linear acceleration at the time
% point are below certain thresholds (`foot_fall_angular_velocity_maximum`
% and `foot_fall_linear_acceleration_maximum`). This function expects these
% labels to be provided in the `stationary_frame_indices` variable.

% The trace is then divided at stationary points that are followed by a
% sufficient duration of non-stationary points
% (`non_foot_fall_period_minimum`), producing a partitioning into intervals.

% If the length of any of these intervals exceeds a threshold
% (`foot_fall_period_maximum`), they are subdivided into intervals shorter
% than the threshold, yielding the final partitioning.

  % Manual configuration
  PLOT_DETAILS = 1;

  % Unpack parameters
  min_non_foot_fall_duration_indices = ...
    floor(foot_fall_params.non_foot_fall_period_minimum / sampling_period);
  max_foot_fall_duration_indices = ...
    floor(foot_fall_params.foot_fall_period_maximum / sampling_period);

  % Define stationary periods
  [stationary_frame_indices, angular_velocity_magnitudes] = ...
    stationary_periods_from_raw_imu_data( ...
      sampling_period, ...
      stationary_period_params, ...
      imu_local_angular_velocities, ...
      imu_local_linear_accelerations);

  % Group together stationary frames separated by less than
  % `min_non_foot_fall_duration_indices` time points
  [ stationary_periods_start_frames, ...
    stationary_periods_end_frames ] ...
    = group_indices(min_non_foot_fall_duration_indices, ...
                    stationary_frame_indices);

  % Choose specific foot fall indices within stationary periods
  foot_fall_frame_indices = choose_foot_fall_frame_indices( ...
      angular_velocity_magnitudes, ...
      stationary_periods_start_frames, ...
      stationary_periods_end_frames, ...
      max_foot_fall_duration_indices ...
      );

  % Force the last sample to be a foot fall
  n_frames = size(imu_local_angular_velocities, 1);
  foot_fall_frame_indices = [foot_fall_frame_indices; n_frames];
    % TODO: What if the last sample is already a foot fall? Is that possible?

  % Convert frame indices 
  foot_fall_frame_mask = zeros(n_frames,1);
  foot_fall_frame_mask(foot_fall_frame_indices) = 1;
  foot_fall_frame_mask = foot_fall_frame_mask == 1;

end % function


% Group together indices separated by less than `spacing_threshold` indices,
% returning for each group an interval that spans the group.
function [interval_start_frames, ...
          interval_end_frames] ...
  = group_indices( ...
      spacing_threshold, ... % Int: the spacing between neighbouring indices below which the indices will be grouped together
      indices ... % Vector of Ints, sorted
      )

  diff_frame_indices = diff(indices);
  boundary_indices = find(diff_frame_indices > spacing_threshold);

  interval_start_frames = indices([1,                boundary_indices+1]);
  interval_end_frames   = indices([boundary_indices, size(indices, 2)]);
end % function


% Choose specific foot fall indices within the provided stationary periods.
function foot_fall_frame_indices ...
  = choose_foot_fall_frame_indices( ...
      angular_velocity_magnitudes, ...
      stationary_periods_start_frames, ...
      stationary_periods_end_frames, ...
      max_foot_fall_duration_indices ...
      )

  n_intervals = size(stationary_periods_start_frames,2);
  foot_fall_frame_indices = [];

  for i = 1:n_intervals
    start_frame = stationary_periods_start_frames(i);
    end_frame = stationary_periods_end_frames(i);

    % Segment long stationary periods into multiple strides, defining multiple
    % foot falls
    interval_length = end_frame - start_frame + 1;
    n_stationary_periods = ceil(interval_length/max_foot_fall_duration_indices);

    frame_boundaries = floor( ...
      linspace(start_frame, end_frame, n_stationary_periods+1) ...
      );
    intermediate_frames = frame_boundaries(2:n_stationary_periods);
    segment_start_frames = [ start_frame, intermediate_frames+1 ];
    segment_end_frames   = [ intermediate_frames, end_frame ];

    for stationary_period_no = 1:n_stationary_periods
      % Within each stationary period, choose the point of minimal angular
      % velocity as the foot fall index
      segment_angular_velocity_magnitudes = ...
        angular_velocity_magnitudes( ...
          segment_start_frames(stationary_period_no) : ...
          segment_end_frames(stationary_period_no));
      [~, segment_min_idx] = min(segment_angular_velocity_magnitudes);
      min_idx = segment_min_idx + ...
                segment_start_frames(stationary_period_no) - 1;
      foot_fall_frame_indices = [foot_fall_frame_indices; min_idx];
    end
  end
end % function
