
%this m file is to validate HS from Acc and HS from the treamill forces
% you tested yourself wuth 3 differnet speed
%the problem is the synrization do the same experiment again with tapping
%the IMU gently on the treadmill

clear all; clc
restoredefaultpath
% addpath('C:\Users\osman\Desktop\pilotWalking\hbcl-imu-data-analysis-17bd9a609b8f\hbcl-imu-data-analysis-17bd9a609b8f\stride_estimation')
addpath('D:\spare\SteppingOnFoamImu\stride_estimation')


addpath('C:\Users\osman\Desktop\desktop\Emg\emgApdm');
addpath('C:\Users\osman\Desktop\desktop\Emg\EmgForceData');
addpath('C:\Users\osman\Desktop\desktop\Emg\pliot4speedsOsman')
addpath(genpath('D:\spare\GaitAnalysisToolBox_Son'));

%% Two APDM IMUs data processing simultaneously
clear all; close all
% Load IMU information from a file
% [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm('20171020-144039_emgPilotOsman_sensor_779_label_Left.h5','20171020-144037_emgPilotOsman_sensor_1027_label_Right.h5');

load 20171020-144037_emgPilotOsman_sensor_1027_label_Rightnew2.mat;
right_Wb_big = W; right_Ab_big = A; clear A W;
load 20171020-144039_emgPilotOsman_sensor_779_label_Leftnew2
left_Wb_big = W; left_Ab_big = A;
clear W A

GRAVITY=9.80297286843

%%
SECTION = [6350 6500  ]; %5840 5960 6020 6140    6200 6330  6350 6500  
[left_Wb,left_Ab] = getdata(left_Wb_big,left_Ab_big,PERIOD,SECTION);
[right_Wb,right_Ab] = getdata(right_Wb_big,right_Ab_big,PERIOD,SECTION);


% time = 0:PERIOD:(length(right_Ab)-1)*PERIOD;
% figure; plot(time, right_Ab(:,3)); grid on


% 
%%
% Process data from the two IMUs simultaneously 
% [left_walk_info,right_walk_info] = compute_pos_two_imus(left_Wb,left_Ab,right_Wb,right_Ab,PERIOD);

% Perform sperately
left_walk_info = compute_pos(left_Wb,left_Ab,PERIOD);
right_walk_info = compute_pos(right_Wb,right_Ab,PERIOD);

%%
% Segment steps
% out_L = [];%[23 26]
% out_R = out_L
% left_strides = stride_segmentation(left_walk_info,PERIOD, out_L);
% right_strides = stride_segmentation(right_walk_info,PERIOD, out_R);  
% 
% % Plot results for left and right foot
% figure;
% subplot(1,2,1);
% plt_ltrl_frwd_strides(left_strides,0);
% subplot(1,2,2);
% plt_ltrl_frwd_strides(right_strides,0);
% matchaxes;
% 
% figure;
% subplot(2,1,1)
% plt_frwd_elev_strides(left_strides,0);
% subplot(2,1,2)
% plt_frwd_elev_strides(right_strides,0);
% matchaxes;
% 
% figure;
% subplot(1,2,1)
% plt_stride_var(left_strides,0);
% subplot(1,2,2)
% plt_stride_var(right_strides,0);
% matchaxes;
%end of APDM
%%
% load trial1_speed1_0_01;
load trial1_speed1_0_04
close all
cut_off_freq = 25;
Force_freq = 960;
[Bf, Af] = butter(3, 25*2/Force_freq);
N = 1000;
F_factor = diag([500 500 1000]);
M_factor = diag([800 250 400]);

i = 1
LGRF{i} = filtfilt(Bf,Af,samples(:,[1 2 3]))*F_factor;
RGRF{i} = filtfilt(Bf,Af,samples(:,[9 10 11]))*F_factor;
%correct the 2nd dim
RGRF{i}(:,2) = -RGRF{i}(:,2);
LGRF{i}(:,2) = -LGRF{i}(:,2);  
figure; plot(LGRF{i}, 'b');  hold on;  plot(RGRF{i}, 'r'); hold on;
dataLen = length(RGRF{i});

%event indices
[RHS{i} LHS{i} RTO{i} LTO{i}] = findEventIndicesForWalkingOnTreadmill(RGRF{i}, LGRF{i}, Force_freq); 

figure;
plot(RGRF{i},'r'); hold on;  plot(LGRF{i},'b');
plot(RHS{i}, RGRF{i}(RHS{i},3),'k*', 'markersize', 10);
plot(LHS{i}, LGRF{i}(LHS{i},3),'k*', 'markersize', 10); xlabel('filtered');

%find the offset and substract from forces
tempSize = length(RTO{i})-1
offsetRZ = []; offsetLZ = []; 
offsetRY = []; offsetLY = [];
offsetRX = []; offsetLX = [];
stepsTOofset = [ 2:4  tempSize-4:tempSize-1];
for j = 1 : tempSize
    tempR = [RTO{i}(j):RHS{i}(j+1)];
    tempL = [LTO{i}(j):LHS{i}(j+1)];
    plot(tempR,  RGRF{i}(tempR, 3),'g'); plot(tempL,  LGRF{i}(tempL, 3),'g');

    if find(j == stepsTOofset )
        offsetRZ = [offsetRZ; mean(RGRF{i}(tempR, 3))];
        offsetLZ = [offsetLZ; mean(LGRF{i}(tempL, 3))];

        offsetRY = [offsetRY; mean(RGRF{i}(tempR, 2))];
        offsetLY = [offsetLY; mean(LGRF{i}(tempL, 2))];

        offsetRX = [offsetRX; mean(RGRF{i}(tempR, 1))];
        offsetLX = [offsetLX; mean(LGRF{i}(tempL, 1))];
    end
end

RGRF_offset{i} = RGRF{i} - [mean(offsetRX)*ones(dataLen,1) mean(offsetRY)*ones(dataLen,1) mean(offsetRZ)*ones(dataLen,1)];
LGRF_offset{i} = LGRF{i} - [mean(offsetLX)*ones(dataLen,1) mean(offsetLY)*ones(dataLen,1) mean(offsetLZ)*ones(dataLen,1)];

figure;
plot(RGRF_offset{i},'r'); hold on;  plot(LGRF_offset{i},'b');
plot(RHS{i}, RGRF_offset{i}(RHS{i},3),'k*', 'markersize', 10);
plot(LHS{i}, LGRF_offset{i}(LHS{i},3),'k*', 'markersize', 10); xlabel('filtered offsetter');

figure; plot(RGRF_offset{i}(:,3),'r'); hold on; plot(LGRF_offset{i}(:,3),'b');   title('force 3d')
figure; plot(right_Ab(:,3),'r'); hold on; plot(left_Ab(:,3),'b'); title(' imu 3d')

%%
%*********************
peaksForce = find(RGRF_offset{i}(:,3) > 1500); %1700

% peaksForce = [6165 96810]

peaksAccPosR = find(right_Ab(:,3) > 50);
peaksAccPosL = find(left_Ab(:,3) > 50);
peaksAccNegR = find(right_Ab(:,3) < -50);
peaksAccNegL = find(left_Ab(:,3) < -50);
ind1 = peaksAccPosR(1);%peaksAccNegR(1);
ind2 = peaksAccNegL(1);% peaksAccNegL(2)

% ind1 = 6740;
% ind2 = 18560;% peaksAccNegL(2)
%*********************

right_Ab_new = right_Ab(ind1:ind2,:);
left_Ab_new =  left_Ab(ind1:ind2,:);

right_Wb_new = right_Wb(ind1:ind2,:);
left_Wb_new =  left_Wb(ind1:ind2,:);

RGRF_new{i} =  interpGaitCycle(RGRF_offset{i}(peaksForce(1):peaksForce(end),:), length(right_Ab_new));
LGRF_new{i} =  interpGaitCycle(LGRF_offset{i}(peaksForce(1):peaksForce(end),:), length(right_Ab_new));
figure;
plot(RGRF_new{i}(:,3),'r'); hold on; plot(LGRF_new{i}(:,3),'b'); 
plot(right_Ab_new(1:end,2)*10,'r'); hold on; plot(left_Ab_new(1:end,2)*10,'b');


left_Wb_new_Amp = (sum((left_Wb_new.^2)')).^.5*180/pi/PERIOD;
right_Wb_new_Amp = (sum((right_Wb_new.^2)')).^.5*180/pi/PERIOD;


right_Ab_new_Amp = (sum((right_Ab_new.^2)')).^.5 - GRAVITY;
left_Ab_new_Amp = (sum((left_Ab_new.^2)')).^.5 - GRAVITY;


figure; 
plot(right_Ab_new_Amp, 'r')

%%

%event indices new
[RHS_new{i} LHS_new{i} RTO_new{i} LTO_new{i}] = findEventIndicesForWalkingOnTreadmill(RGRF_new{i}, LGRF_new{i}, Force_freq); 
figNew = figure;
plot(RGRF_new{i}(:,3),'r'); hold on; plot(LGRF_new{i}(:,3),'b'); 
plot(RHS_new{i}, RGRF_new{i}(RHS_new{i},3),'g*', 'markersize', 10);
plot(LHS_new{i}, LGRF_new{i}(LHS_new{i},3),'g*', 'markersize', 10); 
plot(right_Ab_new(:,3)*10,'r'); hold on; plot(left_Ab_new(:,3)*10,'b');
% plot(RHS_new{i},right_Ab_new(RHS_new{i},3)*10,'g*'); hold on; plot(LHS_new{i}, left_Ab_new(LHS_new{i},3)*10,'g*');
% plot([0; diff(right_Ab_new(:,3))]*10,'k-'); hold on; plot([0; diff(left_Ab_new(:,3))]*10,'k-');

%filter the dim3 acc from IMU's
[Bf, Af] = butter(2, 5*2/(1/PERIOD));
right_Ab_newDim3_filt = filtfilt(Bf, Af, right_Ab_new(:,3));
left_Ab_newDim3_filt = filtfilt(Bf, Af, left_Ab_new(:,3));
plot(right_Ab_newDim3_filt*10,'r.'); hold on; plot(left_Ab_newDim3_filt*10,'b.');
[~,locsR] = findpeaks(right_Ab_newDim3_filt,'MinPeakHeight',0);
[~,locsL] = findpeaks(left_Ab_newDim3_filt,'MinPeakHeight',0);
plot(locsR, RGRF_new{i}(locsR,3),'kd'); hold on; plot(locsL, LGRF_new{i}(locsL,3),'kd');

% hgsave(gcf, 'IMU_Force_heelStrike');

%plot versus magnitude
figNew = figure;
plot(RGRF_new{i}(:,3),'r'); hold on; plot(LGRF_new{i}(:,3),'b'); 
plot(RHS_new{i}, RGRF_new{i}(RHS_new{i},3),'g*', 'markersize', 10);
plot(LHS_new{i}, LGRF_new{i}(LHS_new{i},3),'g*', 'markersize', 10); 
plot(right_Ab_new_Amp*10,'r'); hold on; plot(left_Ab_new_Amp*10,'b');



%should be the zero crossing after the greast peaks
% plot([0; diff(right_Ab_newDim3_filt)]*(:,3)10,'k-'); hold on; plot([0; diff(left_Ab_newDim3_filt)]*10,'k-');

% Perform sperately
% left_walk_info = compute_pos(left_Wb_new,left_Ab_new, PERIOD);
% right_walk_info = compute_pos(right_Wb_new,right_Ab_new, PERIOD);
% FF_walkingL = find(left_walk_info.FF_walking == 1);
% FF_walkingR = find(right_walk_info.FF_walking == 1);
% figure(figNew)
% plot(FF_walkingL, zeros(length(FF_walkingL),1),'b*', 'markersize', 10); 
% plot(FF_walkingR, zeros(length(FF_walkingR),1),'r*', 'markersize', 10);

% figure;
% plot(right_Ab_new(:,3),'r'); hold on; plot(left_Ab_new(:,3),'b');
% plot(FF_walkingL, zeros(length(FF_walkingL),1),'b*', 'markersize', 10); 
% plot(LHS_new{i},  zeros(length(LHS_new{i}),1),'bo', 'markersize', 10);
% plot(FF_walkingR, zeros(length(FF_walkingR),1),'r*', 'markersize', 10);
% plot(RHS_new{i},  zeros(length(RHS_new{i}),1),'ro', 'markersize', 10);
%% process DELSYS IMU's on the feet
close all
filename = 'Run_number_14_Plot_and_Store_Rep_7.15.csv'

allDelsysData  = csvread(filename, 1, 0); 
sizeData = size(allDelsysData); rowSize = sizeData(1); colSize = sizeData(2); 
timeEMG = allDelsysData(:,1); 
R_TA = allDelsysData(:,62); indexSignal = find(R_TA ~=0); %RTA RMV LTA LVM 2 22 42 62
sizeApdm = size(left_Ab(:,3))
R_TA = R_TA(indexSignal); sizeSignal = length(R_TA)

%%
close all
EMG = R_TA;
EMGraw = EMG;
figure; plot(EMG, 'r');

% Remove baseline offset
baseline_offset = mean(EMG(1:2000,:));%<------------
EMG = EMG - baseline_offset;
figure; plot(EMG, 'r');

% f_cutoff = 20; N = 4;
% Wn = [2*f_cutoff/Fs];
% [B,A] = butter(N, Wn,'high');
% EMG_filt1 = filtfilt(B, A, EMG);
% figure; plot(EMG_filt1, 'r');

% Making entire signal positive
EMG = abs(EMG);
Fs = 1/0.0009;
figure; hold on 
p1 = plot(EMG, 'r');

window_time = 110;%ms
% Calculating the moving average and filtering data
window_length = round(window_time/1000*Fs)
EMG_movmean = movmean(EMG, [window_length/2 window_length/2]);
p2 = plot(EMG_movmean, 'g');
legend([p1 p2], 'EMG', 'EMG_movmean')

f_cutoff = 3; N = 2;
Wn = [2*f_cutoff/Fs];
[B,A] = butter(N, Wn,'low');
EMG_filt = filtfilt(B, A, EMG);
EMG_filt2 = filtfilt(B, A, EMG_movmean);


figure; hold on
p1 = plot(EMG_movmean , 'g');
p2 = plot(EMG_filt2, 'r');
legend([p1 p2], 'EMG_movmean', 'EMG_movmean_filtered')


figure; hold on
p1 = plot(EMG_filt , 'g');
p2 = plot(EMG_movmean, 'r');
legend([p1 p2], 'EMG_filt', 'EMG_movmean')


figure; hold on
p1 = plot(EMG_filt*1e+6,'r');
p2 = plot(EMG_filt2*1e+6,'g');
legend([p1 p2], 'EMG_filt', 'EMG_movmean_filtered')
%%
% Normalize EMG to max signal
% EMG_norm = EMG_filt(:,m)/EMG_MVCmax;
    close all  
new_left_ab = left_Ab(7000:18400,3);
new_left_ab = interpGaitCycle(new_left_ab, sizeSignal); 

figure; hold on;
plot(EMG_filt*5e+5,'r');
plot(new_left_ab,'b')

[acor,lag] = xcorr(EMG_filt, new_left_ab);
[~,I] = max(abs(acor));
lagDiff = lag(I)

figure; hold on;
plot(EMG_filt(1:end)*5e+5,'r');
plot(new_left_ab(-lagDiff:end),'b')
