Auto Osman
==========

A GUI for parsing and labeling sections and trials of IMU data.

*Works with Matlab version 2016a and later*

Tutorial
--------

This tutorial will walk through using the GUI for parsing IMU data.
It will use some sample IMU data files to demonstrate the features of the
software.
These sample files are included in the repository at `auto-osman/sample_data`.

First, obtain and run the software.

-   Clone the repository from Bitbucket.
-   Open the repository in MATLAB.
-   Open and run `UI/Imu_SectionParser.m`.
    This should open a GUI interface titled "Figure 1: Data Parser".

Then, import some IMU data.
**Note: In the following, it is currently required that at least two IMUs be
imported. This will be fixed "soon".**

-   Click the "Open from .h5" button in the bottom left corner of the window.
-   This should open a new window titled "Select Imus to Parse"
-   Import and label an IMU data file:
     - Type "Left Foot" in the text box -- this is the label
     - Click on the "\<File\>" button to the right of the text box.
       A new window titled "Select File to Open" will be opened.
     - Navigate to `auto-osman/sample_data` and select the file `left.h5`.
     - Click the "Add imu" button.
-   Repeat the last step for each of the following (label, file) pairs:
     - ("Right Foot", `auto-osman/sample_data/right.h5`)
     - ("Hand",       `auto-osman/sample_data/hand.h5` )
     - ("Waist",      `auto-osman/sample_data/waist.h5`)
-   Click the "OK" Button.
    The program will now plot the data from the IMUs in tabs with the labels we
    have given them above.

We can save metadata for the project.

-   In the "Project Name" field in the bottom of the window, enter "Sample project".
-   In the "Date" field, enter today's date.
-   In the "Description field", enter "testing".
-   Click the "Save Project" button in the bottom left section of window.
    This will open a new window.
    Choose a place to save the project and click "Save".

We can also focus on sections of the data for analysis.

-   Click the "Select Data" button in the bottom left section of the window.
-   Click and drag over a section of the plotted data.
    The section you have highlighted will change colour. 
-   Click on the tab titled "Section Information" in the bottom part of the window.
-   Enter "Test Section" in the section name field.
-   Click the "Save Changes" button in the bottom right of the window.

Let's perform the analysis for one section.

-   Click the "Edit Section" button in the bottom right of the window.
    This will open a new window titled "Figure 2: Trial Parser" that displays
    only the data from the section you had previously selected.
-   Click the "Auto Trial Detect" button in the bottom left corner of the window.
    This will detect the trials and highlight them. 
-   Select one of the trials by clicking on one of the highlighted areas in the graph.
-   For illustrative purposes,
    delete this section by clicking the "Delete Trial" button.
    It will no longer be highlighted.
-   Select another highlighted area and change the "Trial Name" field.
-   Click the "Save Changes" button in the bottom left of the window.
-   Close the "Trial Parser" window.
-   To confirm that your changes to the section were saved,
    click the "Edit Section" button again.
    When you have confirmed the changes, close the "Trial Parser" window.

We can confirm that the project is properly saved.

-   Click the "Save Changes" button in the bottom right section of the window
-   Click the "Save Project" button in the bottom left section the window.
-   Close the "Figure 1: Data Parser" window.
    The project is now closed. 
-   Run the `UI/Imu_SectionParser.m` file again
    to confirm that your changes to the project were saved.
-   Click the "Load from .mat file" button in the bottom left of the window.
    This will open a new window allowing you to select a `.mat` file to open.
    Navigate to the folder you saved the project file in and open the saved
    project file. (Optional)

Running Osman's analysis
------------------------

In order to run Osmans analysis on the output click the "Export for Osman" button.
This will open a dialog:

-   For each section, select the type of trial.
-   For each IMU type (i.e. left foot, right foot, hand, waist),
    select the label of corresponding IMU.
-   Click OK.
-   Select a place to save the output. Make a note of this location as you will have to import it into Osmans code.

Note that all of the parsing data (i.e. sections, trials, names, notes, etc.)
is saved in the project info variable for use by the IMU analyzing code.

(WIP) To run ... something:

-   Comment out all of the lines in Osmans code before analysis (about line 244) except the lines that import the path.
-   include these two lines at the beginning of the analysis section
    load('\<path to saved file\>', 'data', 'imuData');
    load PERIOD;
-   Run analysis
Run Imu_SectionParser.m 

Lab Components Used
-------------------
Components used in this program from other bitbucket reposistories
- read-imu-data
  - https://bitbucket.org/hbcl/read-imu-data/
- imu-data-analysis
  -  https://bitbucket.org/hbcl/imu-data-analysis/

Both of these repositories are imported into this project as git subtrees. Follow the below intstructions to update the respective subtrees if the source directories have been changed.

1. read-imu-data
    - git subtree pull --prefix read-imu-data https://bitbucket.org/hbcl/read-imu-data/src/master master --squash
2. imu-data-analysis
    - git subtree pull --prefix imu-data-analysis https://bitbucket.org/hbcl/imu-data-analysis/src/master master --squash
  