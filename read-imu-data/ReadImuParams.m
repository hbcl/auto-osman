classdef ReadImuParams
  % Parameters for the definition of stationary periods (that is, periods of
  % low movement) in raw IMU data.

  properties
    % Gravity value for normalizing accelerometer during static time
    GRAVITY; % deg/s
    % Maximum static rate [deg/se]
    MAXIMUM_STATIC_RATE;
    % Boolean to sync imu data or not
    SYNC;
    % Value to shift the start time to sync imu indices, value chosen automatically if set to 0
    FORCE_SYNC_VALUE;
  end

  methods
    function obj = ReadImuParams(obj)
      obj.GRAVITY = 30.0;
      obj.MAXIMUM_STATIC_RATE = 2*pi/180;
      obj.SYNC = 1;
      obj.FORCE_SYNC_VALUE = 0;
    end
  end
end
