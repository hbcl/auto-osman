function [W, A, M] = crop(crop_interval, W, A, M)
% A convenience function for cropping data from an IMU.

% crop_interval: 1x2 array of integers, representing the endpoints of an
% interval of indices.

% Eliminate out-of-bounds indices
start_index = max(crop_interval(1), 1);
end_index   = min(crop_interval(2), size(W, 1));

W = W(start_index:end_index, :);
A = A(start_index:end_index, :);
if(exist('M','var'))
    M = M(start_index:end_index, :);
end;

end