function analysis_two(imu, imuChoices, SECTION, imuAnalysisParameters)

    addpath('../imu-data-analysis/stride_estimation');
[left_Wb,left_Ab] = slicedata(imu.Wb{imuChoices{1}}, imu.Ab{imuChoices{1}}, imu.SamplePeriod, SECTION);

% Load IMU information from the right foot
% [Wb,Ab,PERIOD] = getdata_apdm('20120418-132857_sensor_data_monitor_403_label_Right.h5');
[right_Wb,right_Ab] = slicedata(imu.Wb{imuChoices{2}},imu.Ab{imuChoices{2}}, imu.SamplePeriod, SECTION);

% Perform inertial mechanization for both feet independently
[left_walk_info, right_walk_info] = ...
  integrate_two_foot_imus( ...
    left_Wb, left_Ab, right_Wb, right_Ab, imu.SamplePeriod, ...
    imuAnalysisParameters.stationary_period_params, ...
    imuAnalysisParameters.foot_fall_detection_params, ...
    imuAnalysisParameters.foot_fall_opposite_velocity_detection_params, ...
    imuAnalysisParameters.tilt_correction_kalman_filter_params);

% Segment strides only for the walking period
left_strides = stride_segmentation( ...
    left_walk_info, imu.SamplePeriod, ...
    imuAnalysisParameters.step_filter_params);
right_strides = stride_segmentation( ...
    right_walk_info, imu.SamplePeriod, ...
    imuAnalysisParameters.step_filter_params);

% Plot results for left and right foot
figure;
subplot(1,2,1);
plt_ltrl_frwd_strides(left_strides, imuAnalysisParameters.plot_params, 0);
subplot(1,2,2);
plt_ltrl_frwd_strides(right_strides, imuAnalysisParameters.plot_params, 0);
matchaxes;

figure;
subplot(2,1,1)
plt_frwd_elev_strides(left_strides, imuAnalysisParameters.plot_params, 0);
subplot(2,1,2)
plt_frwd_elev_strides(right_strides, imuAnalysisParameters.plot_params, 0);
matchaxes;

figure;
subplot(1,2,1)
plt_stride_var(left_strides, imuAnalysisParameters.plot_params, 0);
subplot(1,2,2)
plt_stride_var(right_strides, imuAnalysisParameters.plot_params, 0);
matchaxes;


end
