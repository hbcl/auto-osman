% Author: Lauro Ojeda, 2011-2015
function plt_fwrd_elev_strides(strides, plot_params, CREATE_NEW_FIGURE)
  SHOW_ENDPOINTS = plot_params.show_endpoints;
  SHOW_NUMBER = plot_params.show_number;
  QUICK_PLOT = plot_params.quick_plot;
  ASSUME_FLAT_SURFACE = plot_params.assume_flat_surface;

  if(~exist('CREATE_NEW_FIGURE','var') | CREATE_NEW_FIGURE==1)
    figure;
  end;

  hold on;

    if QUICK_PLOT
      plot(strides.frwd, strides.elev);
      if SHOW_ENDPOINTS
        plot( strides.frwd(end, :), strides.elev(end, :), 'ko', ...
              'MarkerEdgeColor', 'k', 'MarkerSize', 5);
      end

    else
      n_steps = size(strides.ltrl, 2);
      colors = jet(n_steps);

      for step_no = 1:n_steps
        if ASSUME_FLAT_SURFACE
          % Set the change in elevation over each step to zero, where it
          % is assumed that the elevation trajectory of the step begins
          % at the origin.
          elev = strides.elev(:, step_no);

          % Find last index --
          % this relies on the particular construction of `elev`,
          % in which the last index is repeated to fill the array
          NN = find(diff(elev), 1, 'last');

          elev = elev(1:NN);
          err = elev(end);
          elev = elev - (1:NN)'/NN*err;
          elev(NN+1:length(strides.elev(:, step_no))) = 0;
        else
          elev = strides.elev(:, step_no);
        end

        plot( strides.frwd(:, step_no), -elev, ...
              'Color', colors(step_no, :));

        if SHOW_ENDPOINTS
          plot( strides.frwd(end, step_no), -elev(end), 'ko', ...
                'MarkerEdgeColor', 'k', ...
                'MarkerFaceColor', 'k', ...
                'MarkerSize', 5);
        end;

        if SHOW_NUMBER
          text( strides.frwd(end, step_no) + .001, ...
                -elev(end), ...
                num2str(step_no));
        end;
      end;
    end;

    grid on;
    ylabel('Elevation [m]');
    xlabel('Forward [m]');

  hold off;
end
