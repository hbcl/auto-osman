function deleteGroup2(data, groupNumber, targetDirect)


numbers = data.groupIndices{groupNumber};
for j = 1: length(numbers)
    if ~isempty(find(numbers(j) == data.savedTrials, 1))
        data.savedTrials(find(numbers(j) == data.savedTrials, 1)) = [];
    end
    data.(data.trialTypeString{numbers(j)}).trial = [];
end

currentFolder = cd;
cd(targetDirect);   
save(strcat(data.name,'_stochasticSegmented'),'data'); 
cd(currentFolder);  