function [dataOut] = plotAvgNew(varargin)

% the output used to be dataOut now it is dataOut2

%plotAvgofAvgs_v2
%WITH RESPECT TO STEPS
foamAvgLocation = [];
onFoamAvgIndex = [];
onFoamTime = [];
plotVersusDistance = 1;
plotVersusTime = 1;
plottingMain = 0;
MS = 15;
LW = 2;

dataOut = [];

opt_argin = varargin(1:end-1);
while length(opt_argin) >= 2,
    opt = opt_argin{1};
    val = opt_argin{2};
    opt_argin = opt_argin(3:end);
    switch opt
%         case 'adjrange'
%           adjrange = val;
%         case 'foamThickness'
%           foamThickness = val; %thick5inch
        case 'trialType'
          trialType = val;
        otherwise
          error('options error')
    end  
end

normalDataFlag = 0;
tempData = varargin(end);
% if length(varargin) == 8 %there is pacer data
%     normalDataFlag = 1;
%     tempData2 = varargin(8);
% end

for i = 1:length(tempData{1})  
    dataTS{i} = tempData{1}{i}; 
    if normalDataFlag
        dataPacer{i} = tempData2{1}{i}; 
    end
end

% this added later so there is no catula pacer, pcardata will be normal walk
normalDataFlag = 0;
normalDataFlagPlotNormal = 0;


if plottingMain
    colors = [1 0 0; 0 0 0; 0 0 1; 0 1 1; 1 0 1];
else
    colors = jet(length(dataTS));
end
 
marg_h = [0.04 0.015]; marg_w = [0.05 0.005];
% fig1 = tight_subplot(2,1,[.04 .05], marg_h, marg_w);

figDistance = figure;
figRealTime = figure;
FZ = 18; yTemp1 = 1.35;




%1) for distance and real time shift indices first

%2) For real time shifting indices means coinciding the real on foam times
%not to shift indicies to get the mean, therefore shift realtime indices by
%putting the aprropriate values to the begining as below

%3) for real time get the shortest real time and trim speed and FLL if
%necessary this is like if there are 3 trials like 21.2 23 22.5 secs get the 21.2
%one

%4) finally among the data get the shortest data amount and trim to get the
% mean

%5) plot




for i = 1:length(dataTS)  
%     foamAvgLocation = [foamAvgLocation  dataTS{i}.(adjrange).(foamThickness).(trialType).foamAvgLocation];
    foamAvgLocation = [foamAvgLocation  0];
    onFoamAvgIndex = [onFoamAvgIndex  dataTS{i}.(trialType).onFoamAvgIndex];
%     onFoamTime = [onFoamTime forwardBodyTime{i}(onFoamAvgIndex{i})];
    
%     tempString = adjrange;
%     trialDistance(i) = str2double(tempString(strfind(tempString,'j')+1:strfind(tempString,'f')-1))/2*0.3048; % meters  
    
    forwardBodySpeed{i} = dataTS{i}.(trialType).forwardBodySpeedMean;
    forwardBodyDistance{i} = dataTS{i}.(trialType).forwardBodyDistanceMean;
    
    meanRealTime(i) = dataTS{i}.(trialType).meanRealTime;
    meanFLL{i} = dataTS{i}.(trialType).meanFLL;
    meanforwardBodySpeedOrg{i} = dataTS{i}.(trialType).meanforwardBodySpeedOrgAll;
    onFoamRealtime(i) = dataTS{i}.(trialType).onFoamRealtime ;   
    
    
    if normalDataFlag          
%         Normal_FLL{i} = dataPacer{i}.(adjrange).(foamThickness).(strcat('Normal_', trialType)).meanFLL;
%         Normal_forwardBodySpeed{i} = dataPacer{i}.(adjrange).(foamThickness).(strcat('Normal_', trialType)).meanforwardBodySpeedOrgAll;    
        
        Normal_FLL{i} = dataTS{i}.no_adj.thick0inch.Normal_NoConstraint.meanFLL;
        Normal_forwardBodySpeed{i} = dataTS{i}.no_adj.thick0inch.Normal_NoConstraint.meanforwardBodySpeedOrgAll;  
    end
end


load PERIOD;
for i = 1:length(dataTS)
     realTrialTime{i} = (PERIOD:PERIOD:meanRealTime(i));
     onFoamRealTimeIndex(i) = find(onFoamRealtime(i) == realTrialTime{i});
end


shiftRealTimeIndex = max(onFoamRealTimeIndex) - onFoamRealTimeIndex;
shiftIndices = max(onFoamAvgIndex)-onFoamAvgIndex; 
% shiftTime = max(onFoamTime)- onFoamTime;
shiftDistance = max(foamAvgLocation)- foamAvgLocation;

%FIRST shift the incidices then shift the magnitude
%shifting indices is equired because you'll find means. Shifting time and
%distance (not velocity) are required, to align

lenFLLs = [];
for i = 1:length(dataTS)    
    %shift the final time and FLLs
    meanRealTime(i) = shiftRealTimeIndex(i)*PERIOD + meanRealTime(i);
    meanFLL{i} = shiftRealTimeIndex(i) + meanFLL{i};
    if normalDataFlag
        Normal_FLL{i} = shiftRealTimeIndex(i) + Normal_FLL{i};
    end
       
    %shift indices and then distance
    forwardBodySpeed{i} = [forwardBodySpeed{i}(1)*ones(shiftIndices(i),1); forwardBodySpeed{i}];
%         forwardBodyTime{i} =  [forwardBodyTime{i}(1)*ones(shiftIndices(i),1);   forwardBodyTime{i}];
    forwardBodyDistance{i} = [forwardBodyDistance{i}(1)* ones(shiftIndices(i),1); forwardBodyDistance{i}];

    %now you can shift the time and distance
%     forwardBodyTime{i} =  shiftTime(i) + forwardBodyTime{i};
    forwardBodyDistance{i} = shiftDistance(i) + forwardBodyDistance{i};
    
    
   tempLenFLL = length(meanFLL{i});
    %keep this data to shift FLLs later
    lenFLLs = [lenFLLs tempLenFLL];
    lenOnFoamFLL(i) = find((onFoamRealTimeIndex(i)+shiftRealTimeIndex(i)) == meanFLL{i}); %remember to shift the onFoamRealTimeIndex while finding related FLL
end


%real time data shifted in time which means time maginitude, but not indices
%so you need the shift indices, before shifting find appropriate values for
%the speed and FLL data
maxlenOnFoamFLL = max(lenOnFoamFLL);
for i = 1:length(dataTS) 
    meanDiffFLL(i) = mean(diff(meanFLL{i},1));
    meanInitSpeed(i) =  mean(meanforwardBodySpeedOrg{i}(1:2,:))
    shifFLLs(i) = maxlenOnFoamFLL  - lenOnFoamFLL(i); 
    
    if normalDataFlag
        Normal_meanDiffFLL(i) = mean(diff(Normal_FLL{i},1));
        Normal_meanInitSpeed(i) =  mean(Normal_forwardBodySpeed{i}(1:2,:));
    end
    
end
%shift
for i = 1:length(dataTS) 
    for k = 1:shifFLLs(i)
        meanFLL{i} = [meanFLL{i}(1)-round(meanDiffFLL(i));  meanFLL{i}];
        meanforwardBodySpeedOrg{i} = [meanInitSpeed(i);  meanforwardBodySpeedOrg{i}];
        
        
         if normalDataFlag
                Normal_FLL{i} = [Normal_FLL{i}(1)-round(Normal_meanDiffFLL(i));  Normal_FLL{i}];
                Normal_forwardBodySpeed{i} = [Normal_meanInitSpeed(i);  Normal_forwardBodySpeed{i}];    
         end
    end
end


%get the minimum real time and eliminate speed and/or meanFLLs if necessary
realTime = PERIOD:PERIOD:min(meanRealTime);
%trim FLLs and than speeed based on shortest real time
for i = 1:length(dataTS) 
    meanFLL{i} = meanFLL{i}(meanFLL{i} < length(realTime));
    meanforwardBodySpeedOrg{i} = meanforwardBodySpeedOrg{i}(1:length(meanFLL{i}));
  
    if normalDataFlag
        Normal_FLL{i} = Normal_FLL{i}(Normal_FLL{i} < length(realTime));
        Normal_forwardBodySpeed{i} = Normal_forwardBodySpeed{i}(1:length(Normal_FLL{i}));
    end
end


%find the lens to trim the indices
forwardBodySpeedAll = []; forwardBodyTimeAll =  []; forwardBodyDistanceAll = [];  meanforwardBodySpeedOrgAll = [];
dataLength = []; dataLengthRealTime = []; meanFLLAll = []; realTimeLens = []; dataLengthNormalRealTime = []; Normal_FLLAll = [];
Normal_forwardBodySpeedAll = [];
for i = 1:length(dataTS)
    dataLength = [dataLength length(forwardBodySpeed{i})];  
    dataLengthRealTime = [dataLengthRealTime length(meanforwardBodySpeedOrg{i})];
    
    if normalDataFlag
        dataLengthNormalRealTime = [dataLengthNormalRealTime length(Normal_forwardBodySpeed{i})];
    end
end

minDatalength = min(dataLength);
minDatalengthRealTime = min(dataLengthRealTime);
if normalDataFlag
    minDataLengthNormalRealTime = min(dataLengthNormalRealTime);
end


%trim indices
onFoamStride = max(onFoamAvgIndex); %since all of them are alligned on foam stride is same for each one
for i = 1:length(dataTS) 
    trim_end = dataLength(i)-minDatalength; trim_begin = 1;
    trim_end_realtime = dataLengthRealTime(i) - minDatalengthRealTime;
    if normalDataFlag
        trim_end_realtimeNormal = dataLengthNormalRealTime(i) - minDataLengthNormalRealTime;
    end
 
    forwardBodySpeedAll =  [forwardBodySpeedAll forwardBodySpeed{i}(trim_begin:end-trim_end)];
%     forwardBodyTimeAll = [forwardBodyTimeAll forwardBodyTime{i}(trim_begin:end-trim_end)]; 
    forwardBodyDistanceAll = [forwardBodyDistanceAll forwardBodyDistance{i}(trim_begin:end-trim_end)];    
      
    meanforwardBodySpeedOrgAll = [meanforwardBodySpeedOrgAll  meanforwardBodySpeedOrg{i}(trim_begin:end-trim_end_realtime)] ;
    meanFLLAll = [meanFLLAll meanFLL{i}(trim_begin:end-trim_end_realtime)];
    
    if normalDataFlag
        Normal_forwardBodySpeedAll = [Normal_forwardBodySpeedAll  Normal_forwardBodySpeed{i}(trim_begin:end-trim_end_realtimeNormal)] ;
        Normal_FLLAll = [Normal_FLLAll Normal_FLL{i}(trim_begin:end-trim_end_realtimeNormal)];
    end
    
    
    %for plotting versus steps find on foam step and tarp end and begining 
%     steps = 1:length(forwardBodyDistanceAll(:,i));  

%     if ~noPacerFlag
%         b{i} = find(forwardBodyDistanceAll(:,i) >= max(foamAvgLocation)+trialDistance(i));  
%         c{i} = find(forwardBodyDistanceAll(:,i) < max(foamAvgLocation)-trialDistance(i)); 
% 
%         if (tarpLenght(i) == 8) && (i == 1) %this is a matlab error
%             onFoamStride = 33;
%             b{i} = find(forwardBodyDistanceAll(:,i) >=  max(foamAvgLocation)+trialDistance(i));  
%             c{i} = find(forwardBodyDistanceAll(:,i) < max(foamAvgLocation)-trialDistance(i)); 
%         end
%     end
end







%now plot
for i = 1:length(dataTS)   

    figure(figDistance);%axes(fig1(1)); 
    plot(forwardBodyDistanceAll(:,i)- max(foamAvgLocation), forwardBodySpeedAll(:,i),'Color',colors(i,:),'linewidth', LW); hold on; ylim([1.1 1.8]);
    plot(forwardBodyDistanceAll(:,i)- max(foamAvgLocation), forwardBodySpeedAll(:,i),'.','Color',colors(i,:),'markersize', MS); 
        
%         plot([0  0], ylim,'k-','linewidth',2);
%         plot([0-trialDistance(i) 0-trialDistance(i)], ylim,'-.','Color',colors(i,:));
%         plot([0+trialDistance(i) 0+trialDistance(i)], ylim,'-.','Color',colors(i,:));        
%         xlabel('distance','Fontsize',FZ); ylabel('speed (m/s)','Fontsize',FZ); box off;

    figure(figRealTime); %axes(fig1(2)); 
    %use the below line if you want them to begin from zero
%     plot(realTime(meanFLLAll(:,i))-realTime(meanFLLAll(1,i)), meanforwardBodySpeedOrgAll(:,i),'Color', colors(i,:),'linewidth', LW);hold on;
%     plot(realTime(meanFLLAll(:,i))-realTime(meanFLLAll(1,i)), meanforwardBodySpeedOrgAll(:,i),'.','Color', colors(i,:),'markersize', MS)
    plot(realTime(meanFLLAll(:,i)), meanforwardBodySpeedOrgAll(:,i),'Color', colors(i,:),'linewidth', LW);hold on;
    plot(realTime(meanFLLAll(:,i)), meanforwardBodySpeedOrgAll(:,i),'.','Color', colors(i,:),'markersize', MS)
    xlabel('time','Fontsize',FZ); ylabel('speed (m/s)','Fontsize',FZ); box off;
    h = text(realTime(meanFLLAll(end,i)), meanforwardBodySpeedOrgAll(end,i), num2str(i)); set(h,'fontsize',15); 
    
    if normalDataFlagPlotNormal
         plot(realTime(Normal_FLLAll(:,i)), Normal_forwardBodySpeedAll(:,i), '-.','Color', colors(i,:));hold on;
         plot(realTime(Normal_FLLAll(:,i)), Normal_forwardBodySpeedAll(:,i),'*','Color', colors(i,:))
    end
    
%         plot(forwardBodyTimeAll(:,i)- max(onFoamTime), forwardBodySpeedAll(:,i),'Color',colors(i,:)); hold on; ylim([1.1 1.8]);
%         plot(forwardBodyTimeAll(:,i)- max(onFoamTime), forwardBodySpeedAll(:,i),'*','Color',colors(i,:));   
%         plot([max(onFoamTime)- max(onFoamTime)  max(onFoamTime)- max(onFoamTime)], ylim,'k-','linewidth',3);
    

%     else
        
%         plot(steps-steps(onFoamStride), forwardBodySpeedAll(:,i),'Color',colors(i,:)); hold on; 
%         plot(steps-steps(onFoamStride), forwardBodySpeedAll(:,i),'*','Color',colors(i,:)); 
% 
%         %plot foam and tarp locations
%         plot([steps(onFoamStride)-steps(onFoamStride)   steps(onFoamStride)-steps(onFoamStride)], ylim,'k-','linewidth',2);
%         
%         if ~noPacerFlag
%             plot([steps(b{i}(1))-steps(onFoamStride)   steps(b{i}(1))-steps(onFoamStride)], ylim,'-.','Color',colors(i,:));
%             plot([-steps(b{i}(1))+steps(onFoamStride)   -steps(b{i}(1))+steps(onFoamStride)], ylim,'-.','Color',colors(i,:));
%         end
% 
%         xlabel('steps','Fontsize',FZ); ylabel('speed (m/s)','Fontsize',FZ); box off;
%     end
   
%     if ~noPacerFlag
%         if length(foamThickness) > 1
%             str1 = strcat(num2str(foamThickness(i)*0.0254),'mBump',num2str(ceil(tarpLenght(i)*0.3)),'mRange');
%         else
%             str1 = strcat('Avg',num2str(foamThickness),'mBump',num2str(tarpLenght(i)),'mRange');
%         end
%     else
%         str1 = strcat('Avg',num2str(foamThickness),'mBumpNoPacer');
%     end
    
    str1 =  trialType;
    h = text(-30,yTemp1,str1); set(h,'fontsize',15); set(h,'color',colors(i,:))
    yTemp1 = yTemp1-0.045;
end 


figure(figDistance);%axes(fig1(1));
plot(mean(forwardBodyDistanceAll,2)- max(foamAvgLocation), mean(forwardBodySpeedAll,2),'r-','linewidth', LW); hold on; %ylim([1.1 1.8]);
plot(mean(forwardBodyDistanceAll,2)- max(foamAvgLocation), mean(forwardBodySpeedAll,2),'r.' ,'markersize', MS);         
plot([0  0], ylim,'k-','linewidth',2);
% plot([0-trialDistance(i) 0-trialDistance(i)], ylim,'-.','Color',colors(i,:));
% plot([0+trialDistance(i) 0+trialDistance(i)], ylim,'-.','Color',colors(i,:));        
xlabel('distance','Fontsize',FZ); ylabel('speed (m/s)','Fontsize',FZ); box off;


figure(figRealTime);%axes(fig1(2));    
% ylim([1.4 2]); 
temp_meanFLLAll = round(mean(meanFLLAll, 2));
% use the below line if you want them to begin from zero
% plot(realTime(temp_meanFLLAll)-realTime(temp_meanFLLAll(1)), mean(meanforwardBodySpeedOrgAll ,2),'r-','linewidth', LWavg)
% plot(realTime(temp_meanFLLAll)-realTime(temp_meanFLLAll(1)), mean(meanforwardBodySpeedOrgAll ,2),'r.','markersize', MS)
% plot([max(onFoamRealtime)-realTime(temp_meanFLLAll(1))  max(onFoamRealtime)-realTime(temp_meanFLLAll(1))], ylim,'k-','linewidth',2); 
plot(realTime(temp_meanFLLAll), mean(meanforwardBodySpeedOrgAll ,2),'r-','linewidth', LW+2)
plot(realTime(temp_meanFLLAll), mean(meanforwardBodySpeedOrgAll ,2),'r.','markersize', MS)
plot([max(onFoamRealtime)  max(onFoamRealtime)], ylim,'k-','linewidth',2); 


dataOut.onFoamRealtime  = max(onFoamRealtime); %onFoamRealtimeOut
dataOut.meanRealTime  = min(meanRealTime); %realTimeMagOut %REMEMBER realTime = PERIOD:PERIOD:min(meanRealTime);
dataOut.meanFLLAll  = meanFLLAll; %meanFLLAllOut
dataOut.meanforwardBodySpeedOrgAll  = meanforwardBodySpeedOrgAll; %meanforwardBodySpeedOrgAllOut


%this part to combine uneven data with oU. oD....
timeAvgX = [];
for xx = 1:size(meanFLLAll,2)
    timeAvgX(:,xx) = realTime(meanFLLAll(:,xx))';
end
timeAvgX = mean(timeAvgX,2);
dataOut.time = timeAvgX -dataOut.onFoamRealtime;
dataOut.speed = mean(meanforwardBodySpeedOrgAll,2);
%*****************

tempTime = 0;%3.6/meanPacerSpeed;
% plot([max(onFoamRealtime)-realTime(temp_meanFLLAll(1))-tempTime   max(onFoamRealtime)-realTime(temp_meanFLLAll(1))-tempTime ], ylim,'k--','linewidth',2); 
% plot([max(onFoamRealtime)-realTime(temp_meanFLLAll(1))+tempTime   max(onFoamRealtime)-realTime(temp_meanFLLAll(1))+tempTime ], ylim,'k--','linewidth',2); 
plot([max(onFoamRealtime)-tempTime   max(onFoamRealtime)-tempTime ], ylim,'k--','linewidth',2); 
plot([max(onFoamRealtime)+tempTime   max(onFoamRealtime)+tempTime ], ylim,'k--','linewidth',2); 
xlabel('time','Fontsize',FZ); ylabel('speed (m/s)','Fontsize',FZ); box off;
set(gca, 'FontSize', FZ);
str1 = trialType;
tempY = ylim;
tempX = xlim;
h = text(tempX(1)*1.15, tempY(2)*0.75, str1); set(h,'fontsize',15); 


figureAvg  = figure; hold on; ylim([1.25  1.7])
tempMeanData = mean(meanforwardBodySpeedOrgAll ,2);
plot(realTime(temp_meanFLLAll), tempMeanData,'r-','linewidth', LW+2)
% plot(realTime(temp_meanFLLAll), mean(meanforwardBodySpeedOrgAll ,2),'r.','markersize', MS)
plot([max(onFoamRealtime)  max(onFoamRealtime)], ylim,'k-','linewidth',2); 

plot([max(onFoamRealtime)-tempTime   max(onFoamRealtime)-tempTime ], ylim,'k--','linewidth',2); 
plot([max(onFoamRealtime)+tempTime   max(onFoamRealtime)+tempTime ], ylim,'k--','linewidth',2); 
plot(xlim, [mean(tempMeanData) mean(tempMeanData)],'g-','linewidth', 1)


figure(figRealTime)






