% Author: Lauro Ojeda, 2008-2015
function [W,A, static_period, M]  =  getdata(W,A,PERIOD,SECTION_SECONDS,BIAS,M)
% BIAS, is the static time at the beggining of the experiment
% leave BIAS the parameter empty to make the system detect the static time automatically
% 
% SECTION_SECONDS: a 1x2 array of floats, representing the endpoints of a
% time interval in seconds.

plt_hour = 0; % plot time in hours instead of seconds

% Calibrate
if(exist('BIAS','var') && ~isempty(BIAS)) 
    bias_interval = [ceil(BIAS(1)/PERIOD), floor(BIAS(2)/PERIOD)];
    [W, A] = calibrate(bias_interval, W, A);
	static_period = (bias_interval(1):bias_interval(2));
else
	%static_period = detect_quite_time(W,PERIOD);
    static_period = [];
end

% Crop
if(exist('SECTION_SECONDS', 'var') & ~isempty(SECTION_SECONDS))
    section_index_interval = [  ceil(SECTION_SECONDS(1)/PERIOD) ...
                             , floor(SECTION_SECONDS(2)/PERIOD) ];
    if (exist('M','var'))
        [W, A, M] = crop(section_index_interval, W, A, M);
    else
        [W, A] = crop(section_index_interval, W, A);
    end;
end

% Plot
if (exist('M','var') & ~isempty(M))
    plot_raw_data(PERIOD, plt_hour, W, A, M);
else
    plot_raw_data(PERIOD, plt_hour, W, A);
end

end
