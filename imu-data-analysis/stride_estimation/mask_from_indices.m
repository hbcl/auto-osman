function mask ... % Column vector of Booleans (n_frames)
  = mask_from_indices( ...
      n_frames, ... % Int, the total number of frames
      indices   ... % Vector of frame indices
      )

  mask = zeros(n_frames, 1);
  mask(indices) = 1;
  mask = mask == 1;
end % function
