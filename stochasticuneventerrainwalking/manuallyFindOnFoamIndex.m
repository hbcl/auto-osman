 %find it manually if findOnFoamIndices doesnt work
 function  [rightOnFoamIndex  leftOnFoamIndex ] = manuallyFindOnFoamIndex(currentData, timeL, fwrdShiftL_All, shiftStrideFwdL, stridesFrwdL, ...
                                                                                       timeR, fwrdShiftR_All, shiftStrideFwdR, stridesFrwdR)
    
                                                                                   
    tempF = plotShiftedElevation(currentData, timeL, fwrdShiftL_All, shiftStrideFwdL, stridesFrwdL, ...
     timeR, fwrdShiftR_All, shiftStrideFwdR, stridesFrwdR)                                                                                   
                                                                                   
     templeg = input('enter leg (red is right)', 's');
     tempIndex = input('enter number');        
     if strcmp(templeg ,'r')
         rightOnFoamIndex = tempIndex;
         leftOnFoamIndex = [];
     else
          rightOnFoamIndex = [];
         leftOnFoamIndex = tempIndex;
     end
 
     close(tempF);
end   