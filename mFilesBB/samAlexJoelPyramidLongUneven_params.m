
       %sam pyramid
%     data.startGrIndex{1} = [1];       data.endGrIndex{1} =  [265];    %normal 
%     data.startGrIndex{2} = [265];     data.endGrIndex{2} =  [575];    %pyramid
%     data.startGrIndex{3} = [575];     data.endGrIndex{3} =  [710];    %normal
%     data.startGrIndex{4} = [720];     data.endGrIndex{4} =  [1040];    %pyramid
%     data.startGrIndex{5} = [1088];    data.endGrIndex{5} =  [1195];    %normal



%alex pyramid
    data.startGrIndex{1} = [1];     data.endGrIndex{1} =  [205];    %normal 
    data.startGrIndex{2} = [200];     data.endGrIndex{2} =  [480];    %pyramid
    data.startGrIndex{3} = [480];     data.endGrIndex{3} =  [595];    %normal
    data.startGrIndex{4} = [590];     data.endGrIndex{4} =  [865];    %pyramid
    data.startGrIndex{5} = [865];     data.endGrIndex{5} =  [970];    %normal

    [data.trialEndOutlier{1}(1)] =  deal(4/PERIOD);
    [data.trialEndOutlier{1}(4)] =  deal(5/PERIOD);
  
    [data.trialEndOutlier{2}(5)] =  deal(1/PERIOD);
    [data.trialEndOutlier{2}(6)] =  deal(1/PERIOD);
    [data.trialEndOutlier{2}(10)] =  deal(5/PERIOD);
    
    [data.trialEndOutlier{3}(12)] =  deal(6/PERIOD);
    
    [data.trialEndOutlier{4}(15)] =  deal(3/PERIOD);
    [data.trialEndOutlier{4}(18)] =  deal(4/PERIOD);
    
    [data.trialEndOutlier{5}(20)] =  deal(5/PERIOD);
    
       %joel pyramid
    data.startGrIndex{1} = [1];     data.endGrIndex{1} =  [220];    %normal 
    data.startGrIndex{2} = [220];     data.endGrIndex{2} =  [525];    %pyramid
    data.startGrIndex{3} = [525];     data.endGrIndex{3} =  [635];    %normal
    data.startGrIndex{4} = [635];     data.endGrIndex{4} =  [990];    %pyramid
    data.startGrIndex{5} = [990];     data.endGrIndex{5} =  [1058];    %normal
    
       [data.trialEndOutlier{1}(1)] =  deal(3/PERIOD);
    [data.trialEndOutlier{1}(4)] =  deal(9/PERIOD);
%   
    [data.trialEndOutlier{2}(6)] =  deal(4/PERIOD);
    [data.trialEndOutlier{2}(7)] =  deal(3/PERIOD);
    [data.trialEndOutlier{2}(8)] =  deal(4/PERIOD);
    
    [data.trialEndOutlier{3}(11)] =  deal(4/PERIOD);
    [data.trialEndOutlier{3}(12)] =  deal(7/PERIOD);
    
    
    
    %sam longUneven
    [data.trialBeginOutlier{2}(8)] =  deal(35/PERIOD);
        data.startGrIndex{1} = [1];     data.endGrIndex{1} =  [250];    %normal 
    data.startGrIndex{2} = [235];     data.endGrIndex{2} =  [580];    %longUneven
    data.startGrIndex{3} = [570];     data.endGrIndex{3} =  [682];    %normal
    data.startGrIndex{4} = [689];     data.endGrIndex{4} =  [1120];    %longUneven
    data.startGrIndex{5} = [1120];     data.endGrIndex{5} =  [1238];    %normal
     
     %alex longUneven
        data.startGrIndex{1} = [1];     data.endGrIndex{1} =  [216];    %normal 
    data.startGrIndex{2} = [220];     data.endGrIndex{2} =  [530];    %longUneven
    data.startGrIndex{3} = [525];     data.endGrIndex{3} =  [646];    %normal
    data.startGrIndex{4} = [650];     data.endGrIndex{4} =  [1000];    %longUneven
    data.startGrIndex{5} = [1000];     data.endGrIndex{5} =  [1211];    %normal
    
    %joel longUneven
       data.startGrIndex{1} = [1];     data.endGrIndex{1} =  [240];    %normal 
    data.startGrIndex{2} = [230];     data.endGrIndex{2} =  [570];    %longUneven
    data.startGrIndex{3} = [580];     data.endGrIndex{3} =  [690];    %normal
    data.startGrIndex{4} = [690];     data.endGrIndex{4} =  [1060];    %longUneven
    data.startGrIndex{5} = [1060];     data.endGrIndex{5} =  [1185];    %normal
     
     
    