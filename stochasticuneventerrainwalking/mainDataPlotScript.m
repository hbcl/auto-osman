%% OSMAN DARICI 2018

clear; clc; %close all;

% name = {'DYpilot'}
% targetDirect = 'D:\spare\stochasticTerrainUnevenWalking\DYPliot1';
name = {'shaine'}%{'FarukPilot','LuciaPilot'};%{'FarukPilot'}{'DYpilot2'}
targetDirect = strcat('D:\spare\stochasticTerrainUnevenWalking\somedata\pyramid');%, name{1});
addpath(genpath(targetDirect));
% modelFigDirect = 'D:\spare\stochasticTerrainUnevenWalking\modelFigs'
% addpath(modelFigDirect)

storeMeans = false;

%%

trialType = {'pyramid_short'}%, 'Uneven1_2', 'Uneven2_1', 'Uneven2_2'};% oU_noConstraintNotLabel  oD2FFsBump_noConstraintWithLabel oU_noConstraintWithLabel  oD_noConstraintWithLabel 'Normal_NoConstraint
UnevenStepNum = 6
figMeanInit = 0;
colors  = jet(length(trialType));
clear data dataTS
str = '_pyramidSegmented'
dataSaveStr = str;

for nameInd = 1 : length(name)   
    clear data;
    load (strcat(name{nameInd}, str)); %load (name{nameInd});
    dataTS{nameInd} = data;                            %clear data;  to plot different data with same type you need to do trialType{nameInd} below and clear data at his line
    

    for trialTypeInd = 1 : length(trialType)        
        [figMean figMeanH] = plotNanalyseStrides(dataTS{nameInd},...
                                          'trialType', trialType{trialTypeInd}, 'storeMeans', storeMeans, 'figMean', figMeanInit,...
                                          'colorInput', colors(trialTypeInd, :),'targetDirect', targetDirect, 'UnevenStepNum', UnevenStepNum, 'dataSaveStr', dataSaveStr); %trialTypeInd
        figMeanInit = figMean;
    end
    
    saveOneFigure(figMeanH, trialType{nameInd }, targetDirect)

end

% speed_str = 'HallwayExperimentApprox1_50'
% numPoBf = 2
% xx2 = strcat('imuSpeed2_traj_', trialType, '_', speed_str, '_', num2str(numPoBf));
% openfig(xx2{1});
% ylimTemp = ylim;
% xlimTemp = xlim;
% text(xlimTemp(2)/2, ylimTemp(1)*1.15,trialType)
% xx2 = strcat('AngularVel2_traj_', trialType, '_', speed_str, '_', num2str(numPoBf));
% 
% openfig(xx2{1});
% ylimTemp = ylim;
% xlimTemp = xlim;
% text(xlimTemp(2)/2, ylimTemp(1)*1.15,trialType)

%%
clc;
clear; close all;
% for the first 7 test subjects ('CMR8','HD3', 'AS', 'AA',  'HA', 'SF5','SR'), stats works
name = {'FarukPilot', 'LuciaPilot', 'DYpilot2'}; %      Art4i'ACnew''NKv2new','ACnew''RY5','Art3', 'SF4

clear dataTS   data
str = '_stochasticSegmented'
for i = 1:length(name)
   load (strcat(name{i}, str)); 
   dataTS{i} = data; clear data;
end
%%
% close all

trialType = {'Uneven1_1', 'Uneven1_2', 'Uneven2_1', 'Uneven2_2'};

% multiple figure or data to save
fig_direct = 'D:\spare\stochasticTerrainUnevenWalking\somedata\avgs';
saveAvgDataFlag = 1

for i = 1:length(trialType)
    trialType{i}
    [dataOut] = plotAvgNew('trialType', trialType{i}, dataTS); 
    
    if saveAvgDataFlag
        figname = strcat('Avg', trialType{i});
    
        data_name = strcat('dataAvg', trialType{i})
       
        
        currentFolder = cd;
        cd(fig_direct);   
        saveas(gcf, figname, 'fig');
        save(data_name,'dataOut')
%         saveas(gcf, figname{1}, 'fig');
        fprintf(strcat(trialType{i},' Avg data saved \n'));
        cd(currentFolder); 
    end
end


