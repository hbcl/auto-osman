function [varargout] = plotNanalyseStrides(data, varargin)
%OSMAN DARICI 2018

% you've used avg_v4 for with and without pacer experiments to plot. Also
% plotDataAll_v9 was used to find on foam index.
% in this m file these are all combined with the option of finding on foam
% index. This m file is an extesion of avg_v4.

%the on foam indices ends with 2 (currentData{i}.rightOnFoamIndex2) is only
%for single test subject data analysis. It is used only ofr finding the on
%foam index and stored. It is NOT used when plotting actual averages


xlimVal2 = 40%70;
marg_h = [0.04 0.015]; marg_w = [0.05 0.005]; %for figures
load PERIOD;

colorInput = [0 0 0];
pacerDataFlag = false;

handImuFlag = false;

if nargin == 1 %only a trial plot
    currentData{1} = data;
    trialType =  data.trialType;
else
    opt_argin = varargin;
    while length(opt_argin) >= 2,
        opt = opt_argin{1};
        val = opt_argin{2};
        opt_argin = opt_argin(3:end);
        switch opt
            case 'trialType'
              trialType = val;
            case 'figMean'
              figMean = val;
            case 'storeMeans'
              storeMeans = val;
            case 'colorInput'
              colorInput = val;
            case 'targetDirect'
              targetDirect = val;     
            case 'UnevenStepNum'
              UnevenStepNum = val;  
            case 'dataSaveStr'
              dataSaveStr = val;
            otherwise
              error('options error')
        end  
    end
       
    nameForFig = data.name;
    currentData = data.(trialType).trial;

    if isempty(currentData)
        warning('No current data ************************\ n');
        varargout{1} = figMean;
        return;
    end
    
    index_delete = [];
    for i = 1:length(currentData)
        if isempty(currentData{i})
            index_delete = [index_delete i];
        end
    end
    currentData(index_delete) = [];
    

    if (figMean(1) == 0)%(meanTrials == true) && 
        figMeanH = figure; figMean = tight_subplot(2,1,[.04 .05], marg_h, marg_w);
    else
%         plotMeanInColorInput = 1;
    end  
%     Fig_SpeedVSRealtime = figure;
end

if length(currentData) == 1
    colors = ['b'];
else
    colors = jet(length(currentData)); %['r';'b';'k']
end
   
strech_data = 0;
plotCumDistance = 0;
FZ = 12;

%correct strides and speeds first
correctBegin = 2; correctEnd = 5;
for i = 1:length(currentData) 
    sizeR = size(currentData{i}.right_strides.frwd);
    sizeL = size(currentData{i}.left_strides.frwd);
    sizeMain = min(sizeR(2), sizeL(2)); %Fazlaliklar zaten asigada atilacak onlari strech ederken hesaba katma
    if strech_data 
        %correct strides and speeds first
        meanR = mean([currentData{i}.right_strides.frwd(end,correctBegin:correctEnd)     currentData{i}.right_strides.frwd(end, sizeMain-correctEnd:sizeMain-correctBegin)])  ; 
        meanL = mean([currentData{i}.left_strides.frwd(end,correctBegin:correctEnd)     currentData{i}.left_strides.frwd(end, sizeMain-correctEnd:sizeMain-correctBegin)]);
        meanRL = mean([meanR meanL]);

        stridesFrwdL{i} = currentData{i}.left_strides.frwd * meanRL/meanL;
        stridesFrwdR{i} = currentData{i}.right_strides.frwd * meanRL/meanR;

        meanR_speed = mean([currentData{i}.right_strides.frwd_speed(end,correctBegin:correctEnd)     currentData{i}.right_strides.frwd_speed(end, sizeMain-correctEnd:sizeMain-correctBegin)]);
        meanL_speed = mean([currentData{i}.left_strides.frwd_speed(end,correctBegin:correctEnd)     currentData{i}.left_strides.frwd_speed(end, sizeMain-correctEnd:sizeMain-correctBegin)]);
        meanRL_speed = mean([meanR_speed meanL_speed]);

        speedFrwdL{i} = currentData{i}.left_strides.frwd_speed * meanRL_speed/meanL_speed;   
        speedFrwdR{i} = currentData{i}.right_strides.frwd_speed * meanRL_speed/meanR_speed;
    else
        stridesFrwdL{i} = currentData{i}.left_strides.frwd;
        stridesFrwdR{i} = currentData{i}.right_strides.frwd;
        
        speedFrwdL{i} = currentData{i}.left_strides.frwd_speed; 
        speedFrwdR{i} = currentData{i}.right_strides.frwd_speed;
    end
    %*****************************************************************************
    

    %STRECTHED, now general shift between left and right
    timeL{i} = currentData{i}.left_strides.time';
    timeR{i} = currentData{i}.right_strides.time';  
    
    timeShift = mean([timeL{i}(correctBegin:correctEnd); timeL{i}(sizeMain-correctEnd: sizeMain-correctBegin);...
                      timeR{i}(correctBegin:correctEnd); timeR{i}(sizeMain-correctEnd: sizeMain-correctBegin)])/2;
    
    
    fwrdShift = mean([ stridesFrwdL{i}(end, correctBegin:correctEnd) stridesFrwdL{i}(end, sizeMain-correctEnd: sizeMain-correctBegin)...
                       stridesFrwdR{i}(end, correctBegin:correctEnd)  stridesFrwdR{i}(end, sizeMain-correctEnd: sizeMain-correctBegin)])/2;      
    
    FF_L{i} = find(currentData{i}.Left_foot.FF_walking);
    FF_R{i} = find(currentData{i}.Right_foot.FF_walking);    
	realTrialTime{i} = (1:size(currentData{i}.Left_foot.FF_walking,1))*PERIOD;
         
    if FF_R{i}(1) < FF_L{i}(1)%right step first
        timeShiftL_All{i} = timeShift; timeShiftR_All{i} = 0;
        fwrdShiftL_All{i} =  fwrdShift; fwrdShiftR_All{i} =  0;  
    else
        timeShiftR_All{i} = timeShift; timeShiftL_All{i} = 0;
        fwrdShiftR_All{i} =  fwrdShift; fwrdShiftL_All{i} =  0;  
    end
    %*****************************************************************************
end


%Shift stride time and distance to align them left right or vice versa ***************************************************************
for i = 1:length(currentData)     
    shiftStrideFwdR_tmpAll = []; shiftStrideFwdL_tmpAll = [];
    stridesFrwdUpdatedL_tmp = []; shiftStrideFwdL_tmp = 0; stridesFrwdUpdatedR_tmp = []; shiftStrideFwdR_tmp = 0;
    strideTimesUpdatedL_tmp = []; strideTimesUpdatedR_tmp = []; shiftTimeFwdL_tmp = 0; shiftTimeFwdR_tmp = 0;
           
    for j = 1:length(timeL{i})      
        %strideTimeL = linspace(0,timeL{i}(j),length(FF_L(j):FF_L(j+1)));%(timeL{i}(j)/PERIOD)+1);
        stridesFrwdUpdatedL_tmp = [stridesFrwdUpdatedL_tmp   fwrdShiftL_All{i} + shiftStrideFwdL_tmp + stridesFrwdL{i}(end,j)];           
        strideTimesUpdatedL_tmp = [ strideTimesUpdatedL_tmp    timeShiftL_All{i} + shiftTimeFwdL_tmp + timeL{i}(j)]; 
                
        shiftStrideFwdL_tmpAll = [shiftStrideFwdL_tmpAll shiftStrideFwdL_tmp];
        shiftStrideFwdL_tmp  = shiftStrideFwdL_tmp  + stridesFrwdL{i}(end,j); 
        shiftTimeFwdL_tmp  = shiftTimeFwdL_tmp  + timeL{i}(j); %cumsum
    end
    
    stridesFrwdUpdatedL{i} = stridesFrwdUpdatedL_tmp;
    shiftStrideFwdL{i} = shiftStrideFwdL_tmpAll;
    strideTimesUpdatedL{i} = strideTimesUpdatedL_tmp;
    
    currentData{i}.stridesFrwdUpdatedL = stridesFrwdUpdatedL{i};
    
    for j = 1:length(timeR{i})      
        %strideTimeR = linspace(0,timeR{i}(j),length(FF_R(j):FF_R(j+1)));%(timeR{i}(j)/PERIOD)+1);
        stridesFrwdUpdatedR_tmp = [ stridesFrwdUpdatedR_tmp    fwrdShiftR_All{i} + shiftStrideFwdR_tmp + stridesFrwdR{i}(end,j)];         
        strideTimesUpdatedR_tmp = [ strideTimesUpdatedR_tmp    timeShiftR_All{i} + shiftTimeFwdR_tmp + timeR{i}(j)]; 
                
        shiftStrideFwdR_tmpAll = [shiftStrideFwdR_tmpAll shiftStrideFwdR_tmp];
        shiftStrideFwdR_tmp  = shiftStrideFwdR_tmp  + stridesFrwdR{i}(end,j);  
        shiftTimeFwdR_tmp  = shiftTimeFwdR_tmp  + timeR{i}(j); %cumsum
    end
    
    stridesFrwdUpdatedR{i} = stridesFrwdUpdatedR_tmp;
    shiftStrideFwdR{i} = shiftStrideFwdR_tmpAll;
    strideTimesUpdatedR{i} = strideTimesUpdatedR_tmp;
    
    currentData{i}.stridesFrwdUpdatedR = stridesFrwdUpdatedR{i};
    

    % find on foam index  Because of try catch stateament this part only
    % works of the onfoam index is not found yet. So it will only work when
    % analysing single inidividual data
    try                                        % if you use if ~isempty it is not gonna work with old files
        currentData{i}.leftOnFoamIndex;        % if you dont have the on foam indices 
        currentData{i}.leftOnFoamIndex2;
    catch
        if isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
            [currentData{i}.rightOnFoamIndex2 currentData{i}.leftOnFoamIndex2 success] = findOnFoamIndices(currentData{i}); 
            
            %find the on foam instant based on acceleration******
            stridesTocheck = 2;
            handImuFig = figure;
            accFig = figure;  subplot(2,1,1);
            plot(realTrialTime{i},  currentData{i}.right_Ab(:,3), 'r'); hold on;
            plot(realTrialTime{i} (FF_R{i}), zeros(length(FF_R{i}),1), 'r*' );
            
            tempCheckR = floor(length(FF_R{i})/2);
            [minR, FFxxR ] = min(currentData{i}.right_Ab(FF_R{i}(tempCheckR-stridesTocheck):FF_R{i}(tempCheckR+stridesTocheck),3)); 
            FFxxR  = FFxxR + FF_R{i}(tempCheckR-stridesTocheck)-1;
             plot(realTrialTime{i}(FFxxR),  currentData{i}.right_Ab(FFxxR,3), 'k*');
            
            plot(realTrialTime{i},  currentData{i}.left_Ab(:,3), 'b'); hold on;
            plot(realTrialTime{i} (FF_L{i}), zeros(length(FF_L{i}),1), 'b*' );
            
            tempCheckL = floor(length(FF_L{i})/2);
            [minL, FFxxL] = min(currentData{i}.left_Ab(FF_L{i}(tempCheckL-stridesTocheck):FF_L{i}(tempCheckL+stridesTocheck),3)); FFxxL  = FFxxL + FF_L{i}(tempCheckL-stridesTocheck)-1;
            plot(realTrialTime{i}(FFxxL),  currentData{i}.left_Ab(FFxxL,3), 'k*');
          
            aa = FF_R{i} - FFxxR; aaa = find(aa>0);
            tempIndxR = aaa(1);
             
            aa = FF_L{i} - FFxxL; aaa = find(aa>0);
            tempIndxL = aaa(1);
     
            if minR < minL %remember to subtract one because it is FF bumber not stride number
                currentData{i}.rightOnFoamIndex = tempIndxR-1;
                currentData{i}.leftOnFoamIndex = [];
            else
                currentData{i}.leftOnFoamIndex = tempIndxL-1;
                currentData{i}.rightOnFoamIndex = [];
            end

            
            if success == false
                [currentData{i}.rightOnFoamIndex2 currentData{i}.leftOnFoamIndex2] = manuallyFindOnFoamIndex(currentData{i}, timeL, fwrdShiftL_All, shiftStrideFwdL, stridesFrwdL, ...
                                                                                                               timeR, fwrdShiftR_All, shiftStrideFwdR, stridesFrwdR);
            end 
                        
            figure(accFig);
            subplot(2,1,1);
            if ~isempty(currentData{i}.rightOnFoamIndex2)
                plot(realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex2+1)),  currentData{i}.right_Ab((FF_R{i}(currentData{i}.rightOnFoamIndex2+1)),3), 'g*');
                h = text(realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex2+1)),...
                currentData{i}.right_Ab((FF_R{i}(currentData{i}.rightOnFoamIndex2+1)),3), 'elev'); 
            else
                plot(realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex2+1)),  currentData{i}.left_Ab((FF_L{i}(currentData{i}.leftOnFoamIndex2+1)),3), 'g*');
                h = text(realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex2+1)),...
                currentData{i}.left_Ab((FF_L{i}(currentData{i}.leftOnFoamIndex2+1)),3), 'elev'); 
            end
            set(h,'fontsize',15); set(h,'color','g');

            %*******check waist IMU******
%             subplot(2,1,2);hold on;
%             plot(realTrialTime{i},  currentData{i}.waist_Ab(:,3), 'k'); 
% %             plot(realTrialTime{i},  currentData{i}.waist_Ab(:,1), 'r'); 
% %             plot(realTrialTime{i},  currentData{i}.waist_Ab(:,2), 'b'); 
%         
%             plot(realTrialTime{i} (FF_R{i}), zeros(length(FF_R{i}),1), 'r*' );
%             plot(realTrialTime{i} (FF_L{i}), zeros(length(FF_L{i}),1), 'b*' );
%             if ~isempty(currentData{i}.rightOnFoamIndex2)
%                 plot(realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex2+1)),  0, 'g*');
%                 h = text(realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex2+1)), 0, 'elev'); 
%             else
%                 plot(realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex2+1)),  0, 'g*');
%                 h = text(realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex2+1)), 0, 'elev'); 
%             end
%             set(h,'fontsize',15); set(h,'color','g');
%             plot(realTrialTime{i}(FFxxL),  0, 'k*');
%             plot(realTrialTime{i}(FFxxR), 0, 'k*');
            %**********************************************************
            % adjust the figure
            pos1 = 50; pos2 = 50; width1 = 1500; height1 = 1400;          
            drawnow;
%             set(get(handle(gcf),'JavaFrame'),'Maximized',1);
            set (gcf, 'Units', 'normalized', 'Position', [0,0,1,1]);
            %******************************
             
            if handImuFlag
                figure(handImuFig); hold on;
                plot(realTrialTime{i},  currentData{i}.hand_Ab(:,3), 'k'); 
                plot(realTrialTime{i},  currentData{i}.hand_Ab(:,1), 'r'); 
                plot(realTrialTime{i},  currentData{i}.hand_Ab(:,2), 'b'); 

%                 plot(realTrialTime{i} (FF_R{i}), zeros(length(FF_R{i}),1), 'r*' );
%                 plot(realTrialTime{i} (FF_L{i}), zeros(length(FF_L{i}),1), 'b*' );
%                 if ~isempty(currentData{i}.rightOnFoamIndex2)
%                     plot(realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex2+1)),  0, 'g*');
%                     h = text(realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex2+1)), 0, 'elev'); 
%                 else
%                     plot(realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex2+1)),  0, 'g*');
%                     h = text(realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex2+1)), 0, 'elev'); 
%                 end
%                 set(h,'fontsize',15); set(h,'color','g');
%                 plot(realTrialTime{i}(FFxxL),  0, 'k*');
%                 plot(realTrialTime{i}(FFxxR), 0, 'k*');

                % adjust the figure
                pos1 = 50; pos2 = 50; width1 = 1500; height1 = 1400;          
                drawnow;
%                 set(get(handle(gcf),'JavaFrame'),'Maximized',1);
                set (gcf, 'Units', 'normalized', 'Position', [0,0,1,1]);
            end  
        else
            currentData{i}.leftOnFoamIndex = [];
            currentData{i}.rightOnFoamIndex = [];
            
            currentData{i}.leftOnFoamIndex2 = [];
            currentData{i}.rightOnFoamIndex2 = [];
            
            shiftWholePlot{i} = 0;
            shiftWholePlotinTime{i} = 0;
        end
    end
    
    % find stuff to shift the in distance and time per trial
    
    %do it first for elev found on foam indices they with 2
    if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
        if  ~isempty(currentData{i}.leftOnFoamIndex2)
            shiftWholePlot2{i} =  stridesFrwdUpdatedL{i}( currentData{i}.leftOnFoamIndex2);
            shiftWholePlotinTime2{i} =  strideTimesUpdatedL{i}(currentData{i}.leftOnFoamIndex2);

            onFoamRealTime2(i) = realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex2+1)); %remmeber to put +1 here
            onFoamIndexRealTime2(i) = FF_L{i}(currentData{i}.leftOnFoamIndex2+1);                
        else               
            shiftWholePlot2{i} =  stridesFrwdUpdatedR{i}(currentData{i}.rightOnFoamIndex2);
            shiftWholePlotinTime2{i} =  strideTimesUpdatedR{i}(currentData{i}.rightOnFoamIndex2);

            onFoamRealTime2(i) = realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex2+1));
            onFoamIndexRealTime2(i) = FF_R{i}(currentData{i}.rightOnFoamIndex2+1);             
        end
          
        %do it first for acc found on foam indices        
        if  ~isempty(currentData{i}.leftOnFoamIndex)
            shiftWholePlot{i} =  stridesFrwdUpdatedL{i}( currentData{i}.leftOnFoamIndex);
            shiftWholePlotinTime{i} =  strideTimesUpdatedL{i}(currentData{i}.leftOnFoamIndex);

            onFoamRealTime(i) = realTrialTime{i}(FF_L{i}(currentData{i}.leftOnFoamIndex+1));
            onFoamIndexRealTime(i) = FF_L{i}(currentData{i}.leftOnFoamIndex+1);  
        else       
            shiftWholePlot{i} =  stridesFrwdUpdatedR{i}(currentData{i}.rightOnFoamIndex);
            shiftWholePlotinTime{i} =  strideTimesUpdatedR{i}(currentData{i}.rightOnFoamIndex);

            onFoamRealTime(i) = realTrialTime{i}(FF_R{i}(currentData{i}.rightOnFoamIndex+1));
            onFoamIndexRealTime(i) = FF_R{i}(currentData{i}.rightOnFoamIndex+1);
        end       
     end
end

%find the amount of shifting to align for all trials
shiftToAlign = []; shiftToAlignTime = [];
for i = 1:length(currentData)  
    if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))    
        shiftToAlign(i) = shiftWholePlot{i}; 
        shiftToAlignTime(i) =  shiftWholePlotinTime{i}; 
        
        if length(currentData) == 1
            shiftToAlign2(i) = shiftWholePlot2{i}; 
            shiftToAlignTime2(i) =  shiftWholePlotinTime2{i}; 
        end    
    else 
        shiftToAlign = 0;
        shiftToAlignTime = 0; 
        
        shiftToAlign2 = 0;
        shiftToAlignTime2 = 0; 
    end
end
shiftToAlign = shiftToAlign-max(shiftToAlign); %THIS IS FOR DISTANCE
shiftToAlignTime = shiftToAlignTime-max(shiftToAlignTime); 

%this is used only when analysing a single test subject data file, no use
%for plotting averages
if length(currentData) == 1
    shiftToAlign2 = shiftToAlign2-max(shiftToAlign2); 
    shiftToAlignTime2 = shiftToAlignTime2-max(shiftToAlignTime2);
end
%******************************************************************************************************
   
%find the averaged strides
forwardBodySpeed = []; forwardBodyTime =  []; forwardBodyDistance = [];
onFoamIndexAll = []; 
for i = 1:length(currentData)  
    %find aveg stride vels, times and combine DISCARD IF ONE FOOT HAS MORE STRIDES
    tempDistance = []; tempFwd = []; tempTime = []; tempDistance2 = [];
    for z = 1:min(length(stridesFrwdUpdatedR{i}),length(stridesFrwdUpdatedL{i}))      
        if FF_R{i}(1) < FF_L{i}(1)%timeShiftL_All{i} %right step first
            tempDistance = [tempDistance stridesFrwdUpdatedR{i}(z) stridesFrwdUpdatedL{i}(z)];
            tempFwd = [tempFwd speedFrwdR{i}(z) speedFrwdL{i}(z)]; 
            tempTime = [tempTime strideTimesUpdatedR{i}(z) strideTimesUpdatedL{i}(z)];
        else
            tempDistance = [tempDistance stridesFrwdUpdatedL{i}(z) stridesFrwdUpdatedR{i}(z)];
            tempFwd = [tempFwd speedFrwdL{i}(z) speedFrwdR{i}(z)];
            tempTime = [tempTime strideTimesUpdatedL{i}(z) strideTimesUpdatedR{i}(z)];
        end
    end 
    
    %add the last one if there is any
    if length(stridesFrwdUpdatedL{i}) > length(stridesFrwdUpdatedR{i})
        tempDistance = [tempDistance stridesFrwdUpdatedL{i}(end)];
        tempFwd = [tempFwd   speedFrwdL{i}(end)]; 
        tempTime = [tempTime strideTimesUpdatedL{i}(end)];
    elseif length(stridesFrwdUpdatedL{i}) < length(stridesFrwdUpdatedR{i})
        tempDistance = [tempDistance stridesFrwdUpdatedR{i}(end)];
        tempFwd = [tempFwd   speedFrwdR{i}(end)]; 
        tempTime = [tempTime strideTimesUpdatedR{i}(end)];
    end
 
    if plotCumDistance
        axes(fig55(i));
        tempDistance
        last_index = min(length(stridesFrwdUpdatedR{i}),length(stridesFrwdUpdatedL{i}));
        [stridesFrwdUpdatedR{i}(last_index) stridesFrwdUpdatedL{i}(last_index)]
        bar([stridesFrwdUpdatedR{i}(last_index) stridesFrwdUpdatedL{i}(last_index)]);
        ylabel('Cum distance'); ylim([35 45])
    end   
    
    forwardBodySpeed{i} = tempFwd';
    forwardBodyTime{i} =  tempTime';
    forwardBodyDistance{i} = tempDistance';
    
    %find the on foam index for averaged strides
    if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
        if  currentData{i}.leftOnFoamIndex
            onFoamIndex = find(forwardBodyDistance{i} == stridesFrwdUpdatedL{i}(currentData{i}.leftOnFoamIndex)); 
            onFoamIndexAll(i) = onFoamIndex(1);
        else  
            onFoamIndex = find(forwardBodyDistance{i} == stridesFrwdUpdatedR{i}(currentData{i}.rightOnFoamIndex));
            onFoamIndexAll(i) = onFoamIndex(1);
        end  
    end
end

%shift indices including real time*****************************************
forwardBodySpeedOrg = forwardBodySpeed;
if  isempty(cell2mat(strfind(currentData{1}.trialType,'Normal')))
    shiftIndices = max(onFoamIndexAll) - onFoamIndexAll; 
    onFoamIndexAllShifted = onFoamIndexAll + shiftIndices;  
    shiftRealTimeIndices = max(onFoamIndexRealTime) - onFoamIndexRealTime;
    
    for i = 1:length(currentData)   
        if shiftIndices(i) ~= 0
            forwardBodySpeed{i} = [forwardBodySpeed{i}(1)*ones(shiftIndices(i),1); forwardBodySpeed{i}];
            forwardBodyTime{i} =  [forwardBodyTime{i}(1)*ones(shiftIndices(i),1);   forwardBodyTime{i}];
            forwardBodyDistance{i} = [forwardBodyDistance{i}(1)* ones(shiftIndices(i),1); forwardBodyDistance{i}];
        end
        %now you can shift the time and distance and real trial time
        forwardBodyTime{i} =  -shiftToAlignTime(i) + forwardBodyTime{i};
        forwardBodyDistance{i} =  -shiftToAlign(i) + forwardBodyDistance{i};
        
        %shift the FLLs and real time to coincide the real on foam time
        updatedFF_walking_L{i} = [zeros(shiftRealTimeIndices(i),1) ; currentData{i}.Left_foot.FF_walking];
        updatedFF_walking_R{i} = [zeros(shiftRealTimeIndices(i),1) ;currentData{i}.Right_foot.FF_walking]; 
        
        updated_realTrialTime{i} = (1:size(updatedFF_walking_L{i},1))*PERIOD;
        
        updated_FF_L{i} = find(updatedFF_walking_L{i});
        updated_FF_R{i} = find(updatedFF_walking_R{i}); 

    end
else
    for i = 1:length(currentData)  
        updatedFF_walking_L{i} = currentData{i}.Left_foot.FF_walking;
        updatedFF_walking_R{i} = currentData{i}.Right_foot.FF_walking; 

        updated_realTrialTime{i} = realTrialTime{i};

        updated_FF_L{i} = find(updatedFF_walking_L{i});
        updated_FF_R{i} = find(updatedFF_walking_R{i});
    end
end

%trim******************************************
forwardBodySpeedAll = []; forwardBodyTimeAll =  []; forwardBodyDistanceAll = [];
datalengths = []; realTimeLens = []; FLLsAll =[];  Normal_FLLsAll =[]; 
for i = 1:length(currentData)   
    datalengths(i) =  length(forwardBodySpeed{i});  
    realTimeLens(i) = length(updated_realTrialTime{i});
end
mainDatalength = min(datalengths);
[~, realTimeMinIndex] = min(realTimeLens);
% ******************************************************

%THIS 3x1 figure is for test subject data analysis only
colorShift = 0;   threeFigFlag = 0;
if length(currentData)  == 1
   threeFigFlag = 1;
   figMeanH = figure; figMean = tight_subplot(3,1,[.04 .05], marg_h, marg_w); 
   % foam elev figure     
   axes(figMean(2));
   ystrTemp = 0.15;
   for j = 1:length(timeL{1})
       tempFF_L = find(currentData{1}.Left_foot.FF_walking);
       strideTimeL = linspace(0,timeL{1}(j),length(tempFF_L(j):tempFF_L(j+1)));        

       plot(fwrdShiftL_All{1} + shiftStrideFwdL{1}(j) + stridesFrwdL{1}(1:length(strideTimeL),j),...               
       -currentData{1}.left_strides.elev(1:length(strideTimeL),j),'bo'); hold on;   
   
        h = text(fwrdShiftL_All{1} + shiftStrideFwdL{1}(j) + stridesFrwdL{1}(length(strideTimeL),j),...
            ystrTemp, num2str(j)); set(h,'fontsize',15); set(h,'color','b');
   end
   ylabel('stride elev vs. frwd strides','Fontsize',FZ);
   
   for j = 1:length(timeR{1})
       tempFF_R = find(currentData{1}.Right_foot.FF_walking);
       strideTimeR = linspace(0,timeR{1}(j),length(tempFF_R(j):tempFF_R(j+1)));        

       plot(fwrdShiftR_All{1} + shiftStrideFwdR{1}(j) + stridesFrwdR{1}(1:length(strideTimeR),j),...                
       -currentData{1}.right_strides.elev(1:length(strideTimeR),j),'ro'); hold on;  
        
%        str1 = num2str(j);
        h = text(fwrdShiftR_All{1} + shiftStrideFwdR{1}(j) + stridesFrwdR{1}(length(strideTimeR),j),...
            ystrTemp+0.02, num2str(j)); set(h,'fontsize',15); set(h,'color','r');
   
   end
   xlim([0 xlimVal2]);
  
   %put the on foam bars
   if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
       %this is accc
        plot([-shiftToAlign(1)+shiftWholePlot{1}    -shiftToAlign(i)+shiftWholePlot{1}], ylim,'Color',colors(i+colorShift,:),'linewidth',3);       
        %this is elevation
        plot([-shiftToAlign2(1)+shiftWholePlot2{1}   -shiftToAlign2(i)+shiftWholePlot2{1}], ylim,'-.','Color',  [0 0 0],'linewidth',3);    
        h = text(-shiftToAlign2(1)+shiftWholePlot2{1},0, 'elev');   
        set(h,'fontsize',15); set(h,'color','g');
   end
   
    title(strcat(currentData{i}.trialType, ' trial'),'Fontsize',FZ+5); 
    xlim([0 xlimVal2]);
    % adjust the figure
    pos1 = 50; pos2 = 50; width1 = 1500; height1 = 1400;          
    drawnow;
%     set(get(handle(gcf),'JavaFrame'),'Maximized',1);
    set (gcf, 'Units', 'normalized', 'Position', [0,0,1,1]);
end
 
%trim DISTANCE AND REAL TIME and PLOT*******************************
trim_begin = 1; forwardBodySpeedOrgAll = []; lenFLLs = []; Normal_forwardBodySpeedAll = [];
onFoamFF = [];
for i = 1:length(currentData)   
    trim_end = datalengths(i)-mainDatalength; 
    forwardBodySpeedAll =  [forwardBodySpeedAll forwardBodySpeed{i}(trim_begin:end-trim_end)];
    forwardBodyTimeAll =   [forwardBodyTimeAll  forwardBodyTime{i}(trim_begin:end-trim_end)]; 
    forwardBodyDistanceAll = [forwardBodyDistanceAll forwardBodyDistance{i}(trim_begin:end-trim_end)];  
    
    % plot individuals respect to distance*******************************
    axes(figMean(1)); 
    plot(forwardBodyDistanceAll(:,i), forwardBodySpeedAll(:,i),'Color',colors(i,:)); hold on;
    plot(forwardBodyDistanceAll(:,i), forwardBodySpeedAll(:,i),'*','Color',colors(i,:)); 
    xlim([0 xlimVal2]); 
    
    %put the onFoam loaction bar*****************************
    if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
        axes(figMean(1));  %     -shiftToAlign(i)+shiftWholePlot{i}  is foam loc actually       
        plot(forwardBodyDistanceAll(onFoamIndexAllShifted(i),i), forwardBodySpeedAll(onFoamIndexAllShifted(i),i),'r*','linewidth',3);          
        plot([-shiftToAlign(i)+shiftWholePlot{i}   -shiftToAlign(i)+shiftWholePlot{i}], ylim,'Color',colors(i+colorShift,:),'linewidth',3);

        if length(currentData) == 1 %this is only for single trial analysis    
            plot([-shiftToAlign2(i)+shiftWholePlot2{i}                  -shiftToAlign2(i)+shiftWholePlot2{i}], ylim,'-.','Color', [0 0 0],'linewidth',3);            
            ylimTemp = ylim;
            xlimTemp = xlim;
            text(xlimTemp(2)*3/4, ylimTemp(2)*1/2,'solid vertical line is acc and dash dot vet. line is elevation') 
        end
        
        if threeFigFlag == 1
            axes(figMean(3));
        else
            axes(figMean(2));
        end
    end
    
    %Plot respect to REAL time*************************
    if threeFigFlag == 1
        axes(figMean(3));
    else
        axes(figMean(2));
    end
  
    %trim the real times and FLLs based on shortest real time this is like if there are 3 trials like 21.2 23 22.5 secs get the 21.2
    updated_realTrialTime{i} = updated_realTrialTime{realTimeMinIndex};
    updated_FF_R{i} = updated_FF_R{i}(updated_FF_R{i} < length(updated_realTrialTime{i}));
    updated_FF_L{i} = updated_FF_L{i}(updated_FF_L{i} < length(updated_realTrialTime{i}));
    
    FLLs{i} = sort([updated_FF_R{i}(2:end)' updated_FF_L{i}(2:end)']);
    tempLenFLL = length(FLLs{i});
    
    %keep this data to shift FLLs later
    lenFLLs = [lenFLLs tempLenFLL];    
    if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
        lenOnFoamFLL(i) = find(max(onFoamIndexRealTime) == FLLs{i});
    end
       
    %normally forwardBodySpeedOrg{i} and FLL{i} should have the same
    %dimesion but if the difference between left and right flls is more
    %than 1 then you need to eliminate the last one in the speed if
    %statement below
    if tempLenFLL < length(forwardBodySpeedOrg{i})
%         keyboard(); %         disp('line 605')
        forwardBodySpeedOrg{i} = forwardBodySpeedOrg{i}(1:length(FLLs{i}));
        plot(updated_realTrialTime{i}(FLLs{i}),  forwardBodySpeedOrg{i}, '-.', 'Color', colors(i+colorShift,:)); hold on %reduces the original speed if necessary
    else
        plot(updated_realTrialTime{i}(FLLs{i}(1:length(forwardBodySpeedOrg{i}))),  forwardBodySpeedOrg{i}, '-.','Color',colors(i+colorShift,:)); hold on
    end  
    plot(updated_realTrialTime{i}(updated_FF_R{i}(2:end)),  speedFrwdR{i}(1:length(updated_FF_R{i})-1),'r*'); 
    plot(updated_realTrialTime{i}(updated_FF_L{i}(2:end)),  speedFrwdL{i}(1:length(updated_FF_L{i})-1),'b*');

    if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
        onFoamTime = max(onFoamRealTime);
        plot([onFoamTime  onFoamTime ],ylim, 'b','linewidth',3)       
        if length(currentData) == 1 %this is only for single trial analysis 
            onFoamTime2 = max(onFoamRealTime2);
            plot([onFoamTime2  onFoamTime2 ],ylim, 'k-.','linewidth',3)
        end
    end
    
%     plot(forwardBodyTimeAll(:,i), forwardBodySpeedAll(:,i),'Color',colors(i,:)); hold on;
%     plot(forwardBodyTimeAll(:,i), forwardBodySpeedAll(:,i),'*','Color',colors(i,:)); 
%     xlim([0 xlimVal2]); 
end

%real time data shifted in time which means time maginitude, but not indices
%so you need the shift indices, before shifting find appropriate values for
%the speed and FLL data
if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
    maxlenOnFoamFLL = max(lenOnFoamFLL);
    for i = 1:length(currentData) 
        meanDiffFLL(i) = mean(diff(FLLs{i},1));
        meanInitSpeed(i) =  mean(forwardBodySpeedOrg{i}(1:2,:))
        shifFLLs(i) = maxlenOnFoamFLL  - lenOnFoamFLL(i); 
    end

    for i = 1:length(currentData) 
        for k = 1:shifFLLs(i)
            FLLs{i} = [FLLs{i}(1)-round(meanDiffFLL(i))  FLLs{i}];
            forwardBodySpeedOrg{i} = [meanInitSpeed(i);  forwardBodySpeedOrg{i}];
        end
    end
end

%you need to find the minimum data amount and trim the FLLs to get the mean
minForwardBodySpeedLen = []; minNormalForwardBodySpeedLen = [];
for i = 1:length(currentData)  
     minForwardBodySpeedLen = [ minForwardBodySpeedLen length(forwardBodySpeedOrg{i})];
end
minForwardBodySpeedLen = min(minForwardBodySpeedLen);

for i = 1:length(currentData)  
    FLLsAll = [FLLsAll FLLs{i}(1:minForwardBodySpeedLen)']; 
    forwardBodySpeedOrgAll = [forwardBodySpeedOrgAll forwardBodySpeedOrg{i}(1:minForwardBodySpeedLen)];
end

% put labels
axes(figMean(1)); 
ylabel('stride speed vs. frwd strides','Fontsize', FZ);
str1 = trialType;
yTemp1 = 1.7; h = text(30, yTemp1, str1); set(h,'fontsize',15);

if threeFigFlag == 1
    axes(figMean(3));
else
    axes(figMean(2));
end 
ylabel('stride speed vs. stride REAL time','Fontsize', FZ);

%plot means
if length(currentData) > 1
%     plot(mean(forwardBodyTimeAll,2), mean(forwardBodySpeedAll,2),'k','linewidth',LW); xlabel('time','Fontsize',FZ); ylabel('speed','Fontsize',FZ)
%     plot(mean(forwardBodyTimeAll,2), mean(forwardBodySpeedAll,2),'k*','linewidth',LW);

    FZ = 12; LW = 3;        
    axes(figMean(1)); hold on;
    plot(mean(forwardBodyDistanceAll,2), mean(forwardBodySpeedAll,2),'Color',colorInput,'linewidth',LW); xlabel('distance','Fontsize',FZ); ylabel('speed','Fontsize',FZ)
    plot(mean(forwardBodyDistanceAll,2), mean(forwardBodySpeedAll,2),'Color',colorInput,'linewidth',LW); 
    axes(figMean(2)); hold on;
    finalFFs = round(mean(FLLsAll,2));
    plot(updated_realTrialTime{1}(finalFFs), mean(forwardBodySpeedOrgAll,2),'Color',colorInput,'linewidth',LW)
    plot(updated_realTrialTime{1}(finalFFs), mean(forwardBodySpeedOrgAll,2),'*','Color',colorInput,'linewidth',LW)   
    plot([updated_realTrialTime{1}(finalFFs(onFoamIndexAllShifted(1)+UnevenStepNum)) updated_realTrialTime{1}(finalFFs(onFoamIndexAllShifted(1)+UnevenStepNum))], ylim,...
        'r','linewidth',LW)   
 
     
    axes(figMean(1));
    str1 = strcat('-',trialType, '-', data.name, '-',num2str(mean(std(forwardBodySpeedAll(1:end,:),1))));
    temp = ylim;
    h = text(15, temp(2)*8/10, str1); set(h,'fontsize',15); set(h,'Color',colorInput);
     
    varargout{1} = [figMean];    
    varargout{2} = figMeanH; 
    
    if storeMeans    
        if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
            data.(trialType).forwardBodyDistanceMean = mean(forwardBodyDistanceAll,2); % forwardBodyDistanceMean
            data.(trialType).forwardBodySpeedMean = mean(forwardBodySpeedAll,2);    % forwardBodySpeedMean
            data.(trialType).foamAvgLocation = -shiftToAlign(end)+shiftWholePlot{end};
            data.(trialType).onFoamAvgIndex = onFoamIndexAllShifted(1);% any of the shifted indices
            data.(trialType).onFoamRealtime = updated_realTrialTime{1}(max(onFoamIndexRealTime));
            
            data.(trialType).meanRealTime = updated_realTrialTime{1};
            data.(trialType).meanFLL = round(mean(FLLsAll,2));
            data.(trialType).meanforwardBodySpeedOrgAll = mean(forwardBodySpeedOrgAll,2);
            data.(trialType).realTimeFFs = updated_realTrialTime{1}(finalFFs);
            
        else
            data.(trialType).meanRealTime = updated_realTrialTime{1}(end);
            data.(trialType).meanFLL = round(mean(FLLsAll(:,2:end),2));
            data.(trialType).meanforwardBodySpeedOrgAll = mean(forwardBodySpeedOrgAll(:,2:end),2);
        end
          
        storeData(data, targetDirect, dataSaveStr);   
        disp(data.name); disp(trialType);
        disp('Test subject mean data is stored \n');
    end 
else    
    varargout{1} = currentData{i}.rightOnFoamIndex;
    varargout{2} = currentData{i}.leftOnFoamIndex;        
    
    varargout{3} = currentData{i}.rightOnFoamIndex2;
    varargout{4} = currentData{i}.leftOnFoamIndex2; 
    
     varargout{5} = figMean;
end




















% first find the stuff based on acc and ask if its ok don bother for elev
%     if meanTrials == true
%         axes(figMean(1)); hold on;
%         plot(mean(forwardBodyDistanceAll,2), mean(forwardBodySpeedAll,2),'k','linewidth',LW); xlabel('distance','Fontsize',FZ); ylabel('speed','Fontsize',FZ)
%         plot(mean(forwardBodyDistanceAll,2), mean(forwardBodySpeedAll,2),'k*','linewidth',LW);    
% 
%          i = 1;
%         plot([-shiftToAlign(i)+shiftWholePlot{i}                  -shiftToAlign(i)+shiftWholePlot{i}], ylim,'Color',colors(i+colorShift,:),'linewidth',3);
%         plot([-shiftToAlign(i)+shiftWholePlot{i}-trialDistance{i} -shiftToAlign(i)+shiftWholePlot{i}-trialDistance{i}], ylim,'-.','Color',colors(i+colorShift,:));
%         plot([-shiftToAlign(i)+shiftWholePlot{i}+trialDistance{i} -shiftToAlign(i)+shiftWholePlot{i}+trialDistance{i}], ylim,'-.','Color',colors(i+colorShift,:)); 
%         
%         
%         axes(figMean(2)); hold on;
%         plot(mean(forwardBodyTimeAll,2), mean(forwardBodySpeedAll,2),'k','linewidth',LW); xlabel('time','Fontsize',FZ); ylabel('speed','Fontsize',FZ)
%         plot(mean(forwardBodyTimeAll,2), mean(forwardBodySpeedAll,2),'k*','linewidth',LW);
%         varargout{1} = figMean;      
%         str1 = strcat(adjrange,'-',foamThickness,'-',trialType);
%         temp = ylim;
%         h = text(15, temp(2), str1); set(h,'fontsize',15);
%     else
%          varargout{1} = [];
%     end

% figure; colorShift = 0;
% for i = 1:length(currentData)
%     
%     
% %     FLLs = sort([FF_R{i}(2:end)' FF_L{i}(2:end)']);
% %     plot(realTrialTime{i}(FLLs),  forwardBodySpeed{i}, '-.','Color',colors(i+colorShift,:)); hold on
% %     
% %     plot(realTrialTime{i}(FF_R{i}(2:end)),  speedFrwdR{i}(end,:),'r*'); 
% %     plot(realTrialTime{i}(FF_L{i}(2:end)),  speedFrwdL{i}(end,:),'b*'); 
%     
%     FLLs = sort([updated_FF_R{i}(2:end)' updated_FF_L{i}(2:end)']);
%     plot(updated_realTrialTime{i}(FLLs),  forwardBodySpeed{i}, '-.','Color',colors(i+colorShift,:)); hold on
%     
%     plot(updated_realTrialTime{i}(updated_FF_R{i}(2:end)),  speedFrwdR{i}(end,:),'r*'); 
%     plot(updated_realTrialTime{i}(updated_FF_L{i}(2:end)),  speedFrwdL{i}(end,:),'b*');
%     
%     
%     
%     
%     
%     
%     onFoamTime = max(onFoamRealTime);
%     plot([onFoamTime  onFoamTime ],ylim, 'k','linewidth',3)
%     
% end


%             if success == true
%                 plotShiftedElevation(currentData{i}, timeL, fwrdShiftL_All, shiftStrideFwdL, stridesFrwdL,...
%                 timeR, fwrdShiftR_All, shiftStrideFwdR, stridesFrwdR);
%                 if isempty(currentData{i}.rightOnFoamIndex)
%                     plot([stridesFrwdUpdatedL{i}( currentData{i}.leftOnFoamIndex)  stridesFrwdUpdatedL{i}( currentData{i}.leftOnFoamIndex)], ylim,'Color',[0 0 0],'linewidth',3);
%                 else
%                     plot([stridesFrwdUpdatedR{i}( currentData{i}.rightOnFoamIndex)  stridesFrwdUpdatedR{i}( currentData{i}.rightOnFoamIndex)], ylim,'Color',[0 0 0],'linewidth',3);
%                 end
%                  
%                 result = input('are you happy with this?','s');
%                 if strcmp(result,'n')
%                     success = false;
%                 end   
%             else
%                 clf;
%             end




% trim_begin = 1;
% for i = 1:length(currentData)   
%    
%    
%     
%     trim_end = datalengths(i)-mainDatalength; 
%     forwardBodySpeedAll =  [forwardBodySpeedAll forwardBodySpeed{i}(trim_begin:end-trim_end)];
%     forwardBodyTimeAll =   [forwardBodyTimeAll  forwardBodyTime{i}(trim_begin:end-trim_end)]; 
%     forwardBodyDistanceAll = [forwardBodyDistanceAll forwardBodyDistance{i}(trim_begin:end-trim_end)];  
%     
% %     plot individuals respect to distance
%     axes(figMean(1)); 
%     plot(forwardBodyDistanceAll(:,i), forwardBodySpeedAll(:,i),'Color',colors(i,:)); hold on;
%     plot(forwardBodyDistanceAll(:,i), forwardBodySpeedAll(:,i),'*','Color',colors(i,:)); 
%     xlim([0 xlimVal2]); 
%     
%     if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
%         axes(figMean(1));        
%         plot(forwardBodyDistanceAll(onFoamIndexAllShifted(i),i), forwardBodySpeedAll(onFoamIndexAllShifted(i),i),'r*','linewidth',3);          
%     %     shiftWholePlot{i} is foam loc actually
%         plot([-shiftToAlign(i)+shiftWholePlot{i}                  -shiftToAlign(i)+shiftWholePlot{i}], ylim,'Color',colors(i+colorShift,:),'linewidth',3);
%         plot([-shiftToAlign(i)+shiftWholePlot{i}-trialDistance{i} -shiftToAlign(i)+shiftWholePlot{i}-trialDistance{i}], ylim,'-.','Color',colors(i+colorShift,:));
%         plot([-shiftToAlign(i)+shiftWholePlot{i}+trialDistance{i} -shiftToAlign(i)+shiftWholePlot{i}+trialDistance{i}], ylim,'-.','Color',colors(i+colorShift,:)); 
%         axes(figMean(3));
%         plot([shiftToAlignTime(i)+shiftWholePlotinTime{i}          shiftToAlignTime(i)+shiftWholePlotinTime{i}], ylim,'Color',colors(i+colorShift,:),'linewidth',3);
%     end
%     
%     %respect to time
%     axes(figMean(3)); 
%     plot(forwardBodyTimeAll(:,i), forwardBodySpeedAll(:,i),'Color',colors(i,:)); hold on;
%     plot(forwardBodyTimeAll(:,i), forwardBodySpeedAll(:,i),'*','Color',colors(i,:)); 
%     xlim([0 xlimVal2]); 
% end

% for i = 1:length(currentData)   
%    
%    
%     
%     trim_end = datalengths(i)-mainDatalength; 
%     forwardBodySpeedAll =  [forwardBodySpeedAll forwardBodySpeed{i}(trim_begin:end-trim_end)];
%     forwardBodyTimeAll =   [forwardBodyTimeAll  forwardBodyTime{i}(trim_begin:end-trim_end)]; 
%     forwardBodyDistanceAll = [forwardBodyDistanceAll forwardBodyDistance{i}(trim_begin:end-trim_end)];  
%     
% %     plot individuals respect to distance
%     axes(figMean(1)); 
%     plot(forwardBodyDistance{i}(trim_begin:end-trim_end), forwardBodySpeed{i}(trim_begin:end-trim_end),'Color',colors(i,:)); hold on;
%     plot(forwardBodyDistance{i}(trim_begin:end-trim_end), forwardBodySpeed{i}(trim_begin:end-trim_end),'*','Color',colors(i,:)); 
%     xlim([0 xlimVal2]); 
%     
%     if  isempty(cell2mat(strfind(currentData{i}.trialType,'Normal')))
%         axes(figMean(1));  
%         tempDistance = forwardBodyDistance{i}(trim_begin:end-trim_end);
%         tempSpeed =  forwardBodySpeed{i}(trim_begin:end-trim_end);
%         plot(tempDistance(onFoamIndexAllShifted(i),i), tempSpeed(onFoamIndexAllShifted(i),i),'r*','linewidth',3);          
%     
%         %     shiftWholePlot{i} is foam loc actually
%         plot([-shiftToAlign(i)+shiftWholePlot{i}                  -shiftToAlign(i)+shiftWholePlot{i}], ylim,'Color',colors(i+colorShift,:),'linewidth',3);
%         plot([-shiftToAlign(i)+shiftWholePlot{i}-trialDistance{i} -shiftToAlign(i)+shiftWholePlot{i}-trialDistance{i}], ylim,'-.','Color',colors(i+colorShift,:));
%         plot([-shiftToAlign(i)+shiftWholePlot{i}+trialDistance{i} -shiftToAlign(i)+shiftWholePlot{i}+trialDistance{i}], ylim,'-.','Color',colors(i+colorShift,:)); 
%         axes(figMean(3));
%         plot([shiftToAlignTime(i)+shiftWholePlotinTime{i}          shiftToAlignTime(i)+shiftWholePlotinTime{i}], ylim,'Color',colors(i+colorShift,:),'linewidth',3);
%     end
%     
%     %respect to time
%     axes(figMean(3)); 
%     plot(forwardBodyTimeAll(:,i), forwardBodySpeed{i}(trim_begin:end-trim_end),'Color',colors(i,:)); hold on;
%     plot(forwardBodyTimeAll(:,i), forwardBodySpeed{i}(trim_begin:end-trim_end),'*','Color',colors(i,:)); 
%     xlim([0 xlimVal2]); 
% end