# Implementation of the program

The following informal diagram outlines the structure of the imu-data-analysis
code. The code had decayed over the years from ad-hoc changes to the point
where no one really knew how it worked.
I was tasked with understanding it and cleaning it up.

This diagram was originally prepared for a presentation in a lab meeting,
the goal of which was to give a high-level overview of the code.
During the presentation, I explained the diagram and answered questions about it.
This document captures only some of that explanation; therefore, you should not
expect to gain a precise understanding of the code from this document alone.
I'm including the diagram because (1) with the aforementioned expectations in
mind, it probably illuminates more than it confuses, and (2) I was asked to
include it by the boss.

![](diagrams/imu-data-analysis.svg)

## Interpretation of the diagram

While I have tried to be consistent, this diagram still informal,
so don't attempt to extract precise statements from it.
This is only a high-level overview.

### Legend

Large double-stroked boxes indicate conceptual groupings (e.g. "Single-foot
integration").

Ovals represent data and arrows represent functions between data.

Diamonds annotate data (ovals), labelling them as the inputs or outputs of
conceptual groupings.

Boxes annotate functions (arrows).

The types of the data are sometimes included in an informal way.
For example, `: time -> R^3` is meant to be read as
"has the type of a function from time to triples of real numbers",
by which I mean that the data is a time series of 3D vectors.

### Notes

#### Single-foot integration

This part of the program transforms and filters raw data from a single IMU.
Raw IMU data is reported using a "local" reference frame
and needs to be transformed into a "global" reference frame.

To do this, the program assumes that the IMU is placed on the foot of a person that is walking
so that there will be periods where the foot is on the ground and relatively still.
During such stationary periods, the only force acting on the IMU should be gravity.
This assumption is the basis of the tilt correction applied to the IMU orientations
to address integration error.

These stationary periods are also used as boundaries dividing the recording into discrete footsteps.
We assume that the foot begins and ends each step on the ground in a stationary state.
Given this assumption, the total acceleration of the foot over a step should be approximately zero.
Accordingly,
in order to address integration error,
the program adjusts the accelerations of the foot so that the total acceleration over each step is exactly zero;
this is called the "zero-velocity update".

#### Two-foot integration

The individually obtained single-foot integration results are re-analyzed
by dividing the recording into discrete footsteps with an alternate procedure.
This alternate procedure uses the (horizontal) velocity peaks of the opposite foot as step boundaries.
There is a certain assumption that justifies this, but I don't know what it is -- ask Art.
Also, changing the step boundaries necessitates recomputation of the zero velocity updates.

#### `stride_segmentation` and `get_steps`

The intention behind a lot of this code is unknown to me;
we guessed that much of the code is probably ad-hoc and written for a specific project.
I decided not to try to understand, clean up, or rewrite the code,
given that such a task would be better executed by someone who understands these walking experiments.
As a result, my comments on this code will be brief.

The integration outputs are fed into these functions.

These functions do a lot of things and pack all the results into a big struct.

It looks like the most significant of these analyses is
to decompose the steps into forward and lateral directions,
which is done in two different ways.

There is also some code to use these results to filter out steps as outliers.

#### Other

Lots of plots are generated all throughout these procedures,
many of which I do not understand.
I assume a lot of them are just for sanity-checking and debugging.
