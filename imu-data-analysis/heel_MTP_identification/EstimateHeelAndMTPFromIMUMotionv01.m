function [out] = EstimateHeelAndMTPFromIMUMotionv01(walk_info);
% start with a data set where W0 is the angular velocity (debiased, in
% deg/sec), SECTION is the set of indices that were processed, and
% walk_info0 has come out of "compute_pos" (Lauro's code)

%% make Rotation Matrices out of the Quaternions. Rotmats are useful for
% linear algebra
q = walk_info.quaternion;
for ii = 1:size(q,1)
    rot0n{ii} = qua2rot(q(ii,:)) ; % this form left-multiplies a Column vector: Vnavigation(3x1) = rot0n*Vframe0(3x1)
    rod0n(ii,:) = rodrigues(rot0n{ii});
end
rod0n = unwraprodrigues(rod0n);

%% Find Toe Push-Off 
% One way is Peak Angular Velocity. This is considered to be the End
% of the "rotating at the ball of the foot" behavior
% % [~, endinds] =lmin(W0(SECTION,1).*(W0(SECTION,1)<-350));
% % nSamplesBeforePeakAngVel = 55;
% % startinds = endinds - nSamplesBeforePeakAngVel;

% Use height changes from the location at "footfall"
startHeight = 0.01;
endHeight = 0.04;
% use only the "middle" of walking bout
startpct = 0.1; % 0.2
endpct = 0.9; % 0.8
walkdistance = normvec(walk_info.P(end,:),2);
ffdistance = normvec(walk_info.P(walk_info.FF,:),2);
goodFF = find( (ffdistance > startpct*walkdistance) & (ffdistance < endpct*walkdistance) );
ffinds = find(walk_info.FF); ffinds = ffinds(goodFF); %ffinds = ffinds(ceil(0.2*length(ffinds)):floor(0.9*length(ffinds))); 
% startinds = arrayfun(@(a) a-1+find(walk_info0.P(a:end,3)>(walk_info0.P(a,3)+startHeight),1), ffinds,'UniformOutput',0);
startinds = arrayfun(@(a) a-1+find( normvec(walk_info.P(a:end,:)-repmat(walk_info.P(a,:),length(walk_info.P)-a+1,1),2) > startHeight,1), ffinds,'UniformOutput',0);
 bads = cellfun(@(a) isempty(a), startinds);
 ffinds(bads) = [];
 startinds = cat(1,startinds{:});
% endinds = arrayfun(@(a,ff) a-1+find(walk_info0.P(a:end,3)>(walk_info0.P(ff,3)+endHeight),1), startinds, ffinds,'UniformOutput',0);
endinds =  arrayfun(@(a) a-1+find( normvec(walk_info.P(a:end,:)-repmat(walk_info.P(a,:),length(walk_info.P)-a+1,1),2) > endHeight,1), ffinds,'UniformOutput',0);
 bads = cellfun(@(a) isempty(a), endinds);
 ffinds(bads) = [];
 startinds(bads) = [];
 endinds = cat(1,endinds{:});


%% for each push-off, define a window of time before that which is estimated
% to be consistent with a ball-of-the-foot rotating behavior.
nepochs = length(startinds);
clear epochs
for ii = 1:nepochs
    allinds = startinds(ii):endinds(ii); % get a bunch of samples before each end-of-ball-of-foot-rotation
    epochs(ii).inds = allinds ; 
    epochs(ii).P0n = reshape(walk_info.P(allinds,:)',[],1); % for the epoch, use Position of the IMU (minus the World Frame Origin (= 0)) as the Right-Hand Side. But format it as a column: [x;y;z;x;y;z;x;y;z;...]
    epochs(ii).R0n = cat(1,rot0n{allinds}); % rot0n is the rotation matrix at each frame (Vnav(3x1) = rot0n*V0(3x1)) where V0 is expressed in the IMU (0) frame. The concatenation (vertically) of all these makes a big matrix that multiplies a single body-fixed vector from IMU to the Ball of the Foot: r0. 
    epochs(ii).eyemat = zeros(length(allinds)*3, nepochs*3); % many columns of zeros to make way for individual-steps offsets in the World Frame. 
      epochs(ii).eyemat(:,((ii-1)*3)+(1:3)) = repmat(eye(3),length(allinds),1) ; % put Identity into the correct columns: [eye(3); eye(3); eye(3); ...]
end
LHS = [cat(1, epochs.eyemat), -1*cat(1, epochs.R0n)]; % concatenate the Eyes vertically (block-diagonal, with Identity in the blocks), and stack them next to all the matched body-to-world transformations, R0n.
RHS = cat(1,epochs.P0n); % concatenate all the reference data: IMU position in navigation frame. 
rnr0 = LHS\RHS ; % solve for all the offsets rn (x;y;z; x;y;z; x;y;z; ...] and the single body-fixed vector from IMU to Ball of Foot: r0.
rnMTP = rnr0(1:end-3); % a bunch of offsets (each stride) in the Navigation Frame
r0MTP = rnr0(end-2:end); % a single body-fixed vector from IMU to Ball of Foot: r0.
r0MTP

% Null Axis (joint axis)
[~,s,~] = svd(LHS,'econ'); s = diag(s)';
tol = mean(s(end-1:end))-eps;   % put it between the relevant vectors
nullspace = nulltol(LHS,tol); % get the 1-axis null space of the computation
nullAxisMTP = nullspace(end-2:end,:); nullAxisMTP = nullAxisMTP/normvec(nullAxisMTP,1);


%% Now plot how well this did: 
% % In Navigation Frame, plot of the estimated Ball of Foot location from both sides: World (static) and IMU  
% figure(12397800+1); scatterxyz(reshape(-LHS(:,end-2:end)*r0MTP + RHS,3,[])',[],1:(length(RHS)/3),'.'); hold on; plotxyz(reshape(LHS(:,1:end-3)*rnmtp,3,[])','ro'); axis equal; axis vis3d; rotate3d; title('Ball of the Foot and Spatial Best-Fits during Push-Off');
% Same, but with the IMU location added in 
figure(12397200+2); scatterxyz(reshape(-LHS(:,end-2:end)*r0MTP + RHS,3,[])',[],1:(length(RHS)/3),'.'); hold on; plotxyz(reshape(LHS(:,1:end-3)*rnMTP,3,[])','ro'); scatterxyz(reshape(RHS,3,[])',[],1:(length(RHS)/3),'.'); axis equal; axis vis3d; rotate3d; title('IMU, Ball of the Foot and Spatial Best-Fits during Push-Off');
hold on; plotxyz(walk_info.P);
% Similar, but with the Ball of the Foot projected to Zero for all strides, so we can see the scatter in the results    
figure(12397200+3); scatterxyz(reshape(-LHS(:,end-2:end)*r0MTP + RHS,3,[])' - reshape(LHS(:,1:end-3)*rnMTP,3,[])',[],1:(length(RHS)/3),'.'); hold on; scatterxyz(reshape(RHS,3,[])' - reshape(LHS(:,1:end-3)*rnMTP,3,[])',[],1:(length(RHS)/3),'.'); axis equal; axis vis3d; rotate3d; title('IMU Motion Relative to Ball of the Foot');





%% And, Find Heel Smack
% of the "rotating at the heel" behavior

% Use height changes from the location at "footfall"
endHeight = 0.01;
startHeight = 0.04;
% ffinds = find(walk_info0.FF); ffinds = ffinds(ceil(0.2*length(ffinds)):floor(0.9*length(ffinds))); 
% endinds = arrayfun(@(a) find(walk_info0.P(1:a,3)>(walk_info0.P(a,3)+endHeight),1,'last'), ffinds,'UniformOutput',0);
endinds = arrayfun(@(a) -1+find( normvec(walk_info.P(1:a,:)-repmat(walk_info.P(a,:),a,1),2) < endHeight,1), ffinds,'UniformOutput',0);
bads = cellfun(@(a) isempty(a), endinds);
 ffinds(bads) = [];
 endinds = cat(1,endinds{:});
% startinds = arrayfun(@(a,ff) find(walk_info0.P(1:a,3)>(walk_info0.P(ff,3)+startHeight),1,'last'), endinds, ffinds,'UniformOutput',0);
startinds =  arrayfun(@(a) find( normvec(walk_info.P(1:a,:)-repmat(walk_info.P(a,:),a,1),2) < startHeight,1), ffinds,'UniformOutput',0);
 bads = cellfun(@(a) isempty(a), startinds);
 ffinds(bads) = [];
 endinds(bads) = [];
 startinds = cat(1,startinds{:});


 
%% for each Foot Slap, define a window of time before that which is estimated
% to be consistent with a heel rotating behavior.
nepochs = length(startinds);
clear epochs
for ii = 1:nepochs
    allinds = startinds(ii):endinds(ii); % get a bunch of samples before each end-of-ball-of-foot-rotation
    epochs(ii).inds = allinds ; 
    epochs(ii).P0n = reshape(walk_info.P(allinds,:)',[],1); % for the epoch, use Position of the IMU (minus the World Frame Origin (= 0)) as the Right-Hand Side. But format it as a column: [x;y;z;x;y;z;x;y;z;...]
    epochs(ii).R0n = cat(1,rot0n{allinds}); % rot0n is the rotation matrix at each frame (Vnav(3x1) = rot0n*V0(3x1)) where V0 is expressed in the IMU (0) frame. The concatenation (vertically) of all these makes a big matrix that multiplies a single body-fixed vector from IMU to the Ball of the Foot: r0. 
    epochs(ii).eyemat = zeros(length(allinds)*3, nepochs*3); % many columns of zeros to make way for individual-steps offsets in the World Frame. 
      epochs(ii).eyemat(:,((ii-1)*3)+(1:3)) = repmat(eye(3),length(allinds),1) ; % put Identity into the correct columns: [eye(3); eye(3); eye(3); ...]
end
LHS = [cat(1, epochs.eyemat), -1*cat(1, epochs.R0n)]; % concatenate the Eyes vertically (block-diagonal, with Identity in the blocks), and stack them next to all the matched body-to-world transformations, R0n.
RHS = cat(1,epochs.P0n); % concatenate all the reference data: IMU position in navigation frame. 
rnr0 = LHS\RHS ; % solve for all the offsets rn (x;y;z; x;y;z; x;y;z; ...] and the single body-fixed vector from IMU to Ball of Foot: r0.
rnHeel = rnr0(1:end-3); % a bunch of offsets (each stride) in the Navigation Frame
r0Heel = rnr0(end-2:end); % a single body-fixed vector from IMU to Ball of Foot: r0.
r0Heel

% Null Axis (joint axis)
[~,s,~] = svd(LHS,'econ'); s = diag(s)';
tol = mean(s(end-1:end))-eps;   % put it between the relevant vectors
nullspace = nulltol(LHS,tol); % get the 1-axis null space of the computation
nullAxisHeel = nullspace(end-2:end,:); nullAxisHeel = nullAxisHeel/normvec(nullAxisHeel,1);


%% Now plot how well this did: 
% % In Navigation Frame, plot of the estimated Ball of Foot location from both sides: World (static) and IMU  
% figure; scatterxyz(reshape(-LHS(:,end-2:end)*r0Heel + RHS,3,[])',[],1:(length(RHS)/3),'.'); hold on; plotxyz(reshape(LHS(:,1:end-3)*rnheel,3,[])','ro'); axis equal; axis vis3d; rotate3d; title('Heel and Spatial Best-Fits during Foot-Smack');
% Same, but with the IMU location added in. 
figure(12397200+2); scatterxyz(reshape(-LHS(:,end-2:end)*r0Heel + RHS,3,[])',[],1:(length(RHS)/3),'.'); hold on; plotxyz(reshape(LHS(:,1:end-3)*rnHeel,3,[])','ro'); scatterxyz(reshape(RHS,3,[])',[],1:(length(RHS)/3),'.'); axis equal; axis vis3d; rotate3d; title('IMU, Heel and Spatial Best-Fits during Foot-Smack');
% Similar, but with the Ball of the Foot projected to Zero for all strides, so we can see the scatter in the results    
figure(12397200+4); scatterxyz(reshape(-LHS(:,end-2:end)*r0Heel + RHS,3,[])' - reshape(LHS(:,1:end-3)*rnHeel,3,[])',[],1:(length(RHS)/3),'.'); hold on; scatterxyz(reshape(RHS,3,[])' - reshape(LHS(:,1:end-3)*rnHeel,3,[])',[],1:(length(RHS)/3),'.'); axis equal; axis vis3d; rotate3d; title('IMU Motion Relative to Heel');



%% Now find FORWARD in the IMU frame: 
% Find the forward direction in each footfall from the direction of the
% following stride. 
% Then project it from World Frame to IMU Frame
% Then average across strides. 
for ii = 1:length(ffinds)-1
    forwardvec = diff(walk_info.P(ffinds(ii:ii+1),:),1,1).*[1 1 0]; % only use Planar direction. 
    forwardvechat = forwardvec ./ repmat(normvec(forwardvec,2),1,3); % normalize
    forwardvechatIMUall(ii,:) = ((rot0n{ffinds(ii)}')*forwardvechat'); % cast Forward from World into IMU frame
    upwardvechatIMUall(ii,:) = ((rot0n{ffinds(ii)}')*[0 0 1]'); % cast Vertical Up from World into IMU frame
    rightwardvechatIMUall(ii,:) = cross(forwardvechatIMUall(ii,:), upwardvechatIMUall(ii,:)) ; % cast Vertical Up from World into IMU frame
end
% Take the mean and renormalize
forwardvechatIMU = mean(forwardvechatIMUall,1); 
forwardvechatIMU = forwardvechatIMU / normvec(forwardvechatIMU);
upwardvechatIMU = mean(upwardvechatIMUall,1); 
upwardvechatIMU = upwardvechatIMU / normvec(upwardvechatIMU);
rightwardvechatIMU = cross(forwardvechatIMU, upwardvechatIMU); % Produce a Rightward direction
forwardvechatIMU = cross(upwardvechatIMU, rightwardvechatIMU); % Recreate Forward from Upward and Rightward so they are really orthogonal

figure(12397200+15); quiver3(0*forwardvechatIMUall(:,1), 0*forwardvechatIMUall(:,2), 0*forwardvechatIMUall(:,3),  forwardvechatIMUall(:,1), forwardvechatIMUall(:,2), forwardvechatIMUall(:,3), 'g.'); axis equal; axis([-1 1 -1 1 -1 1]); axis vis3d; rotate3d; hold on; 
quiver3(0*upwardvechatIMUall(:,1), 0*upwardvechatIMUall(:,2), 0*upwardvechatIMUall(:,3),  upwardvechatIMUall(:,1), upwardvechatIMUall(:,2), upwardvechatIMUall(:,3), 'b.'); 
quiver3(0*rightwardvechatIMUall(:,1), 0*rightwardvechatIMUall(:,2), 0*rightwardvechatIMUall(:,3),  rightwardvechatIMUall(:,1), rightwardvechatIMUall(:,2), rightwardvechatIMUall(:,3), 'r.'); 
legend({'Forward','Up','Right'});
xlabel('x'); ylabel('y'); zlabel('z'); title('IMU Frame Vectors'); 


% %% Now find an estimate of a "Toe" location
% % Toe comes off the ground about halfway between the time of maximum PF
% % angular velocity of the foot and the next time of Zero PF ang vel.
% % First, find the Maxima:
% wx = W0(SECTION,1);
% [Wnegmax, iWnegmax] = lmin(wx.*(wx<-400));
% % next find all the immediately following Zero Crossings
% allind = (1:size(wx,1))';
% iWnextpos = arrayfun(@(iii) find((wx.*(allind>iii)) > 0, 1, 'first'), iWnegmax );
% iUnique = unique(iWnextpos)';
% iWnegmax = arrayfun(@(iunq) round(mean(iWnegmax( find(iWnextpos==iunq) ))) , iUnique);
% iWnextpos = iUnique;
% 
% iToeOff = ceil(mean([iWnegmax, iWnextpos],2));
% 
% % Find Unit vector from Heel to the Projection of the MTP
% r0HeelTowardMTP = diag([1 1 0])*(r0MTP-r0Heel); r0HeelTowardMTP = r0HeelTowardMTP/normvec(r0HeelTowardMTP);
% 
% rRotHTMTP = cell2mat(arrayfun(@(iii) rot0n{iii}*r0HeelTowardMTP, iToeOff', 'UniformOutput',0))';
% 
% % Find a Reference Z 
% % value from the heel position at the previous and next
% % heel strikes (assume constant Z drift of the ground, as typically
% % happens as a data artifact)
% iFF = find(walk_info0.FF);
% zFF = walk_info0.P(iFF,3); % position of the IMU in Z
% zToeOff = interp1(iFF,zFF + r0Heel(3),iToeOff); % position of the Ground in Z, from the Heel
% 
% rHeelToeOff = cell2mat(arrayfun(@(iii) walk_info0.P(iii,:)'+rot0n{iii}*r0Heel, iToeOff', 'UniformOutput',0))' ;
% rToeToeOff = rHeelToeOff + repmat( ((zToeOff-rHeelToeOff(:,3))./rRotHTMTP(:,3)) ,1,3) .* rRotHTMTP; % Specific Solutions for each step
% lengthHeelToe = rRotHTMTP(:,3)\(zToeOff-rHeelToeOff(:,3)); % General solution (least-squares) for best Foot Length
% 
% r0Toe = r0Heel + r0HeelTowardMTP*lengthHeelToe;
% 
% rToeToeOffAvgFootLength = rHeelToeOff + (lengthHeelToe * rRotHTMTP);
% 
% lengthHeelToe
% r0Toe
% 
% 
% %% Now plot how well this did: 
% % % In Navigation Frame, plot of the estimated Toe location from both sides: World (static) and IMU  
% % figure; scatterxyz(reshape(-LHS(:,end-2:end)*r0Heel + RHS,3,[])',[],1:(length(RHS)/3),'.'); hold on; plotxyz(reshape(LHS(:,1:end-3)*rnheel,3,[])','ro'); axis equal; axis vis3d; rotate3d; title('Heel and Spatial Best-Fits during Foot-Smack');
% % Same, but with the IMU location added in 
% figure(12397200+2); scatterxyz(rToeToeOffAvgFootLength,[],1:length(iToeOff),'*'); hold on; scatterxyz(walk_info0.P(iToeOff,:),[],1:length(iToeOff),'*'); plotxyz(rToeToeOff,'gs'); axis equal; axis vis3d; rotate3d; title('IMU and Toe at Toe-Off');
% % Similar, but with the Ball of the Foot projected to Zero for all strides, so we can see the scatter in the results    
% figure(12397200+5); scatterxyz(rToeToeOffAvgFootLength - rToeToeOff,[],1:length(iToeOff),'.'); hold on; scatterxyz(walk_info0.P(iToeOff,:) - rToeToeOff,[],1:length(iToeOff),'.'); scatterxyz(rHeelToeOff - rToeToeOff,[],1:length(iToeOff),'.'); axis equal; axis vis3d; rotate3d; title('IMU Motion Relative to Toe');
% 
% % *** Not Working Perfectly Yet - lots of smear, result doesn't make sense
% % when plotted. 2013-10-25   



out.r0MTP = r0MTP';
out.r0Heel = r0Heel';
out.nullAxisMTP = nullAxisMTP';
out.nullAxisHeel = nullAxisHeel';
out.Rrfu = [rightwardvechatIMU; forwardvechatIMU; upwardvechatIMU]; % Rotation matrix that puts a vector from IMU Frame into a Right, Forward, Up (XYZ) frame thus: Vrfu (3x1) = Rrfu * Vimuframe (3x1)
% out.r0Toe = r0Toe;