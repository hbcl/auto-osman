

%use this m file to create the code to analyze the experiment
% you need to check how to fid the HS from acc
%this data uses the experiment that you've tested yourself on the hallway

clear all; clc
restoredefaultpath
% addpath('C:\Users\osman\Desktop\pilotWalking\hbcl-imu-data-analysis-17bd9a609b8f\hbcl-imu-data-analysis-17bd9a609b8f\stride_estimation')
addpath('D:\spare\SteppingOnFoamImu\stride_estimation')
addpath(genpath('C:\Users\osman\Desktop\desktop\Emg\EmgOsmanPilot2'));
addpath(genpath('D:\spare\GaitAnalysisToolBox_Son'));

%% Two APDM IMUs data processing simultaneously
clear all; close all
% Load IMU information from a file
% [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm('20171031-205514_emgPilotOsman2_sensor_779_label_Left.h5','20171031-205508_emgPilotOsman2_sensor_1027_label_Right.h5');

load 20171031-205508_emgPilotOsman2_sensor_1027_label_Rightnew2;
right_Wb = W; right_Ab = A; clear A W;
load 20171031-205514_emgPilotOsman2_sensor_779_label_Leftnew2
left_Wb = W; left_Ab = A;
clear W A
load PERIOD;
sizeApdm = size(left_Ab(:,3))
PERIOD_apdm = PERIOD; clear PERIOD;


%% HS trial
%this the mu's are hit on the table gently to see how the acc changes on
%each tap then a couple of genle steps on the foot.
% 
% clear all; close all
% % Load IMU information from a file
% [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm('20171208-103714_osmanIMU_HStrial_sensor_780_label_Left.h5','20171208-101003_osmanIMU_HStrial_sensor_780_label_Right.h5');
% 



%% process DELSYS IMU's on the feet
close all;clc
filename = 'Run_number_33_Plot_and_Store_Rep_2.6.csv' 

allDelsysData  = csvread(filename, 1, 0); 
sizeData = size(allDelsysData); rowSize = sizeData(1); colSize = sizeData(2); 
timeEMG = allDelsysData(:,21); %21
PERIOD_Del_emg = timeEMG(2);
% muscle = allDelsysData(:,42); % 22 RRF 42 RGM 62 LSOL 82LRF  102 L GM

muscleR{1} = allDelsysData(:,2);%RSOL
muscleR{2} = allDelsysData(:,22);%RRF
muscleR{3} = allDelsysData(:,42);%RGM

muscleL{1} = allDelsysData(:,62);%LSOL
muscleL{2} = allDelsysData(:,82);%LRF
muscleL{3} = allDelsysData(:,102);%LGM

muscleR_name = {'RSOL', 'RRF', 'RGM'}; muscleL_name = {'LSOL', 'LRF', 'LGM'};

%indexSignal = find(muscle ~=0); %RTA RMV LTA LVM 2 22 42 62
% muscle = muscle(indexSignal); sizeSignal = length(muscle)

%time and foot signals
time_imu = allDelsysData(:,143);
PERIOD_Del_imu = time_imu(2);
GRAVITY = 9.80297286843;

right_Ab_Del = [allDelsysData(:,144) allDelsysData(:,146) allDelsysData(:,148)]*GRAVITY;
right_Wb_Del = [allDelsysData(:,150) allDelsysData(:,152) allDelsysData(:,154)]*pi/180*PERIOD_Del_imu;

left_Ab_Del = [allDelsysData(:,164) allDelsysData(:,166) allDelsysData(:,168)]*GRAVITY;
left_Wb_Del = [allDelsysData(:,170) allDelsysData(:,172) allDelsysData(:,174)]*pi/180*PERIOD_Del_imu;

%eliminate zeros
% indexSignal1 = find(right_Ab_Del(:,1) ~=0); indexSignal2 = find(right_Ab_Del(:,2) ~=0); indexSignal3 = find(right_Ab_Del(:,3) ~=0);
% indexAcc_Del1 = min([indexSignal1(1) indexSignal1(1) indexSignal1(1)]);
% indexAcc_Del2 = min([indexSignal1(end) indexSignal1(end) indexSignal1(end)]);
% 
% right_Ab_Del = right_Ab_Del(indexAcc_Del1:indexAcc_Del2, :)*GRAVITY;
% right_Wb_Del = right_Wb_Del(indexAcc_Del1:indexAcc_Del2, :)*pi/180*PERIOD_Del_imu;
% 
% left_Ab_Del = left_Ab_Del(indexAcc_Del1:indexAcc_Del2, :)*GRAVITY;
% left_Wb_Del = left_Wb_Del(indexAcc_Del1:indexAcc_Del2, :)*pi/180*PERIOD_Del_imu;
% 
% time_imuDelsys = time_imuDelsys(indexAcc_Del1:indexAcc_Del2);




%% pick the section from Apdm and Delysis
close all
figure; plot(right_Ab_Del(:,3), 'b'); ylim([-50 50])
figure; plot(right_Ab(:,3),'r');ylim([-50 50])

sect_Del_imu = [13000:23000]% this is samples
sect_Apdm = [325000:334250]% this is samples

tempIndex1 = find(timeEMG > time_imu(sect_Del_imu(1)));
tempIndex2 = find(timeEMG > time_imu(sect_Del_imu(end)));

sect_Del_emg = [tempIndex1(1):tempIndex2(1)];

figure; plot(right_Ab_Del(sect_Del_imu,3), 'b'); ylim([-50 50])
figure; plot(right_Ab(sect_Apdm,3),'r');ylim([-50 50])
%%
%get the section data
close all
%imu Delsys section
right_Ab_Del_sect = right_Ab_Del(sect_Del_imu,:);
left_Ab_Del_sect =  left_Ab_Del(sect_Del_imu,:);
right_Wb_Del_sect = right_Wb_Del(sect_Del_imu,:);
left_Wb_Del_sect = left_Wb_Del(sect_Del_imu,:);
time_imu_sect = time_imu(sect_Del_imu,:);

%emg Delsys section
for i = 1:length(muscleR)
    muscleR_sect{i} = muscleR{i}(sect_Del_emg);
end

for i = 1:length(muscleL)
    muscleL_sect{i} = muscleL{i}(sect_Del_emg);
end

timeEMG_sect = timeEMG(sect_Del_emg);

%Apdm section
right_Ab_sect = right_Ab(sect_Apdm,:);
left_Ab_sect =  left_Ab(sect_Apdm,:);
right_Wb_sect =  right_Wb(sect_Apdm,:);
left_Wb_sect =   left_Wb(sect_Apdm,:);

% clear tempAb
tempAb = [-right_Ab_Del_sect(:,2) -right_Ab_Del_sect(:,1) -right_Ab_Del_sect(:,3)];
right_Ab_Del_sect = tempAb;
ylimVal = 50%0.04
for dim = 1:3
    figure; plot(right_Ab_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('A right Del dim',num2str(dim)));
    figure; plot(right_Ab_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('A right Apdm dim',num2str(dim)));
end

clear tempAb
tempAb = [-left_Ab_Del_sect(:,2) -left_Ab_Del_sect(:,1) -left_Ab_Del_sect(:,3)];
left_Ab_Del_sect = tempAb;
for dim = 1:3
    figure; plot(left_Ab_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('A left Del dim',num2str(dim)));
    figure; plot(left_Ab_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('A left Apdm dim',num2str(dim)));
end

clear tempWb
tempWb = -[right_Wb_Del_sect(:,2) right_Wb_Del_sect(:,1) right_Wb_Del_sect(:,3)];
right_Wb_Del_sect = tempWb;
ylimVal = 0.08%0.04
for dim = 1:3
    figure; plot(right_Wb_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('W right Del dim',num2str(dim)));
    figure; plot(right_Wb_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('W right Apdm dim',num2str(dim)));
end

clear tempWb
tempWb = -[left_Wb_Del_sect(:,2) left_Wb_Del_sect(:,1) left_Wb_Del_sect(:,3)];
left_Wb_Del_sect = tempWb;
for dim = 1:3
    figure; plot(left_Wb_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('W left Del dim',num2str(dim)));
    figure; plot(left_Wb_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('W left Apdm dim',num2str(dim)));
end



%% find HSs for Delsys 
close all
figure
plot(right_Ab_Del_sect(:,3),'r'); hold on; plot(left_Ab_Del_sect(:,3),'b');
%filter the dim3 acc from IMU's
[Bf, Af] = butter(2, 5*2/(1/PERIOD_Del_imu));
right_Ab_newDim3_filt = filtfilt(Bf, Af, right_Ab_Del_sect(:,3));
left_Ab_newDim3_filt = filtfilt(Bf, Af, left_Ab_Del_sect(:,3));
plot(right_Ab_newDim3_filt,'r.'); hold on; plot(left_Ab_newDim3_filt,'b.');
[~,locsR] = findpeaks(right_Ab_newDim3_filt,'MinPeakHeight',0);
[~,locsL] = findpeaks(left_Ab_newDim3_filt,'MinPeakHeight',0);

%this part checks medians and clears if there is any HS found other than walking*******************************
%this assumes HSs found during walking are true, means the foot didnt do anything weird
med_L = median(diff(locsL));
med_R = median(diff(locsR));

tempIndexL = find(diff(locsL) > 3*med_L); 
tempIndexR = find(diff(locsR) > 3*med_R); 

% shift the ndices once if they are after the middle index of the
% locations, so when you clear you'll clear the section comes after walking
tempIndexL(tempIndexL>length(locsL)/2) = tempIndexL(tempIndexL>length(locsL)/2)+1
tempIndexR(tempIndexR>length(locsR)/2) = tempIndexR(tempIndexR>length(locsR)/2)+1
locsR(tempIndexR) = [];
locsL(tempIndexL) = [];
% plot(locsR, right_Ab_newDim3_filt(locsR),'kd'); hold on; plot(locsL, left_Ab_newDim3_filt(locsL),'kd');

HSL = locsL(2:end-2);
HSR = locsR(2:end-2);
plot(HSR, right_Ab_newDim3_filt(HSR),'kd'); hold on; plot(HSL, left_Ab_newDim3_filt(HSL),'kd');
%*****************************************************************************************************************


%check one more it  is left to right or right to left and find HS****************************
if HSL(1) < HSR(1)
    currentHS = 'left'
else
    currentHS = 'right'
end

HS_imu_R = []; HS_imu_L = [];
for i = 1:min(length(HSL), length(HSR))   
    if strcmp(currentHS, 'left')       
        if HSL(i) < HSR(i)
            HS_imu_R = [HS_imu_R HSR(i)];
            HS_imu_L = [HS_imu_L HSL(i)];
        else
            error('check HS')
        end
    else %right
        if HSR(i) < HSL(i)
            HS_imu_R = [HS_imu_R HSR(i)];
            HS_imu_L = [HS_imu_L HSL(i)];
        else
            error('check HS')
        end
    end 
end
%*****************************************************************************************************
HS_imu = [sort([HS_imu_R HS_imu_L])];
%% find EMG HS 
figure; plot(time_imu_sect, right_Ab_Del_sect(:,3)); hold on; plot(time_imu_sect(HS_imu), zeros(1, length(HS_imu)),'k*');
plot(time_imu_sect, left_Ab_Del_sect(:,3)); plot(time_imu_sect(HS_imu), zeros(1, length(HS_imu)),'k*');

HS_emg_R = []; HS_emg_L = [];
for z = 1:length(HS_imu_R)   
    temp = find(timeEMG_sect == time_imu_sect(HS_imu_R(z)));
    if ~isempty(temp)
        HS_emg_R = [HS_emg_R temp];
    else
        temp2 = find(timeEMG_sect > time_imu_sect(HS_imu_R(z)));
        HS_emg_R = [HS_emg_R temp2(1)];
    end
end
 
for z = 1:length(HS_imu_L) 
    temp = find(timeEMG_sect == time_imu_sect(HS_imu_L(z)));
    if ~isempty(temp)
        HS_emg_L = [HS_emg_L temp];
    else
        temp2 = find(timeEMG_sect > time_imu_sect(HS_imu_L(z)));
        HS_emg_L = [HS_emg_L temp2(1)];
    end    
end

HS_emg = sort([HS_emg_R HS_emg_L]);

plot(timeEMG_sect(HS_emg), zeros(1, length(HS_emg)), 'bo', 'markersize', 5);
%%

%static period IMU
static_period_Del_imu  =  detect_quite_time(right_Wb_Del_sect, PERIOD_Del_imu);
figure; 
plot(time_imu_sect, right_Wb_Del_sect(:,3)); hold on
plot(time_imu_sect(static_period_Del_imu), right_Wb_Del_sect(static_period_Del_imu), 'k*')

%static period EMG
static_period_Del_emg = [];
for z = 1:length(static_period_Del_imu)
    temp = find(timeEMG_sect == time_imu_sect(static_period_Del_imu(z)));
    if ~isempty(temp)
        static_period_Del_emg = [static_period_Del_emg temp];
    else
        temp2 = find(timeEMG_sect > time_imu_sect(static_period_Del_imu(z)));
        static_period_Del_emg = [static_period_Del_emg temp2(1)];
    end
end



%% try running Lauro's code with Delsys and get FFL's
close all
[left_Wb_Del_temp, left_Ab_Del_temp] = getdata(left_Wb_Del_sect, left_Ab_Del_sect, PERIOD_Del_imu,[]);
[right_Wb_Del_temp, right_Ab_Del_temp] = getdata(right_Wb_Del_sect, right_Ab_Del_sect, PERIOD_Del_imu,[]);
% [left_walk_info_Del_temp , right_walk_info_Del_temp ] = compute_pos_two_imus(left_Wb_Del_temp, left_Ab_Del_temp,...
%     right_Wb_Del_temp, right_Ab_Del_temp, PERIOD_Del_imu);
left_walk_info_Del_temp = compute_pos(left_Wb_Del_temp,left_Ab_Del_temp,PERIOD_Del_imu);
right_walk_info_Del_temp = compute_pos(right_Wb_Del_temp,right_Ab_Del_temp,PERIOD_Del_imu);

left_strides_Del_temp = stride_segmentation(left_walk_info_Del_temp,PERIOD_Del_imu, []);
right_strides_Del_temp = stride_segmentation(right_walk_info_Del_temp,PERIOD_Del_imu, []);  

FF_imu_R = find(right_walk_info_Del_temp.FF_walking ~=0);
FF_imu_L = find(left_walk_info_Del_temp.FF_walking ~=0);

FF_imu = sort([FF_imu_L FF_imu_R]);
figure; plot(FF_imu_L,'b*'); hold on;plot(FF_imu_R,'r*');  title('imu FF')

FF_emg_R = []; 
for z = 1:length(FF_imu_R)   
    temp = find(timeEMG_sect == time_imu_sect(FF_imu_R(z)));
    if ~isempty(temp)
        FF_emg_R = [FF_emg_R temp];
    else
        temp2 = find(timeEMG_sect > time_imu_sect(FF_imu_R(z)));
        FF_emg_R = [FF_emg_R temp2(1)];
    end
end
 FF_emg_L = [];
for z = 1:length(FF_imu_L) 
    temp = find(timeEMG_sect == time_imu_sect(FF_imu_L(z)));
    if ~isempty(temp)
        FF_emg_L = [FF_emg_L temp];
    else
        temp2 = find(timeEMG_sect > time_imu_sect(FF_imu_L(z)));
        FF_emg_L = [FF_emg_L temp2(1)];
    end    
end
FF_emg = sort([FF_emg_L FF_emg_R]);
figure; plot(FF_emg_L,'b*'); hold on;plot(FF_emg_R,'r*'); title('EMG FF')

figure;
subplot(1,2,1);
plt_ltrl_frwd_strides(left_strides_Del_temp,0);
subplot(1,2,2);
plt_ltrl_frwd_strides(right_strides_Del_temp,0);
matchaxes; title('delsys')
hgsave(gcf, 'del1');

figure;
subplot(2,1,1)
plt_frwd_elev_strides(left_strides_Del_temp,0);
subplot(2,1,2)
plt_frwd_elev_strides(right_strides_Del_temp,0);
matchaxes; title('delsys')
hgsave(gcf, 'del2');

%process APDM's
% [left_Wb_temp, left_Ab_temp] = getdata(left_Wb_sect, left_Ab_sect, PERIOD_apdm,[]);
% [right_Wb_temp, right_Ab_temp] = getdata(right_Wb_sect, right_Ab_sect, PERIOD_apdm,[]);
% % [left_walk_info_temp , right_walk_info_temp ] = compute_pos_two_imus(left_Wb_temp, left_Ab_temp,...
% %     right_Wb_temp, right_Ab_temp, PERIOD_apdm);
% left_walk_info_temp = compute_pos(left_Wb_temp,left_Ab_temp,PERIOD_apdm);
% right_walk_info_temp = compute_pos(right_Wb_temp,right_Ab_temp,PERIOD_apdm);
% 
% left_strides_temp = stride_segmentation(left_walk_info_temp,PERIOD_apdm, []);
% right_strides_temp = stride_segmentation(right_walk_info_temp,PERIOD_apdm, []);  
% 
% 
% figure;
% subplot(1,2,1);
% plt_ltrl_frwd_strides(left_strides_temp,0);
% subplot(1,2,2);
% plt_ltrl_frwd_strides(right_strides_temp,0);
% matchaxes; title('apdm')
% % hgsave(gcf, 'apdm1');
% figure;
% subplot(2,1,1)
% plt_frwd_elev_strides(left_strides_temp,0);
% subplot(2,1,2)
% plt_frwd_elev_strides(right_strides_temp,0);
% matchaxes; title('apdm')
% % hgsave(gcf, 'apdm2');
% 
% figure; 
% subplot(2,1,1); hold on; 
% f1 = plot(left_strides_Del_temp.frwd_speed,'b'); plot(left_strides_Del_temp.frwd_speed,'b*');
% f2 = plot(left_strides_temp.frwd_speed,'r'); plot(left_strides_temp.frwd_speed,'r*'); 
% legend([f1 f2], 'Delsys', 'Apdm')
% subplot(2,1,2); hold on; 
% f1 = plot(right_strides_Del_temp.frwd_speed,'b'); plot(right_strides_Del_temp.frwd_speed,'b*');
% f2 = plot(right_strides_temp.frwd_speed,'r'); plot(right_strides_temp.frwd_speed,'r*'); 
% legend([f1 f2], 'Delsys', 'Apdm')
% % hgsave(gcf, 'speedApdmDelsys');
%%
close all; clc

%plot raw emg's
figure;
k = 0; yVal = 50;scale = 1e+6;
for i = 1:length(muscleR_sect)
    subplot(3,2,i+k); 
    plot(muscleR_sect{i}*scale); hold on
    plot(static_period_Del_emg, muscleR_sect{i}(static_period_Del_emg)*scale, 'k*')
    plot(FF_emg_R, muscleR_sect{i}(FF_emg_R)*scale, 'b*'); ylabel(muscleR_name{i});
    ylim([-yVal yVal]) 
    
    subplot(3,2,i+k+1); 
    plot(muscleL_sect{i}*scale); hold on
    plot(static_period_Del_emg, muscleL_sect{i}(static_period_Del_emg)*scale, 'k*')
    plot(FF_emg_L, muscleL_sect{i}(FF_emg_L)*scale, 'b*'); ylabel(muscleL_name{i});
    ylim([-yVal yVal]) 
    k = k +1;
end

% Remove baseline offset and abs
offsetRange = 1:round(length(static_period_Del_emg)/5);
for i = 1:length(muscleL)
    baseline_offset = mean(muscleR_sect{i}(static_period_Del_emg(offsetRange))); %this should be enough for offseting
    muscleR_sect_offset{i} = abs(muscleR_sect{i} - baseline_offset);
    figure; plot(muscleR_sect_offset{i})

    %left
    baseline_offset = mean(muscleL_sect{i}(static_period_Del_emg(offsetRange))); %this should be enough for offseting
    muscleL_sect_offset{i} = abs(muscleL_sect{i} - baseline_offset);
    figure; plot(muscleL_sect_offset{i})
end


f_cutoff = 3; N = 2;
Fs = 1/PERIOD_Del_emg;
Wn = [2*f_cutoff/Fs];
[B,A] = butter(N, Wn,'low');
for i = 1:length(muscleL)
    muscleL_sect_offset_filt{i} = filtfilt(B, A, muscleL_sect_offset{i});
    muscleR_sect_offset_filt{i} = filtfilt(B, A, muscleR_sect_offset{i});
end




%interpolate right
param = FF_emg_R;
range =  mean(diff(param));
for i = 1:length(muscleR)
    muscleR_sect_avg{i} = [];
    for j = 1 : length(param)-1
        muscleR_sect_avg{i} =  [muscleR_sect_avg{i} interpGaitCycle(muscleR_sect_offset_filt{i}(param(j):param(j+1),:), range)];
    end
    figure; plot(muscleR_sect_avg{i}, 'r', 'linewidth',2); title(muscleR_name{ii});
    muscleR_sect_avg{i} = mean(muscleR_sect_avg{i},2);
end


%interpolate left
param = FF_emg_L;
range =  mean(diff(param));
for i = 1:length(muscleL)
    muscleL_sect_avg{i} = [];
    for j = 1 : length(param)-1
        muscleL_sect_avg{i} =  [muscleL_sect_avg{i} interpGaitCycle(muscleL_sect_offset_filt{i}(param(j):param(j+1),:), range)];
    end 
    figure; plot(muscleL_sect_avg{i}, 'r', 'linewidth',2); title(muscleL_name{ii});
    muscleL_sect_avg{i} = mean(muscleL_sect_avg{i},2);
end


figure;
k = 0; yVal = 50;scale = 1e+6;
for i = 1:length(muscleR_sect)
    
    %right   
    tempEMG = muscleR_sect_avg{i}
    subplot(3,2,i+k); 
    plot(tempEMG*scale); hold on; ylabel(muscleR_name{i});
    
    %left
    tempEMG = muscleL_sect_avg{i};
    subplot(3,2,i+k+1); 
    plot(tempEMG*scale); hold on; ylabel(muscleL_name{i});
    k = k +1;
end

%%
% Normalize EMG to max signal
% EMG_norm = EMG_filt(:,m)/EMG_MVCmax;
    close all  
new_left_ab = left_Ab(7000:18400,3);
new_left_ab = interpGaitCycle(new_left_ab, sizeSignal); 

figure; hold on;
plot(EMG_filt*5e+5,'r');
plot(new_left_ab,'b')

[acor,lag] = xcorr(EMG_filt, new_left_ab);
[~,I] = max(abs(acor));
lagDiff = lag(I)

figure; hold on;
plot(EMG_filt(1:end)*5e+5,'r');
plot(new_left_ab(-lagDiff:end),'b')
%%
SECTION = [2480 2550 ]; %5840 5960 6020 6140    6200 6330  6350 6500  
[left_Wb,left_Ab] = getdata(left_Wb,left_Ab,PERIOD,SECTION);
[right_Wb,right_Ab] = getdata(right_Wb,right_Ab,PERIOD,SECTION);

%%
% Process data from the two IMUs simultaneously 
[left_walk_info,right_walk_info] = compute_pos_two_imus(left_Wb,left_Ab,right_Wb,right_Ab,PERIOD);

% Perform sperately
% left_walk_info = compute_pos(left_Wb,left_Ab,PERIOD);
% right_walk_info = compute_pos(right_Wb,right_Ab,PERIOD);

%%
% Segment steps
% out_L = [];%[23 26]
% out_R = out_L
% left_strides = stride_segmentation(left_walk_info,PERIOD, out_L);
% right_strides = stride_segmentation(right_walk_info,PERIOD, out_R);  
% 
% % Plot results for left and right foot
% figure;
% subplot(1,2,1);
% plt_ltrl_frwd_strides(left_strides,0);
% subplot(1,2,2);
% plt_ltrl_frwd_strides(right_strides,0);
% matchaxes;
% 
% figure;
% subplot(2,1,1)
% plt_frwd_elev_strides(left_strides,0);
% subplot(2,1,2)
% plt_frwd_elev_strides(right_strides,0);
% matchaxes;
% 
% figure;
% subplot(1,2,1)
% plt_stride_var(left_strides,0);
% subplot(1,2,2)
% plt_stride_var(right_strides,0);
% matchaxes;
% %end of APDM
