function [SECTION] = sync_indices(time, PERIOD, numFiles, SYNC, FORCE_SYNC_VALUE)

    if(SYNC)
        if(FORCE_SYNC_VALUE)
            % If a known value of the time shift is know, replace the variable FORCE_SYNC_VALUE 
            % with the correct value, you may use croscorrelations to find the correct shift value
            warning('Sync with user defined value');
            shift = FORCE_SYNC_VALUE;
        else
    
            max_time = 0;
            
            % Get the largest first index value from the time array of files
            for i = 1:numFiles
                if(time{i}(1) >= max_time)
                    max_time = time{i}(1);
                end
            end
            
            shift_val = max_time;
            
            for i = 1:numFiles
                shift{i} = find(time{i} >= shift_val, 1);
               
            end
            
        end
        
        for i = 1:numFiles
            SECTION{i} = [shift{i}, size(time{i}, 1)] * PERIOD;
        end
    
    else
            warning('Not Syncing');
    end
end
