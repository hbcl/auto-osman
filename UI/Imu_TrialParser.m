
%% Imu_TrialParser %%
% Author Anthony Killick
% Parameters
% trials_in - trials already selected
% data - Data from Imu in the "ImuData" object format
% plots - 1 X 'X' array of names of imus imported
% x1 - trial start index
% x2 - trial end index
function trials_out = Imu_TrialParser(trials_in, data, plots, x1, x2, params)
%%%%%%%%%%%%%%%%%%%% BEGIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%
%%  Set up Window
screensize = get(groot, 'Screensize');
winsize = screensize .* [ 1, 1, 0.85, 0.85 ];  % Adjust slightly smaller than screen
f = figure('Visible','off', 'ToolBar', 'none', 'Position', winsize);
centerfig(f);
trialEndOutlier = params.trial_begin_outlier/data.SamplePeriod; %Should be editable by the user
trialBeginOutlier = params.trial_end_outlier/data.SamplePeriod; %Should be editable by the user
%Store temporary saves while user is working
trials_temp = [];
%set(f,'CloseRequestFcn', @closefig);

%%  Generate Plots
plotPanel = uipanel('Parent', f ,'Position',[0, 0.2, 1, 0.8]);
[subplt1,subplt2] = data.plotWithSelectData(plotPanel, plots, x1, x2);

%Linking subplot axes on the x axis synchronizes zooming on the x axis
linkaxes([subplt1, subplt2], 'x');

%%  Trial information panel
infoPanel = uipanel('Parent', f, 'Position', [0.2, 0, 0.8, 0.2]);
trialFieldNames = {'Trial Name', 'Start Time', 'End Time', 'Notes'};
trialFieldHandles = inputFields(infoPanel, trialFieldNames);

%%  Buttons:
buttonPanel =  uipanel('Parent', f,                   ...
                        'Position', [0, 0, 0.2, 0.2]   ...
                        );
nButton = 1/4; % 1/number of buttons
autoSelect = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',          ...
                        'String', 'Auto Trial Detect', 'Units', 'normalized',  ...
                        'Position',[0, 0 * nButton, 1, nButton],               ...
                        'Callback',@autoTrialDetect_Callback                   ...
                        );

updateTrial = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',          ...
                        'String', 'Save changes' ,         ...
                        'Units', 'normalized',                               ...
                        'Position',[0, 1 * nButton, 1, nButton],             ...
                        'Callback', @updateTrial_Callback                   ...                                     ...
                        );

selectData = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',        ...
                        'String', 'Select Data', 'Units', 'normalized',      ...
                        'Position',[0, 2 * nButton, 1, nButton],             ...
                        'Callback', @selectData_Callback                     ...
                        );
deleteTrial = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',        ...
                        'String', 'Delete Trial', 'Units', 'normalized',      ...
                        'Position',[0, 3 * nButton, 1, nButton],              ...
                        'Callback', @deleteTrial_Callback,                    ...
                        'Enable', 'off'                                       ...
                        );

%%  Containers for trial data
trials = trials_in; %struct created on first selection
static = trials 
trialHandles = {}; 
minTrialLengthSeconds = params.trial_minimum_length;
trialMinLength = minTrialLengthSeconds / data.SamplePeriod;
numTrials = 0; % keeps track of number of trials created to id them in the
                    % cell array. When trials are deleted, this remains unchanged

f.Name = 'Trial Parser';
f.Visible = 'on';

%%  Handles stored in struct when axes plotted:
handles =struct();  
selectInd = 0; %currently selected trial number

initialSetup();
% TODO: static sections... auto find, plot, change vals replot
%%  Callbacks and functions
function initialSetup()
    for i= 1:length(trials)
        plotTrial(i);
    end
end
function autoTrialDetect_Callback(~,~)
    static = data.StaticSamples(data.StaticSamples > x1 & ...
                                data.StaticSamples < x2);
    for i = 1:length(static)-1
%         TrialMinLength should be set by a user to figure out how long a
%         trial must be to count as a trial
        if (static(i+1) - static(i)) > trialMinLength
            x1= (static(i) - trialBeginOutlier) * data.SamplePeriod;
            x2= (static(i+1) - trialEndOutlier) * data.SamplePeriod;
            makeTrial(x1,x2);
        end
    end
end
function makeTrial(x1, x2)
    numTrials = numTrials + 1;
    new = size(trials,2) + 1;
    trials(new).id = numTrials;
    trials(new).name = strcat('Trial ', num2str(numTrials));
    trials(new).startTime = x1;
    trials(new).endTime = x2;
    trials(new).notes = 'Enter any notes here';
    trials(new).trials = []; % section editor makes this in to a struct array of trials
    plotTrial(new)
end
    
function plotTrial(trialInd)
    ax = [subplt1,subplt2];
    x1 = trials(trialInd).startTime;
    x2 = trials(trialInd).endTime;
    for i= [1,2]
        axes(ax(i))
        hold on;
        y = ylim;
        trialHandles{trialInd}(i) = fill([x1, x1, x2, x2], ...
                [y(1), y(2), y(2), y(1)], 'g',               ...
                'EdgeColor', 'none', 'FaceAlpha', 0.2,      ... 
                'ButtonDownFcn', @(~,~)clickTrial_Callback(trials(trialInd).id));
    end
end

function clickTrial_Callback(id)
    saveTempData();
    if selectInd ~= 0 
        for i = [1,2]
            set(trialHandles{selectInd}(i), 'EdgeColor', 'none');
        end
    end
    ind = find([trials.id]==id);
    selectInd = ind;   % alter selectInd at the scope of the gui
    for i = 1:length(trialHandles{selectInd})
        set(trialHandles{selectInd}(i), 'EdgeColor', 'k');
    end
    % enable editing and put info in fields UI
    for i = 1: length(trialFieldHandles)
        set(trialFieldHandles(i), 'Enable', 'on');
    end
    set(updateTrial, 'Enable', 'on');
    set(deleteTrial, 'Enable', 'on');
    loadTrialVals();
end

function loadTrialVals()
    set(trialFieldHandles(1), 'String', trials(selectInd).name);
    set(trialFieldHandles(2), 'String', trials(selectInd).startTime);
    set(trialFieldHandles(3), 'String', trials(selectInd).endTime);
    set(trialFieldHandles(4), 'String', trials(selectInd).notes);
end

function saveTempData()
    if selectInd ~= 0
        x1 = str2double(get(trialFieldHandles(2),'String'));
        x2 = str2double(get(trialFieldHandles(3),'String'));
        trials(selectInd).name = get(trialFieldHandles(1), 'String');
        trials(selectInd).startTime = x1;
        trials(selectInd).endTime = x2;
        trials(selectInd).notes = get(trialFieldHandles(4), 'String');
    end
end

function deleteTrial_Callback(~,~)
    % delete fill objects and handles
%     trialHandles(selectInd) = [];
        for i = 1: length(trialHandles{ selectInd })
            delete(trialHandles{ selectInd }(i))
        end
      trialHandles(selectInd) = [];
    % delete section and section information
    trials(selectInd) = [];
    % set selectInd to 0
    selectInd = 0;
    % turn off text fields
    for i = 1: length(trialFieldHandles)
        set(trialFieldHandles(i), 'Enable', 'off');
    end
    set(deleteTrial, 'Enable', 'off');
    set(updateTrial, 'Enable', 'on');
end

function selectData_Callback(~,~)
    set(updateTrial, 'Enable', 'on');
    axes(subplt1);
    rect = getrect();
    disp(rect)
    x1 = rect(1);
    x2 = x1+ rect(3);
    makeTrial(x1,x2);
end

function updateTrial_Callback(~,~)
    if(selectInd ~= 0)
        x1 = str2double(get(trialFieldHandles(2),'String'));
        x2 = str2double(get(trialFieldHandles(3),'String'));
        trials(selectInd).name = get(trialFieldHandles(1), 'String');
        trials(selectInd).startTime = x1;
        trials(selectInd).endTime = x2;
        trials(selectInd).notes = get(trialFieldHandles(4), 'String')
    % Display altered times:
        for i = 1: length(trialHandles{selectInd})
            new = get(trialHandles{selectInd}(i), 'Vertices');
            new(1,1) = x1; new(2,1) = x1; new(3,1) = x2; new(4,1) = x2;
            set(trialHandles{selectInd}(i), 'Vertices', new);
        end
    end
    trials_temp = trials
end

uiwait()
trials_out = trials_temp
%%%%%%%%%%%%%%%%%%%% END FUNCTION %%%%%%%%%%%%%%%%%%%%%%%
end
