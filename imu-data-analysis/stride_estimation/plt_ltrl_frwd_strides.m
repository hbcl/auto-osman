% Author: Lauro Ojeda, 2011-2015
function plt_ltrl_fwrd_strides(strides, plot_params, CREATE_NEW_FIGURE, USE_SWING_ONLY)
  SHOW_ENDPOINTS = plot_params.show_endpoints;
  SHOW_NUMBER = plot_params.show_number;
  QUICK_PLOT = plot_params.quick_plot;

  if(~exist('CREATE_NEW_FIGURE', 'var') | CREATE_NEW_FIGURE==1)
    figure;
  end;

  hold on;

    if exist('USE_SWING_ONLY', 'var') & USE_SWING_ONLY
      ltrl = strides.ltrl_swing;
    else
      ltrl = strides.ltrl;
    end

    if QUICK_PLOT
      plot(ltrl, strides.frwd);
      if SHOW_ENDPOINTS
        plot( ltrl(end, :), strides.frwd(end, :), 'ko', ...
              'MarkerEdgeColor', 'k', ...
              'MarkerSize', 5);
      end

    else
      n_steps = size(ltrl, 2);
      colors = jet(n_steps);

      for step_no = 1:n_steps
        plot( ltrl(:, step_no), strides.frwd(:, step_no), ...
              'Color', colors(step_no, :));

        if SHOW_ENDPOINTS
          plot( ltrl(end, step_no), strides.frwd(end, step_no), 'ko', ...
                'MarkerEdgeColor', 'k', ...
                'MarkerFaceColor', 'k', ...
                'MarkerSize', 5);
        end

        if SHOW_NUMBER
          text( ltrl(end, step_no) + .001, strides.frwd(end, step_no), ...
                num2str(step_no));
        end
      end
    end

    grid on;
    ylabel('Frwd [m]');
    xlabel('Ltrl [m]');

  hold off;
end
