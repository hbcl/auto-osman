classdef StationaryPeriodParams
  % Parameters for the definition of stationary periods (that is, periods of
  % low movement) in raw IMU data.

  properties
    % Upper threshold for the angular velocity magnitude (deg/s) for "stationary"
    % points
    foot_fall_angular_velocity_maximum; % deg/s

    % Upper threshold for the difference in magnitudes between the linear
    % acceleration and gravity (m/s^2) for "stationary" points.
    foot_fall_linear_acceleration_maximum; % m/s^2
  end

  methods
    % Default values
    function obj = StationaryPeriodParams(obj)
      obj.foot_fall_angular_velocity_maximum = 30.0;
      obj.foot_fall_linear_acceleration_maximum = 1.0;
    end
  end
end
