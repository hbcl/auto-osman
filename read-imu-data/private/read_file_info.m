
function [num_files, file_name, file_info, time, FREQ] = read_file_info(FILES)
    %Size of the file array
    num_files = size(FILES, 2);


    for i=1 : num_files
        %Convert filename from string to character array     
       file_name{i} = char(FILES{i});
       version = read_apdm_version(FILES{i}{1});       
       %get file info and store in info array
       file_info{i} = h5info(char(FILES{i}));   

        if(version == 2 || version == 3)
		    caseIdList = h5readatt(file_name{i}, '/', 'CaseIdList');
            caseIdList = deblank(strsplit(caseIdList{1}, ']'));
            caseIdList = caseIdList{1};
            groupName = caseIdList;
            time{i} = h5read(file_name{i}, ['/' groupName '/Time']);
    		FREQ = h5readatt(file_name{i}, ['/' groupName], 'SampleRate');
        else
           %get time from file and store in time array
           time{i} = h5read(file_name{i}, [file_info{i}.Groups(2).Groups(1).Name, '/Time']);
            %Get sampling rate from a file and calulate the frequency
            FREQ  = h5readatt(file_name{1}, file_info{1}.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
        end
    end
