function out = export_osman(projectInfo, imu, sections)
    out = true;
    vertical_elements = 3 + length(sections);
    element_height = 75;
    text_height = 30;
    box_height = vertical_elements * element_height + 50;
    box_top = 800;
    box_bottom = box_top - box_height;
    box_left = 360;
    box_width = 550;
    left_margin = 100;
    label_width = 120;
    input_width = 100;
    right_margin = 30;
    offset = right_margin + left_margin;
    file_selection_width = 150;
    num_imus = 1;
    trial_types = {'normal', 'longUneven', 'pyramid'};
    imu_trial_types_control = [];
    imu_trial_types = [];
    
    d = dialog('Visible','on',...
        'Name', 'Export for Osman', ...
        'Position', [box_left, box_bottom, box_width, box_height]);
    
    %Descriptor for labelling trials
    uicontrol('Parent',d,...
               'Style','text',...
               'Position',[100 (box_height-40) 400 40],...
               'String','Please choose the corresponding trial type on the right for the section on the left.');
    
    %create dialog for each section
    for j=1:length(sections)
         uicontrol('Parent',d,...
               'Style','text',...
               'Position',[100 (box_height-25 - (50*j)) label_width 30],...
               'String', sections(j).name);

        imu_trial_types_control{j} = uicontrol('Parent', d, 'Style', 'popupmenu', ...
            'String', trial_types, ...
            'Position', [300, (box_height - 25 - (50*j)), label_width, 30]);
    end
    
    %Descriptor for choosing imus
    uicontrol('Parent',d,...
               'Style','text',...
               'Position',[100 230  400 40],...
               'String','Please choose the imu you have imported from the dropdown on the right which corresponds with the label on the left.');
    
    uicontrol('Parent',d,...
               'Style','text',...
               'Position',[100 200 label_width 30],...
               'String','LEFT FOOT');

    left_foot = uicontrol('Parent', d, 'Style', 'popupmenu', ...
        'String', projectInfo.plots , ...
        'Position', [300, 200, label_width, 30]);
    
    
    uicontrol('Parent',d,...
               'Style','text',...
               'Position',[100 150 label_width 30],...
               'String','RIGHT FOOT');
           
    right_foot = uicontrol('Parent', d, 'Style', 'popupmenu', ...
        'String', projectInfo.plots , ...
        'Position', [300, 150, label_width, 30]);
    
     uicontrol('Parent',d,...
               'Style','text',...
               'Position',[100 100 label_width 30],...
               'String','WAIST');
           
     waist = uicontrol('Parent', d, 'Style', 'popupmenu', ...
        'String', projectInfo.plots , ...
        'Position', [300, 100, label_width, 30]);
    
       uicontrol('Parent',d,...
               'Style','text',...
               'Position',[100 50 label_width 30],...
               'String','HAND');
    
    hand = uicontrol('Parent', d, 'Style', 'popupmenu', ...
        'String', projectInfo.plots , ...
        'Position', [300, 50, label_width, 30]);
    
     % OK
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'OK', ...
        'Position', [200, 0, 70, 30] , ...
        'Callback', @ok_Callback);

    % Cancel
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'Cancel', ...
        'Position', [280, 0, 70, 30] , ...
        'Callback', @cancel_Callback);
    
    
function ok_Callback(~,~)
        imuData = [];
        for n = 1:length(sections)
            imuData.(strcat('gr',num2str(n))).Left_Wb = {imu.Wb{left_foot.Value}(int32(sections(n).startTime/ imu.SamplePeriod) : int32(sections(n).endTime/imu.SamplePeriod), :)};
            imuData.(strcat('gr',num2str(n))).Right_Wb = {imu.Wb{right_foot.Value}(int32(sections(n).startTime/ imu.SamplePeriod) : int32(sections(n).endTime/imu.SamplePeriod), :)};      
            imuData.(strcat('gr',num2str(n))).Left_Ab = {imu.Ab{left_foot.Value}(int32(sections(n).startTime/ imu.SamplePeriod) : int32(sections(n).endTime/imu.SamplePeriod), :)};      
            imuData.(strcat('gr',num2str(n))).Right_Ab = {imu.Ab{left_foot.Value}(int32(sections(n).startTime/ imu.SamplePeriod) : int32(sections(n).endTime/imu.SamplePeriod), :)};
            imuData.(strcat('gr',num2str(n))).Hand_Wb = {imu.Wb{hand.Value}(int32(sections(n).startTime/ imu.SamplePeriod) : int32(sections(n).endTime/imu.SamplePeriod), :)};      
            imuData.(strcat('gr',num2str(n))).Hand_Ab = {imu.Ab{hand.Value}(int32(sections(n).startTime/ imu.SamplePeriod) : int32(sections(n).endTime/imu.SamplePeriod), :)};
        end
        data = [];
        PERIOD = imu.SamplePeriod;
        data.name = projectInfo.Name;
        data.date = projectInfo.Date;
        data.approxTrialDuration = 15;
        MAX_TRIAL_NUM = 100;                        %Need to add fields to set approx trial duration
        data.approxTrialSampleNum = data.approxTrialDuration / PERIOD;
        for i = 1:3
        [data.trialEndOutlier{i}(1:MAX_TRIAL_NUM)] =  deal(2/PERIOD); %samples
        [data.trialBeginOutlier{i}(1:MAX_TRIAL_NUM)] =  deal(18/PERIOD); %samples
        end
            
        data.groupIndices = []; %Done
        data.savedTrials = [];
        data.badTrials = [];
        data.startGrIndex = []; %maybe unneccasay - used as a number in a for loop
        
        data.endGrIndex = [];   %maybe unneccasary
        
        temp = 0
        for i=1:length(sections)
            indices = (temp + 1):(temp + length(sections(i).trials));
            temp = temp + length(sections(i).trials);
            data.groupIndices{i} = indices;
            data.startGrIndex{i} = 1;
        end
        
        for i=1:length(imu_trial_types_control)
           imu_trial_types{i} = trial_types{imu_trial_types_control{i}.Value}; 
        end
        
        data.trialIndex = [];   %
        data.trialTypeString = [];
        for i=1:length(sections)
            for j=1:length(sections(i).trials)
                trial = {[sections(i).trials(j).startTime - sections(i).startTime, sections(i).trials(j).endTime - sections(i).startTime]};
                data.trialIndex = [data.trialIndex trial];
                data.trialTypeString = [data.trialTypeString imu_trial_types(i)];
            end
        end
        
        
                
%         data.trialTypeString = [];  %
        
        [file,path] = uiputfile;
        save(strcat(path, file), 'imuData', 'data');
        delete(d)
    end

    function cancel_Callback(~,~)
        delete(d)
    end
    
end