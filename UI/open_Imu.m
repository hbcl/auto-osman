function [wd, files, imu_names, selection_complete]=open_Imu()
%OPEN_IMU Opens Imus from .h5 files
%   Intended to open 4 different data streams from IMUs and 
%   sync them according to timestamp 
    selection_complete = false;
    wd = '';
    vertical_elements = 3; 
    element_height = 75;
    text_height = 30;
    box_height = vertical_elements * element_height;
    box_top = 800;
    box_bottom = box_top - box_height;
    box_left = 360;
    box_width = 550;
    left_margin = 100;
    input_width = 100;
    right_margin = 30;
    offset = right_margin + left_margin;
    file_selection_width = 150;
    num_imus = 1;
    
    %$Create dialog for user to choose imus to load
    d = dialog('Visible','off',...
        'Name', 'Select Imus to Parse', ...
        'Position', [box_left, box_bottom, box_width, box_height]);

    imus = {};
    files = {};
    imu_names = {''};

    %Offset number for position of add ium element
    file = 1;

    imus = [uicontrol('Parent', d, 'Style', 'edit', ...
        'String', 'IMU', ...
        'Position', [left_margin, box_height - (element_height), input_width, text_height]), ...
        uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', '<File Name>', ...
        'Position', [left_margin + offset, box_height - (element_height), file_selection_width, text_height], ...
        'Callback', {@selectFile_Callback, file})];
    files = [files; '<File Name>'];

    %Add an imu
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'Add imu' , ...
        'Position', [200, 300 - 3*70, 70, 30], ...
        'Callback', {@addIMU_callback});

    % OK
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'OK', ...
        'Position', [200, 300 - 4*70, 70, 30] , ...
        'Callback', @ok_Callback);

    % Cancel
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'Cancel', ...
        'Position', [280, 300 - 4*70, 70, 30] , ...
        'Callback', @cancel_Callback);

    %Move dialog box to the top and make it visible
    movegui(d,'north')
    d.Visible = 'on';

    uiwait(d)

   %%---------------------------------- Callbacks -----------------------------------%%
    function selectFile_Callback(~, ~,i)
      disp(i);
      [fn,pn] = uigetfile('*.h5');
      wd = pn;
      addpath(pn);
      files{i} =  fn;
      
      disp(files{i});
      update_imu_elements(imus, box_height);
    end

    function addIMU_callback(~,~)
        vertical_elements = vertical_elements + 1;
        box_height = vertical_elements * element_height;
        box_bottom = box_top - box_height;
        temp_name = 'IMU' + vertical_elements; 
        num_imus = num_imus + 1;
        file = file + 1;
        imus = [imus; [uicontrol('Parent', d, 'Style', 'edit', ...
            'String', 'IMU', ...
            'Position', [left_margin, box_height - (element_height), input_width, text_height]), ...
            uicontrol('Parent', d, 'Style', 'pushbutton', ...
            'String', 'Select file', ...
            'Position', [left_margin + offset, box_height - (element_height), file_selection_width, text_height], ...
            'Callback', {@selectFile_Callback, file})]];
        files = [files, '<File Name>'];
        update_imu_elements(imus, box_height);
        d.Position(4) = box_height;
        movegui(d, 'north');

    end
        

    function ok_Callback(~,~)

        files = {};
        imu_names = {};
        disp(length(imus));
        for i=1: size(imus, 1)
            imu_names{i} = imus(i, 1).String;
            files{i} = imus(i, 2).String;
        end
        selection_complete = true;
        delete(d)
    end

    function cancel_Callback(~,~)
        delete(d)
    end

    function update_imu_elements(imus, height)
        for i= 1:size(imus, 1)
            imus(i, 1).Position(2) = (height - (element_height * i));
            imus(i, 2).Position(2) = (height - (element_height * i));
        end
        update_file_names(imus);
    end

    function update_file_names(imus)
       for i = 1:size(imus, 1)
           imus(i, 2).String = files(i);
       end
    end


end

