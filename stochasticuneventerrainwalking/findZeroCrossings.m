function  zc = findZeroCrossings(data)

 zc =  [];

for i = 1:length(data)-1
    if data(i)*data(i+1) < 0 || (data(i) == 0)
        zc =  [zc i];
    end
end