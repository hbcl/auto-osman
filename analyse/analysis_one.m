function analysis_one(imu, imuChoice, SECTION, imuAnalysisParameters)

        addpath('../imu-data-analysis/stride_estimation');
        % Define the section (in seconds) that will need to be processed and segment the IMU data accordingly
        % SECTION = [240,350];
    
        [Wb,Ab] = slicedata(imu.Wb{imuChoice},imu.Ab{imuChoice},imu.SamplePeriod,SECTION);

        % Perform inertial mechanization
        walk_info = integrate_single_foot_imu( ...
            Wb, Ab, imu.SamplePeriod, ...
            imuAnalysisParameters.stationary_period_params, ...
            imuAnalysisParameters.foot_fall_detection_params, ...
            imuAnalysisParameters.tilt_correction_kalman_filter_params);

        % Segment strides only for the walking period
        strides = stride_segmentation( ...
            walk_info, imu.SamplePeriod, ...
            imuAnalysisParameters.step_filter_params);

        % Plot results
        plt_ltrl_frwd_strides(strides, imuAnalysisParameters.plot_params);
        plt_frwd_elev_strides(strides, imuAnalysisParameters.plot_params);
        plt_stride_var(strides, imuAnalysisParameters.plot_params)
end
