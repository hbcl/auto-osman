% Author: Lauro Ojeda, 2011-2015


DIR = 'D:\spare\stochasticTerrainUnevenWalking\somedata\FarukUneven\';
addpath('C:\Users\odarici\Desktop\spare\SteppingOnFoamImu\stride_estimation');

L_FILE = '20180312-220802_pilotUnevenFaruk_sensor_1378_label_Left.h5';
R_FILE = '20180312-220800_pilotUnevenFaruk_sensor_1390_label_Right.h5';

% L_FILE = '20180312-220443_pilotUnevenFarukOldApdm_sensor_780_label_Left.h5';
% R_FILE = '20180312-220437_pilotUnevenFarukOldApdm_sensor_1027_label_Right.h5';

close aLL
newApdm = 1

%% Two APDM IMUs data processing simultaneously
% Load IMU information from a file
if newApdm
    [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_New_apdm(L_FILE, R_FILE );
else
    [left_Wb,left_Ab,right_Wb,right_Ab,PERIOD] = sync_apdm(L_FILE, R_FILE );
end
% Plot signals to help determining the SECTION that corresponds to the experiment
% getdata(left_Wb,left_Ab,PERIOD);

% Define the section (in seconds) that will need to be processed and segment the IMU data accordingly
SECTION = [980 1040];
% SECTION = [900 1000];
[left_Wb,left_Ab] = getdata(left_Wb,left_Ab,PERIOD,SECTION);
[right_Wb,right_Ab] = getdata(right_Wb,right_Ab,PERIOD,SECTION);

% Process data from the two IMUs simultaneously 
[left_walk_info,right_walk_info] = compute_pos_two_imus(left_Wb,left_Ab,right_Wb,right_Ab,PERIOD);

% Segment steps
left_strides = stride_segmentation(left_walk_info,PERIOD);
right_strides = stride_segmentation(right_walk_info,PERIOD);

% Plot results for left and right foot
figure;
subplot(1,2,1);
plt_ltrl_frwd_strides(left_strides,0);
subplot(1,2,2);
plt_ltrl_frwd_strides(right_strides,0);
matchaxes;

figure;
subplot(2,1,1)
plt_frwd_elev_strides(left_strides,0);
subplot(2,1,2)
plt_frwd_elev_strides(right_strides,0);
matchaxes;

figure;
subplot(1,2,1)
plt_stride_var(left_strides,0);
subplot(1,2,2)
plt_stride_var(right_strides,0);
matchaxes;

