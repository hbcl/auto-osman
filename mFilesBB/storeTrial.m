function data = storeTrial(data, trial, trialNumber, targetDirect, type)

    data.savedTrials = [data.savedTrials trialNumber];
    currentFolder = cd;
    cd(targetDirect);   
    data.(data.trialTypeString{trialNumber}).trial{trialNumber} = trial;
    save(strcat(data.name,'_', type, 'Segmented'),'data'); 
    cd(currentFolder);
    fprintf('%d. trial saved ********\n', trialNumber); 