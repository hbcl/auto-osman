%% inputFields.m %%
% Author: Anthony Killick
% Quickly adds text input fields to a parent axis
% The number of input fieds is the length of the list of names of the fields
% Parameters:
%    parentHandle: handle of parent graphics object in which to embed input fields
%    names: cell array of names for input fields

function handles = inputFields(parentHandle, names)

     refAxes = axes('Parent', parentHandle, 'Position', [0,0,1,1], ...
                'Visible', 'off');

      for i= 1:length(names)
        
        corr = 0.2 * (i-1);
        text('Parent', refAxes,                       ... 
                   'String', names(i),                ...
                   'Position', [0,  0.9 - corr]       ...
                   );
         handles(i) = uicontrol('Parent', parentHandle, 'Style', 'edit',  ... 
                   'String', 'Enter value here', 'Units', 'normalized',   ...
                   'Position', [0.1, 0.8 - corr, 0.5, 0.2],               ...
                   'Enable', 'off'                                        ...
                   );
      end

end