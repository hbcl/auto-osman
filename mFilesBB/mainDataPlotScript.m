%% OSMAN DARICI 2018

clear; clc; %close all;

% name = {'DYpilot'}
% targetDirect = 'D:\spare\stochasticTerrainUnevenWalking\DYPliot1';
name = {'joel'}%{'FarukPilot','LuciaPilot'};%{'FarukPilot'}{'DYpilot2'}
experimentName = {'pyramid'}
trialType = {'pyramid'}%{'pyramid'}
targetDirect = strcat('D:\spare\stochasticTerrainUnevenWalking\somedata\', experimentName{1});%, name{1});
figDirect = strcat(targetDirect, '\figs');
addpath(genpath(targetDirect));
% modelFigDirect = 'D:\spare\stochasticTerrainUnevenWalking\modelFigs'
% addpath(modelFigDirect)



%%

%  longUneven4inchB 'pyramid_short', 'Uneven1_2', 'Uneven2_1', 'Uneven2_2'};% oU_noConstraintNotLabel  oD2FFsBump_noConstraintWithLabel oU_noConstraintWithLabel  oD_noConstraintWithLabel 'Normal_NoConstraint
UnevenStepNum = 6
figMeanInit = 0;
colors  = jet(length(trialType));
clear data dataTS
str = strcat('_', experimentName, 'Segmented');
% str = '_pyramidSegmented';
dataSaveStr = str{1};
storeMeans = false;%true;
for nameInd = 1 : length(name)   
    clear data;
    load (strcat(name{nameInd}, str{1})); %load (name{nameInd});
    dataTS{nameInd} = data;                            %clear data;  to plot different data with same type you need to do trialType{nameInd} below and clear data at his line
    

    for trialTypeInd = 1 : length(trialType)        
        [figMean figMeanH] = plotNanalyseStrides(dataTS{nameInd},...
                                          'trialType', trialType{trialTypeInd}, 'storeMeans', storeMeans, 'figMean', figMeanInit,...
                                          'colorInput', colors(trialTypeInd, :),'targetDirect', targetDirect, 'UnevenStepNum', UnevenStepNum, 'dataSaveStr', dataSaveStr); %trialTypeInd
        figMeanInit = figMean;
    end
    
    figName = strcat(name, trialType{nameInd});
    saveOneFigure(figMeanH, figName{1}, figDirect);

end

% speed_str = 'HallwayExperimentApprox1_50'
% numPoBf = 2
% xx2 = strcat('imuSpeed2_traj_', trialType, '_', speed_str, '_', num2str(numPoBf));
% openfig(xx2{1});
% ylimTemp = ylim;
% xlimTemp = xlim;
% text(xlimTemp(2)/2, ylimTemp(1)*1.15,trialType)
% xx2 = strcat('AngularVel2_traj_', trialType, '_', speed_str, '_', num2str(numPoBf));
% 
% openfig(xx2{1});
% ylimTemp = ylim;
% xlimTemp = xlim;
% text(xlimTemp(2)/2, ylimTemp(1)*1.15,trialType)

%%
clc;
clear; close all;
experimentName = {'longUneven'}%{'pyramid'}
name = {'shaine', 'kyle', 'sam', 'alex','joel'}%{'FarukPilot', 'LuciaPilot', 'DYpilot2'}; %    
clear dataTS   data
str = strcat('_', experimentName, 'Segmented');
for i = 1:length(name)
   load (strcat(name{i}, str{1})); 
   dataTS{i} = data; clear data;
end
%%
% close all

trialType = {'longUnevenA', 'longUnevenB'}%{'Uneven1_1', 'Uneven1_2', 'Uneven2_1', 'Uneven2_2'};
% {'pyramid'}%
% multiple figure or data to save
fig_direct = 'D:\spare\stochasticTerrainUnevenWalking\somedata\avgs';
saveAvgDataFlag = 1

for i = 1:length(trialType)
    trialType{i}
    [dataOut] = plotAvgNew('trialType', trialType{i}, dataTS); 
    
    if saveAvgDataFlag
        figname = strcat('Avg', trialType{i});
    
        data_name = strcat('dataAvg', trialType{i})
       
        
        currentFolder = cd;
        cd(fig_direct);   
        saveas(gcf, figname, 'fig');
        save(data_name,'dataOut')
%         saveas(gcf, figname{1}, 'fig');
        fprintf(strcat(trialType{i},' Avg data saved \n'));
        cd(currentFolder); 
    end
end


