classdef ImuData
    %IMUDATA Summary of this class goes here
    %   Detailed explanation goes here

    properties
        numImus
        Wb
        Ab
        Mb
        LeftWb
        LeftAb
        RightWb
        RightAb
        HandWb
        HandAb
        WaistWb
        WaistAb
        StaticSamples
        SamplePeriod
    end

    methods
        function [h] = plotData(obj, ax, i)
                h = plotRaw(ax, obj.StaticSamples, obj.SamplePeriod, obj.Wb{i}, obj.Ab{i});
        end

        function  [s1,s2] =plotWithSelectData(obj, parentHandle, plotNames, x1, x2)
            
            t = (1:size(obj.Wb{1},1)) * obj.SamplePeriod; % Time
            static = obj.StaticSamples(obj.StaticSamples > x1 & ...
                                       obj.StaticSamples < x2);
            zeroline = zeros(size(obj.Wb{1}));
            flags=zeroline;
            flags(static) = 1;
            flags = diff(flags) * 10;
            checkBoxPanel = uipanel('Parent', parentHandle,     ...
                                    'Position', [0.8,0,0.2,1]   ...
                                    );
            plotPanel = uipanel('Parent', parentHandle,     ...
                                'Position', [0,0,0.8,1]     ...
            );

            wPlt = {};
            aPlt = {};
            for i = 1: length(plotNames)
                axes(plotPanel)
                s1 = subplot(2,1,1); % angular accel
                hold on;
                wPlt{i} = plot(t(x1:x2), obj.Wb{i}(x1:x2,:)*180/pi, ...
                               'Visible', 'on');
                ylabel('Angular Accel [deg/s]')
                s2 = subplot(2,1,2); % accel 
                hold on;
                aPlt{i} = plot(t(x1:x2), obj.Wb{i}(x1:x2,:), ...
                               'Visible', 'on   ');
                ylabel('Acceleration [m/s^2]');
                % Checkbox for plot
                chk(i) = uicontrol('Parent', checkBoxPanel,             ...
                                'style', 'checkbox',                    ...
                                'Value', 1,                              ...
                                'units', 'normalized',                  ...
                                'Position',[0, 1-(0.2*i), 1, 0.2],      ...
                                'String', plotNames(i),                 ...
                                'Callback', {@chkChanged,i}             ...
                                 );
            end
            plot (s1, t(static), zeroline(static),'k.')
            % Callback for each checkbox:
            function chkChanged(src, ~, i)
                if src.Value
                    state = 'on';
                else 
                    state = 'off';
                end
                disp(wPlt{i})
                for line = 1: length(wPlt{i})
                    wPlt{i}(line).Visible = state;
                    aPlt{i}(line).Visible = state;
                end
            end

        end
    end
end