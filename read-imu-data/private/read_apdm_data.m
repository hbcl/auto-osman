% Author: Lauro Ojeda, 2012-2015
function [W,A,PERIOD,M] = read_apdm_data(FILE,ORIENTATION)
% 	if(~exist('FILE','var')) FILE = 'data.h5'; end;
% 	caseIdList = hdf5read(FILE,'/CaseIdList');
% 	groupName = caseIdList(1).data;
% 	FREQ = hdf5read(FILE, [groupName '/SampleRate']);
% 	PERIOD = 1/double(FREQ);

	version = read_apdm_version(FILE);
	disp(version);
    info = h5info(FILE);
	
	if(version == 3) %%Old apdm models
		caseIdList = h5readatt(FILE, '/', 'CaseIdList');
		caseIdList = deblank(strsplit(caseIdList{1}, ']'));
		caseIdList = caseIdList{1};
		groupName = caseIdList;
		FREQ = h5readatt(FILE, ['/' groupName], 'SampleRate');
		a = transpose(h5read(FILE, ['/' groupName '/Calibrated/Accelerometers']));
		w = transpose(h5read(FILE, ['/' groupName '/Calibrated/Gyroscopes']));
		m = transpose(h5read(FILE, ['/' groupName '/Calibrated/Magnetometers']));
		time = h5read(FILE, ['/' groupName '/Time']);
	else %%New apdm models
		disp("New Version");
 	   FREQ  = h5readatt(FILE, info.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
    	a = (h5read(FILE, [info.Groups(2).Groups(1).Name,'/Accelerometer']))';
		w = (h5read(FILE, [info.Groups(2).Groups(1).Name,'/Gyroscope']))';
    	m = (h5read(FILE, [info.Groups(2).Groups(1).Name,'/Magnetometer']))';
    	time = h5read(FILE, [info.Groups(2).Groups(1).Name,'/Time']);   
	end

    PERIOD = 1/double(FREQ);
    
	ORIGINAL = 0;
	LED_UP_RIGHT_FRWD = 1; % This one is normally used on feet
	LED_UP_LEFT_FRWD = 2;
	LED_LOW_RIGHT_FRWD = 3;
	LED_LOW_DOWN_FRWD = 4;
	LED_UP_RIGHT_BACK = 5; 
	LED_LOW_LEFT_BACK = 6;
	if(~exist('ORIENTATION','var')) ORIENTATION = LED_UP_RIGHT_FRWD; end;
	switch(ORIENTATION)
		case ORIGINAL 
			% +X  <---o +Z
			%         |
			%         V +Y
			WX = w(:,1); WY = w(:,2); WZ = w(:,3);
			AX = a(:,1); AY = a(:,2); AZ = a(:,3);
		case LED_UP_RIGHT_FRWD
			WX = w(:,2); WY = w(:,1); WZ = -w(:,3);
			AX = a(:,2); AY = a(:,1); AZ = -a(:,3);
		case LED_UP_LEFT_FRWD
			WX = w(:,1); WY = -w(:,2); WZ = -w(:,3);
			AX = a(:,1); AY = -a(:,2); AZ = -a(:,3);
		case LED_LOW_DOWN_FRWD
			WX = -w(:,2); WY = -w(:,1); WZ = -w(:,3);
			AX = -a(:,2); AY = -a(:,1); AZ = -a(:,3);
		case LED_LOW_RIGHT_FRWD
			WX = -w(:,1); WY = w(:,2); WZ = -w(:,3);
			AX = -a(:,1); AY = a(:,2); AZ = -a(:,3);
		case LED_UP_RIGHT_BACK
			WX = w(:,3); WY = -w(:,1); WZ = w(:,2);
			AX = a(:,3); AY = -a(:,1); AZ = a(:,2);
		case LED_LOW_LEFT_BACK
			WX = w(:,3); WY = w(:,1); WZ = -w(:,2);
			AX = a(:,3); AY = a(:,1); AZ = -a(:,2);
	end;

	W = [WX,WY,WZ]*PERIOD;
	A = [AX,AY,AZ];
	M = m;
