function [interval_starts, ...
          interval_ends] ...
  = filter_indexIntervals( ...
      predicate, ... Function handle: "thing" -> Boolean
      vec ... Vector of "things"
      )

  % Returns intervals of indices of `vec` for which the predicate is true. The
  % left endpoints (`interval_starts`) are inclusive, while the right endpoints
  % (`interval_ends`) are exclusive.

  truth_values = arrayfun(predicate, vec);

  % Flank with `false`s so that we may detect contiguous intervals of `true`s
  extended_truth_values = [ 0 truth_values 0 ];

  interval_starts = find(diff(extended_truth_values) ==  1);
  interval_ends   = find(diff(extended_truth_values) == -1);
