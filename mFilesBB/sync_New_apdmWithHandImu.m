% Author: Lauro Ojeda, 2012-2015
function [LeftWb, LeftAb, RightWb, RightAb, HandWb, HandAb, WaistWb, WaistAb, static_period, PERIOD] = sync_New_apdmWithHandImu(L_FILE, R_FILE, H_FILE, W_FILE, waistImuFlag, SYNC, FORCE_SYNC_VALUE); %<--------------static_period
if(~exist('SYNC','var'))
	SYNC = 1; 
end;
if(~exist('FORCE_SYNC_VALUE','var'))
	FORCE_SYNC_VALUE = 0;% Use a non-zero value (counts) combined with SYNC=1 in order to force data synchronization
end;


infoL = h5info(L_FILE);
L_time = h5read(L_FILE, [infoL.Groups(2).Groups(1).Name,'/Time']);

infoR = h5info(R_FILE);
R_time = h5read(R_FILE, [infoR.Groups(2).Groups(1).Name,'/Time']);

infoH = h5info(H_FILE);
H_time = h5read(H_FILE, [infoH.Groups(2).Groups(1).Name,'/Time']);


if waistImuFlag
    infoW = h5info(W_FILE);
    W_time = h5read(W_FILE, [infoW.Groups(2).Groups(1).Name,'/Time']);
end


FREQ  = h5readatt(R_FILE, infoR.Groups(2).Groups(1).Groups(1).Name, 'Sample Rate');
PERIOD = 1/double(FREQ);
L_SECTION = [1,size(L_time,1)]*PERIOD;
R_SECTION = [1,size(R_time,1)]*PERIOD;
H_SECTION = [1,size(H_time,1)]*PERIOD;
if waistImuFlag
    W_SECTION = [1,size(W_time,1)]*PERIOD;
end


if(SYNC)
	if(FORCE_SYNC_VALUE)
		% If a known value of the time shift is know, replace the variable FORCE_SYNC_VALUE 
		% with the correct value, you may use croscorrelations to find the correct shift value
		warning('Sync with user defined value');
		shift = FORCE_SYNC_VALUE;
	else
% 		display('Sync with sensor timer');
% 		if(L_time(1) <= R_time(1))
% 			shift = -find(L_time >= R_time(1),1)
% 		else
% 			shift = find(R_time >= L_time(1),1)
% 		end;

        if waistImuFlag
            [shift_val] = max([L_time(1) R_time(1) W_time(1) H_time(1)]);
            shiftW = find(W_time >= shift_val,1);
        else
            [shift_val] = max([L_time(1) R_time(1) H_time(1)]);
        end

         shiftL = find(L_time >= shift_val,1);
         shiftR = find(R_time >= shift_val,1);
         shiftH = find(H_time >= shift_val,1);
	end;
    
    
% 	if(shift<0)
% 		display(sprintf('Shift Left IMU signals [%d]',-shift));
% 		L_SECTION = [-shift,size(L_time,1)]*PERIOD; % To shift left side
% 	else
% 		display(sprintf('Shift Right IMU signals [%d]',shift));
% 		R_SECTION = [shift,size(R_time,1)]*PERIOD; % To shift right side
% 	end;
    
    L_SECTION = [shiftL, size(L_time,1)]*PERIOD;
    R_SECTION = [shiftR, size(R_time,1)]*PERIOD;
    H_SECTION = [shiftH, size(H_time,1)]*PERIOD;
    if waistImuFlag
        W_SECTION = [shiftW, size(W_time,1)]*PERIOD;
    end

else
		warning('Not Syncing');
end;

% Load data 
[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(L_FILE,1);
[LeftWb,LeftAb, static_period, LeftMb] = getdata(Wb,Ab,PERIOD,L_SECTION,[],Mb); %<--------------static_period
set(gcf,'Name','Left IMU');

[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(R_FILE,1);
[RightWb,RightAb, ~, RightMb] = getdata(Wb,Ab,PERIOD,R_SECTION,[],Mb); %<--------------static_period
set(gcf,'Name','Right IMU');

[Wb,Ab,PERIOD,Mb] = getdata_New_apdm(H_FILE,1);
[HandWb, HandAb, ~, HandMb] = getdata(Wb, Ab, PERIOD, H_SECTION, [], Mb); 
set(gcf,'Name','Hand IMU');

if waistImuFlag
    [Wb,Ab,PERIOD,Mb] = getdata_New_apdm(W_FILE,1);
    [WaistWb, WaistAb, ~, WaistMb] = getdata(Wb, Ab, PERIOD, W_SECTION, [], Mb); 
    set(gcf,'Name','Waist IMU');
else
    WaistWb = [];  WaistAb = [];
end

