
function analyze_two_feet(files, imu, sections, imuAnalysisParameters)
    title = 'Analyze Walking Data Two Feet';
    prompt = 'Please choose imus below to analyze.';
    left_label = 'Left';
    right_label = 'Right';
    window_width = 400;
    window_height = 300;
    screen = get(0, 'ScreenSize');
    position = [((screen(3) - window_width)/2) ((screen(4) - window_height)/2) window_width window_height];
    margin = 75;
    placeholder = {'Left', 'Right', 'Waist', 'Hand'};

    d = dialog('Visible', 'on',     ...
                'Name', title,      ...
                'Position', position);

    uicontrol('Parent', d,          ...
                'Style', 'text',    ...
                'Position', [margin (window_height-100) 250 60],    ...
                'String', prompt);

    uicontrol('Parent', d,          ...
                'Style', 'text',    ...
                'Position', [margin (window_height- 155) 75 60],    ...
                'String', left_label);

    left_imu = uicontrol('Parent', d,             ...
                            'Style', 'popupmenu',   ...
                            'String', files,  ...
                            'Position', [175 (window_height - 150) 150 60]);

    uicontrol('Parent', d,          ...
                'Style', 'text',    ...
                'Position', [margin (window_height- 205) 75 60],    ...
                'String', right_label);
    
    right_imu = uicontrol('Parent', d,             ...
                            'Style', 'popupmenu',   ...
                            'String', files,  ...
                            'Position', [175 (window_height - 200) 150 60]);

     % OK
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
                'String', 'OK', ...
                'Position', [200, 30, 70, 30] , ...
                'Callback', @ok_Callback);

    % Cancel
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'Cancel', ...
        'Position', [280, 30, 70, 30] , ...
        'Callback', @cancel_Callback);

    function ok_Callback(~,~)
        d.Visible = 'off';
        imu_choice = {left_imu.Value right_imu.Value};
        section_choice( ...
            imu, imu_choice, @analysis_two, sections, ...
            imuAnalysisParameters);
        delete(d);
    end
        
    function cancel_Callback(~,~)
        delete(d)
    end
end
