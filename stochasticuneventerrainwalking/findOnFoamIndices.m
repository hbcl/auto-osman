function [rightOnFoamIndex leftOnFoamIndex success ] = findOnFoamIndices(trial)

% elev_constant2 = 0.05;  elev_constant1 = 0.03;  elev_constant3 =0.03;%0.1; ORGINAL
elev_constant2 = 0.035;  elev_constant1 = 0.1;  elev_constant3 = 0.03;%0.1;

success = true;

%zero them first
rightOnFoamIndex = []; leftOnFoamIndex = [];

if ~isempty(cell2mat(strfind(trial.trialType, 'Uneven1_1')))
    elevConst = elev_constant2;
elseif ~isempty(cell2mat(strfind(trial.trialType, 'Uneven1_2'))) | ~isempty(cell2mat(strfind(trial.trialType, 'Uneven2')))
    elevConst = elev_constant1;
end



if ~isempty(cell2mat(strfind(trial.trialType, 'Uneven1'))) % 'DnU', UnD, oD
    
    [~,IndR] = max(diff(-trial.right_strides.elev(end,:)));
    [~,IndL] = max(diff(-trial.left_strides.elev(end,:)));
    
    diffR = diff(-trial.right_strides.elev(end,:));
    diffL = diff(-trial.left_strides.elev(end,:));
    
    [aux,peaks_idxR]= findpeaks(diffR,'minpeakheight',elevConst)
    [aux,peaks_idxL]= findpeaks(diffL,'minpeakheight',elevConst)
    min_peaks_idxL = min(peaks_idxL);
    min_peaks_idxR = min(peaks_idxR);
    
     if min_peaks_idxL > min_peaks_idxR %  eliminate if both fisrt indexed vals are numbers
        rightOnFoamIndex = min(peaks_idxR)+1;
        leftOnFoamIndex = [];
     elseif min_peaks_idxL < min_peaks_idxR
        rightOnFoamIndex = [];
        leftOnFoamIndex = min(peaks_idxL)+1;
     else  
        if trial.stridesFrwdUpdatedL(min_peaks_idxL+1) > trial.stridesFrwdUpdatedR(min_peaks_idxL+1)
            rightOnFoamIndex = min_peaks_idxL+1;
            leftOnFoamIndex = [];
        else
            rightOnFoamIndex = [];
            leftOnFoamIndex = min_peaks_idxL+1;
        end 
     end
end


if ~isempty(cell2mat(strfind(trial.trialType, 'Uneven2'))) % 'DnU', UnD, oD
    
    ElevR = -trial.right_strides.elev;
    ElevL = -trial.left_strides.elev;
        
    [~,IndR] = max(ElevR(:));
    [~,IndL] = max(ElevL(:));
    
    [I_rowR, I_colR] = ind2sub(size(ElevR),IndR)
    [I_rowL, I_colL] = ind2sub(size(ElevL),IndL)

    if ElevR(I_rowR, I_colR) > ElevL(I_rowR, I_colR)  
        rightOnFoamIndex = I_colR - 1;
        leftOnFoamIndex = [];
    else
        rightOnFoamIndex = [];
        leftOnFoamIndex = I_colL - 1;
    end
end



if ~isempty(cell2mat(strfind(trial.trialType, 'longUneven'))) % 'DnU', UnD, oD
    
   diffR = diff(-trial.right_strides.elev(end,:));
    diffL = diff(-trial.left_strides.elev(end,:));
    
    [aux,peaks_idxR]= findpeaks(diffR,'minpeakheight',elev_constant3)
    [aux,peaks_idxL]= findpeaks(diffL,'minpeakheight',elev_constant3)
   
    
     if peaks_idxL(1) > peaks_idxR(1) %  eliminate if both fisrt indexed vals are numbers
        rightOnFoamIndex = peaks_idxR(1)+1;
        leftOnFoamIndex = [];
     elseif peaks_idxL(1) < peaks_idxR(1)
        rightOnFoamIndex = [];
        leftOnFoamIndex = peaks_idxL(1)+1;
     else  
        if trial.stridesFrwdUpdatedL(peaks_idxL(1)+1) > trial.stridesFrwdUpdatedR(peaks_idxL(1)+1)
            rightOnFoamIndex = peaks_idxL(1)+1;
            leftOnFoamIndex = [];
        else
            rightOnFoamIndex = [];
            leftOnFoamIndex = peaks_idxL(1)+1;
        end 
     end
end







% if ~isempty(cell2mat(strfind(trial.trialType, 'Uneven1')))
%     trial.trialType = {'oU'};
% elseif ~isempty(cell2mat(strfind(trial.trialType, 'Uneven1')))
%     trial.trialType = {'DnU'};
% end



if ~isempty(cell2mat(strfind(trial.trialType, 'UnD'))) % 'DnU', UnD, oD
    leftOnFoamIndex = find(-trial.left_strides.elev(end,:) < -elev_constant1);
    rightOnFoamIndex = find(-trial.right_strides.elev(end,:) < -elev_constant1);
end


% 
if ~isempty(cell2mat(strfind(trial.trialType, 'oD'))) % 'DnU', UnD, oD
    leftOnFoamIndex = find(-trial.left_strides.elev(end,:) < -elev_constant1);
    rightOnFoamIndex = find(-trial.right_strides.elev(end,:) < -elev_constant1);
    
    if ~isempty(leftOnFoamIndex) && ~isempty(rightOnFoamIndex)
        if leftOnFoamIndex(1) > rightOnFoamIndex(1) 
            leftOnFoamIndex = [];
            rightOnFoamIndex =  rightOnFoamIndex(1);
        else
            rightOnFoamIndex = [];
            leftOnFoamIndex = leftOnFoamIndex(1);
        end
    end
end

if ~isempty(cell2mat(strfind(trial.trialType, 'oU')))
    leftOnFoamIndex = find(-trial.left_strides.elev(end,:) > elev_constant3);
    rightOnFoamIndex = find(-trial.right_strides.elev(end,:) > elev_constant3);
    if ~isempty(leftOnFoamIndex) && ~isempty(rightOnFoamIndex)
        if leftOnFoamIndex(1) > rightOnFoamIndex(1) 
            leftOnFoamIndex = [];
            rightOnFoamIndex =  rightOnFoamIndex(1);
        else
            rightOnFoamIndex = [];
            leftOnFoamIndex = leftOnFoamIndex(1);
        end
    end
end

if cell2mat(strfind(trial.trialType, 'UnD'))
    if ~isempty(leftOnFoamIndex)
        for z = 1:length(leftOnFoamIndex)
            if leftOnFoamIndex(z) == 1
                continue;
            end
            
            if -trial.left_strides.elev(end,leftOnFoamIndex(z)-1) > elev_constant2 %now check the one before it
                leftOnFoamIndex = leftOnFoamIndex(z)-1;
                break;
            end
        end
    end

    if ~isempty(rightOnFoamIndex)
        for z = 1:length(rightOnFoamIndex)
            
            if rightOnFoamIndex(z) == 1
                continue;
            end
            
            if -trial.right_strides.elev(end,rightOnFoamIndex(z)-1) > elev_constant2
                rightOnFoamIndex = rightOnFoamIndex(z)-1; 
                break;
            end
        end
    end
end

%bazen negatife gitmekle bulmuo o zaman pozitifi kontrol et ve bir cikar
% if ~isempty(cell2mat(strfind(trial.trialType, 'DnU'))) && isempty(leftOnFoamIndex) && isempty(rightOnFoamIndex) 
%     leftOnFoamIndex = find(-trial.left_strides.elev(end,:) > elev_constant2);
%     rightOnFoamIndex = find(-trial.right_strides.elev(end,:) > elev_constant2);
% 
%      if ~isempty(leftOnFoamIndex) 
%          leftOnFoamIndex = leftOnFoamIndex - 1; 
%      end
%      if ~isempty(rightOnFoamIndex) 
%          rightOnFoamIndex = rightOnFoamIndex - 1 ;
%      end
% end

%get the first index only
% if ~isempty(cell2mat(strfind(trial.trialType, 'oD'))) ||  ~isempty(cell2mat(strfind(trial.trialType, 'oU'))) %there should be two indexes eliminate one 
%     if ~isempty(leftOnFoamIndex) && ~isempty(rightOnFoamIndex) %  eliminate if both fisrt indexed vals are numbers
%         if trial.stridesFrwdUpdatedL(leftOnFoamIndex(1)) > trial.stridesFrwdUpdatedR(rightOnFoamIndex(1))
%             rightOnFoamIndex = rightOnFoamIndex(1);
%             leftOnFoamIndex = [];
%         else
%             rightOnFoamIndex = [];
%             leftOnFoamIndex = leftOnFoamIndex(1);
%         end
%     end
% end



%get the first index only
% if ~isempty(cell2mat(strfind(trial.trialType, 'oU'))) ||  ~isempty(cell2mat(strfind(trial.trialType, 'UnD')))%there should be two indexes eliminate one 
%     
%     [~, Ind] = max(-trial.left_strides.elev(end, leftOnFoamIndex));  leftOnFoamIndex = leftOnFoamIndex(Ind);
%     [~, Ind] = max(-trial.right_strides.elev(end, rightOnFoamIndex)); rightOnFoamIndex = rightOnFoamIndex(Ind);
%     
%     if ~isempty(leftOnFoamIndex) && ~isempty(rightOnFoamIndex) %  eliminate if both fisrt indexed vals are numbers
%         if trial.stridesFrwdUpdatedL(leftOnFoamIndex(1)) < trial.stridesFrwdUpdatedR(rightOnFoamIndex(1))
%             rightOnFoamIndex = [];
%             leftOnFoamIndex = leftOnFoamIndex(1);
%         else
%             rightOnFoamIndex = rightOnFoamIndex(1);
%             leftOnFoamIndex = [];
%         end
%     end
% end

% if ~isempty(cell2mat(strfind(trial.trialType, 'oD'))) %there should be two indexes eliminate one 
%     
%     [~, Ind] = min(-trial.left_strides.elev(leftOnFoamIndex));  leftOnFoamIndex = leftOnFoamIndex(Ind);
%     [~, Ind] = min(-trial.right_strides.elev(rightOnFoamIndex)); rightOnFoamIndex = rightOnFoamIndex(Ind);
%     
%     if ~isempty(leftOnFoamIndex) && ~isempty(rightOnFoamIndex) %  eliminate if both fisrt indexed vals are numbers
%         if trial.stridesFrwdUpdatedL(leftOnFoamIndex(1)) < trial.stridesFrwdUpdatedR(rightOnFoamIndex(1))
%             rightOnFoamIndex = [];
%             leftOnFoamIndex = leftOnFoamIndex(1);
%         else
%             rightOnFoamIndex = rightOnFoamIndex(1);
%             leftOnFoamIndex = [];
%         end
%     end
% end


if isempty(leftOnFoamIndex) && isempty(rightOnFoamIndex)
    warning('can not find elevation on foam *************\n');  
    success = false;
    return;
end

if ~isempty(leftOnFoamIndex) && ~isempty(rightOnFoamIndex)
    success = false;
    warning('found left and right elevs *************\n');
    return;
end

if  length(leftOnFoamIndex) > 1  || length(rightOnFoamIndex) > 1
    success = false;
    warning('found two different elevs *************\n');
    return;
end















%     trial.leftOnFoamIndex = leftOnFoamIndex;
%     trial.righttOnFoamIndex = rightOnFoamIndex;
%******************************************************************************
