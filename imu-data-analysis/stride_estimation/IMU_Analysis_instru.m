close all 
clear 
clc

%% Parameters

stationary_period_params = StationaryPeriodParams();
foot_fall_params = FootFallDetectionParams();
foot_fall_opposite_velocity_params = FootFallOppositeVelocityDetectionParams();
tilt_correction_params = TiltCorrectionKalmanFilterParams();
step_filter_params = StepFilterParams();
plot_params = PlotParams();

%% Two feet analysis
% Deal with the ".h5" file, using the following function to load the data
files_left  = '/home/alex/gradschool/imu-test-data/20190108-093818_all_day5_sensor_1378_label_Left_foot.h5';
files_right = '/home/alex/gradschool/imu-test-data/20190108-093535_all_day5_sensor_1376_label_Right_foot.h5';

[LeftWb, LeftAb, RightWb, RightAb, PERIOD] = ...
    sync_New_apdm(files_left, files_right);

% Look at the data to make sure:
%   1. During the static period, the data should lay on the x-axis.
%   2. if not, we should add "BIAS" part in "getdata function"
% choose the proper section that you wanna analysis from the previous plot
% note: Unit of SECTION is sec.

downsampling_factor = 4;
plot_raw_data(PERIOD, false, LeftWb,  LeftAb,  [], downsampling_factor);
set(gcf,'Name','Left IMU');
plot_raw_data(PERIOD, false, RightWb, RightAb, [], downsampling_factor);
set(gcf,'Name','Right IMU');

%pause;
%Begin = input('Enter where the section begins (sec)--> ');
%End =  input('Enter where the section ends (sec)--> ');
Begin = 5693;
End = 7125;
SECTION = [Begin End];

%  The data in the static period lay on the x-axis.
[LeftWb,LeftAb] = getdata(LeftWb,LeftAb,PERIOD,SECTION);
[RightWb,RightAb] = getdata(RightWb,RightAb,PERIOD,SECTION);

% if not, uncomment the following and choose a section of static time,
% e.g. BIAS = 10; which means using 10 sec to calibrate the data
% data from the beginning of the section you are choosing

% BIAS = 2;
% [LeftWb,LeftAb] = getdata(LeftWb,LeftAb,PERIOD,SECTION,BIAS);
% [RightWb,RightAb] = getdata(RightWb,RightAb,PERIOD,SECTION,BIAS);

[left_walk_info, right_walk_info] = ...
  integrate_two_foot_imus( ...
    LeftWb, LeftAb, RightWb, RightAb, PERIOD, ...
    stationary_period_params, foot_fall_params, ...
    foot_fall_opposite_velocity_params, tilt_correction_params);
left_strides = stride_segmentation(left_walk_info, PERIOD, step_filter_params);
right_strides = stride_segmentation(right_walk_info, PERIOD, step_filter_params);
plt_ltrl_frwd_strides(left_strides, plot_params);
plt_ltrl_frwd_strides(right_strides, plot_params);
plt_frwd_elev_strides(left_strides, plot_params);
plt_frwd_elev_strides(right_strides, plot_params);
plt_stride_var(left_strides, plot_params);
plt_stride_var(right_strides, plot_params);
%% one foot analysis

% data file type is ".h5" we use the followinb function to load the data(put into the same directory)
[W,A,PERIOD,M] = getdata_New_apdm(files_left);
[W, A] = getdata(W,A,PERIOD);
% then specify which section of data you want to analyze, if you want to
% analyze whole data, choose SECTION = [];
% if you want to see the histogram, uncomment the following
%pause;
%Begin = input('Enter where the section begins (unit: sec)--> ');
%End =  input('Enter where the section ends (unit: sec)--> ');
Begin = 5693;
End = 7125;
SECTION = [Begin End];
% the following function sepecify the section of data you want to analyze
[W, A] = getdata(W,A,PERIOD,SECTION);
% the following function get detailed information about walking
walk_info = integrate_single_foot_imu( ...
  W, A, PERIOD, ...
  stationary_period_params, foot_fall_params, tilt_correction_params);
strides = stride_segmentation(walk_info, PERIOD, step_filter_params);
plt_ltrl_frwd_strides(strides, plot_params);
plt_frwd_elev_strides(strides, plot_params);
plt_stride_var(strides, plot_params);
