%% 
clear ; close all; clc; beep off;
restoredefaultpath;
addpath('D:\spare\SteppingOnFoamImu\stride_estimation\');

% 
name = 'DYpilot'
targetDirect = 'C:\Users\osman\Desktop\pilotWalking\DYPliot1';
% topdir = pwd;
% tempFileIndex = strfind(topdir,'SteppingOnFoamImu');
% targetDirect1 = strcat(topdir(1:tempFileIndex(1)-1),'SteppingOnFoamImu\v2\v3_ledStrip\',name,'_ledStrip');
% if isempty(ls(targetDirect1))
%     mkdir(targetDirect1);
% end
% addpath(genpath(targetDirect1));
% 
% targetDirect = strcat(topdir(1:tempFileIndex(1)-1),'SteppingOnFoamImu\v2\v3_ledStrip\',name,'_ledStrip\analyzedData');
% if isempty(ls(targetDirect))
%     mkdir(targetDirect);
% end
addpath(genpath(targetDirect));


%%
%RUN THIS PART JUST ONCE AND COMMENT OUT, HERE SAMPLE RATE (PERIOD) IS
%ASSUMED TO BE CONSTANT PERIOD = 0.0078
% % Syncronize and save
% [LeftWb, LeftAb, RightWb, RightAb, PERINK, ~, ~, static_period] = sync_apdm('RY5_monitor_778_label_Left_20160630-212053.h5',...
%     'RY5_monitor_1027_label_Right_20160630-212056.h5');

load 20171001-175513_pilotDarian2_sensor_779_label_Leftnewmat
LeftWb = W; LeftAb = A; 
clear W A;
load 20171001-175516_pilotDarian2_sensor_1027_label_Rightnewmat
RightWb = W; RightAb = A;
clear W A;

t = (1:size(LeftWb,1))*PERIOD;
figure; plot(t, LeftWb)

%%
handImuFlag = false;%true;
load PERIOD;

% FILL MANUALLY
%     test subject specific parameters***********FILL IT JUST ONCE*********************
data.name = name;
data.date = '01_10_2017';                
data.approxTrialDur = 15;  
data.approxTrialSampleNum =  data.approxTrialDur/PERIOD; %samples

MAX_TRIAL_NUM = 60;
trialTypeStringTemp = {'normal', 'UnD', 'normal', 'Uneven1', 'normal', 'Uneven2', 'normal'};
grNum_val = length(trialTypeStringTemp);
%end outliers
for i = 1:grNum_val
    [data.trialEndOutlier{i}(1:MAX_TRIAL_NUM)] =  deal(1/PERIOD); %samples
    [data.trialBeginOutlier{i}(1:MAX_TRIAL_NUM)] =  deal(20/PERIOD); %samples
end
[data.trialEndOutlier{1}(1)] =  deal(5/PERIOD);
[data.trialEndOutlier{1}(2)] =  deal(3/PERIOD);
[data.trialEndOutlier{1}(3)] =  deal(4/PERIOD);
[data.trialEndOutlier{1}(4)] =  deal(2/PERIOD);
[data.trialEndOutlier{2}(9)] =  deal(3/PERIOD);
[data.trialEndOutlier{2}(5)] =  deal(7/PERIOD);
[data.trialEndOutlier{2}(7)] =  deal(4/PERIOD);
[data.trialBeginOutlier{3}(1:2)] =  deal(15/PERIOD)

data.groupIndices = [];
data.savedTrials = [];
data.badTrials = [];
   
%define main sections     
%if you want to rerun a saved suject enter the trialindex here********
%     for i = 1:length(data.startGrIndex)
%         data.startGrIndex{i} = data.startGrIndex{i}-10; 
%     end
%     data.startGrIndex = datastartGrIndex;
%     data.endGrIndex = dataendGrIndex;
%************************************************************************
data.startGrIndex{1} = [2172];        data.endGrIndex{1} =  [2400];    %normal 
data.startGrIndex{2} = [2875];        data.endGrIndex{2} =  [3140];    %UnD
data.startGrIndex{3} = [3460];        data.endGrIndex{3} =  [3700];    %normal   
data.startGrIndex{4} = [3620];        data.endGrIndex{4} =  [4500];    %Uneven1
data.startGrIndex{5} = [4834];        data.endGrIndex{5} =  [5000];    %normal
data.startGrIndex{6} = [6021];        data.endGrIndex{6} =  [6584];    %Uneven2
data.startGrIndex{7} = [6700];        data.endGrIndex{7} =  [6812];    %normal

trailNum = 1;
for grNum = 1:length(data.startGrIndex)  

    data.groupIndices{grNum} = [];

    for k = 1:length(data.startGrIndex{grNum}) 

        if handImuFlag;
            [imuData.(strcat('gr',num2str(grNum))).Hand_Wb{k},  imuData.(strcat('gr',num2str(grNum))).Hand_Ab{k},  ~] = getdata(HandWb, HandAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
            close(gcf);
        end
%         [imuData.(strcat('gr',num2str(grNum))).Waist_Wb{k},  imuData.(strcat('gr',num2str(grNum))).Waist_Ab{k},  ~] = getdata(WaistWb, WaistAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
%         close(gcf);
        [imuData.(strcat('gr',num2str(grNum))).Left_Wb{k},  imuData.(strcat('gr',num2str(grNum))).Left_Ab{k},  ~] = getdata(LeftWb, LeftAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
        close(gcf);
        [imuData.(strcat('gr',num2str(grNum))).Right_Wb{k}, imuData.(strcat('gr',num2str(grNum))).Right_Ab{k},  static_period] = getdata(RightWb, RightAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
        t = (1:size(imuData.(strcat('gr',num2str(grNum))).Left_Wb{k}, 1)) * PERIOD; 
        
        %find the trial intervals and update the trial index  *************************  
        temp_ylim = ylim;
        subplot(2,1,1); hold on;
        for i = 1:length(static_period)-1
            if (static_period(i+1) - static_period(i)) > data.approxTrialSampleNum
                trialIndex{trailNum} = [static_period(i) - data.trialBeginOutlier{grNum}(trailNum)    static_period(i+1) - data.trialEndOutlier{grNum}(trailNum)];             
                trialIndex{trailNum} = trialIndex{trailNum} *  PERIOD; %convert to secs  
                plot([trialIndex{trailNum}(1) trialIndex{trailNum}(1)], ylim, 'r--', 'linewidth', 2);
                plot([trialIndex{trailNum}(2) trialIndex{trailNum}(2)], ylim, 'b--', 'linewidth', 2);
                temp_ylim = ylim;
                h = text(trialIndex{trailNum}(1) + 2 ,temp_ylim(2)*0.75 ,num2str(trailNum)); set(h,'fontsize',25);
                data.groupIndices{grNum} = [data.groupIndices{grNum} trailNum];
                trailNum = trailNum+1;
            end
        end
        data.trialIndex = trialIndex; 
        figname = strcat('figureMainIndex', num2str(grNum), '_', num2str(k),'_', data.name);
        currentFolder = cd;
        cd(targetDirect);   
        saveas(gcf, figname, 'fig')
        cd(currentFolder);      
        %*******************************************************
    end

end

currentFolder = cd;
cd(targetDirect);   
save(strcat('imuData', data.name),'imuData');
cd(currentFolder);   

%adjust this part based on gorup numbers
%gr1
grIn = data.groupIndices{1}; 
[data.trialTypeString{grIn}] =  deal('Normal');

%gr2
grIn = data.groupIndices{2}; 
[data.trialTypeString{grIn(1):grIn(end)}]=  deal('UnD');
[data.foamThickness{grIn}] =    deal('thick3inch');

%gr3
grIn = data.groupIndices{3}; 
[data.trialTypeString{grIn}] =  deal('Normal');

%gr4
grIn = data.groupIndices{4}; 
[data.trialTypeString{grIn(1):2:grIn(end)}]=  deal('Uneven1_1');
[data.trialTypeString{grIn(1)+1:2:grIn(end)}]=  deal('Uneven1_2');

%gr5
grIn = data.groupIndices{5}; 
[data.trialTypeString{grIn}] =  deal('Normal');

%gr6
grIn = data.groupIndices{6}; 
[data.trialTypeString{grIn(1):2:grIn(end)}]=  deal('Uneven2_1');
[data.trialTypeString{grIn(1)+1:2:grIn(end)}]=  deal('Uneven2_2');    

%gr7
grIn = data.groupIndices{7}; 
[data.trialTypeString{grIn}] =  deal('Normal');

currentFolder = cd;
cd(targetDirect);   
save(strcat(data.name,'_UnevenPilot'),'data'); 
cd(currentFolder);
fprintf('all initial params saved *****\n'); 

%***********************************************

%% ANALYZE
%no filter ever

clear all
load DYpilot_UnevenPilot;
load imuDataDYpilot;
handImuFlag = false
load PERIOD
targetDirect = 'C:\Users\osman\Desktop\pilotWalking\DYPliot1';


FILTER = 0; OutlierSecL = []; OutlierSecR = OutlierSecL; % outliers are already handled

grNum_val_begin = 1;
grNum_val_end = length(data.groupIndices);  
runSingleTrial = 0;
if runSingleTrial
    % make sure first to save all, you can go to the past trials before
    % saving all, but can not go to future trials, you need to save all first
    grNum_val = 4%4%2;
    singleTrial = 33%28%15;
    grNum_val_end = grNum_val;
end 
close all
autoRecordFlag = 0;
for grNum = 7:7%grNum_val_begin:grNum_val_end      
    for k = 1:length(data.startGrIndex{grNum}) 

        clear Left_Wb Right_Wb Left_Ab Right_Ab
        Left_Wb = imuData.(strcat('gr',num2str(grNum))).Left_Wb{k};
        Right_Wb = imuData.(strcat('gr',num2str(grNum))).Right_Wb{k};
%         Waist_Wb = imuData.(strcat('gr',num2str(grNum))).Waist_Wb{k};
        
        
        Left_Ab = imuData.(strcat('gr',num2str(grNum))).Left_Ab{k};
        Right_Ab = imuData.(strcat('gr',num2str(grNum))).Right_Ab{k};
%         Waist_Ab = imuData.(strcat('gr',num2str(grNum))).Waist_Ab{k};
        
        
        if handImuFlag
            Hand_Wb = imuData.(strcat('gr',num2str(grNum))).Hand_Wb{k};
            Hand_Ab = imuData.(strcat('gr',num2str(grNum))).Hand_Ab{k};
        end
        

%         figname = strcat('figureMainIndex', num2str(grNum), '_', num2str(k), '_', data.name);
%         openfig(figname);

        trialIndex = data.trialIndex;   
        trailNumstart = data.groupIndices{grNum}(1);
        trialNumEnd  = data.groupIndices{grNum}(end);
        
        if runSingleTrial
            trailNumstart =  singleTrial(1);
            trialNumEnd = singleTrial(end);           
        end

        % set the parameters
        for i = trailNumstart : trialNumEnd 
            
%             if ~runSingleTrial && ~isempty(find(i ==  data.savedTrials, 1))
%                 fprintf('trial is already saved, passing to next trial \n');
%                 continue;
%             end

            clear trial;
            trial.trialType = data.trialTypeString(i);
            trial.trailNum = i;

            %get the data
            [trial.left_Wb,  trial.left_Ab] =  getdata(Left_Wb,  Left_Ab, PERIOD, trialIndex{i});
            [trial.right_Wb, trial.right_Ab] = getdata(Right_Wb, Right_Ab, PERIOD, trialIndex{i});
%             [trial.waist_Wb, trial.waist_Ab] = getdata(Waist_Wb, Waist_Ab, PERIOD, trialIndex{i});
            if handImuFlag
                [trial.hand_Wb, trial.hand_Ab] = getdata(Hand_Wb, Hand_Ab, PERIOD, trialIndex{i});
            end
            
            % Process data from the two IMUs on the feet simultaneously 
            [trial.Left_foot,trial.Right_foot] = compute_pos_two_imus(trial.left_Wb, trial.left_Ab, trial.right_Wb, trial.right_Ab, PERIOD);

            % Segment steps
            trial.left_strides = stride_segmentation(trial.Left_foot, PERIOD, FILTER, OutlierSecL);
            trial.right_strides = stride_segmentation(trial.Right_foot, PERIOD, FILTER, OutlierSecR);

            % close all; clc;
            plotOnlyStrideVelswithNormals = 0;
            [trial.rightOnFoamIndex  trial.leftOnFoamIndex trial.rightOnFoamIndex2  trial.leftOnFoamIndex2] = plotNanalyseStrides(trial);

%             keyboard;
            pause('on');
            if ~autoRecordFlag
                result = input('store the trial  using acc data?','s');
                if ~strcmp(result,'n') 
                    tempind = find(i == data.badTrials,1);
                    if ~isempty( tempind)
                        data.badTrials( tempind) = [];
                    end
                    data = storeTrial(data, trial, i, targetDirect);
                    disp('trial stored using acc data')
                else
                     result = input('store the trial  using elev data?','s');
                     if ~strcmp(result,'n')         
                        trial.rightOnFoamIndex = trial.rightOnFoamIndex2;
                        trial.leftOnFoamIndex = trial.leftOnFoamIndex2;
                         
                        tempind = find(i == data.badTrials,1);
                        if ~isempty( tempind)
                            data.badTrials( tempind) = [];
                        end                      
                        data = storeTrial(data, trial, i, targetDirect);
                        disp('trial stored using elev data')
                     else                          
                         result = input('do you want to store it manually?','s');
                         if  ~strcmp(result,'n') 
                             templeg = input(' put it manually \n enter leg (red is right)', 's');
                             tempIndex = input('enter number');        
                             if strcmp(templeg ,'r')
                                 trial.rightOnFoamIndex = tempIndex;
                                 trail.leftOnFoamIndex = [];
                             else
                                 trial.leftOnFoamIndex = tempIndex;
                                 trail.rightOnFoamIndex = [];
                             end
                             
                             tempind = find(i == data.badTrials,1);
                            if ~isempty( tempind)
                                data.badTrials( tempind) = [];
                            end 
                             
                             data = storeTrial(data, trial, i, targetDirect);
                             disp('trial data stored manually'); 
                         else
                             trial = [];
                             data.badTrials = [data.badTrials i];
                             data = storeTrial(data, trial, i, targetDirect);
                             disp('trial data NOT stored');
                         end
                     end  
                end
            else            
                tempind = find(i == data.badTrials,1);
                if ~isempty( tempind)
                    data.badTrials( tempind) = [];
                end
                data = storeTrial(data, trial, i, targetDirect);
                disp('trial data stored aut omatically'); 
            end
%             keyboard;
            close all; clc;
            
        end
    end
end
%%

