% Demo - Find Heel and MTP Joint from IMU Walking Trajectories


clean
GRAVITY = 9.803;
% example for Demo
% [data0] = syncAPDMimusv01('C:\Users\Peter\Desktop\John and John - IMU Program\ApdmWalk\apdm_20130712\20120418-132855_sensor_data_monitor_472_label_Left.h5');
% SECTION = [33924       44240] ; BIAS = [ 27108       33820]; % round(xlim);

% pathname = [pwd '\FirstTry2013-10-30'];
% SECTION = [ 518508      529322] ; BIAS = [  503889      508601]; 

pathname = [pwd '\IMUPlate2014-01-23'];
SECTION = [ 580187      586661]; BIAS = [575510      580180];

% Use this spot to put in particular trials:
cd(pathname)
filenames = dir('*.h5'); filenames = {filenames.name};
[data(1:length(filenames))] = syncAPDMimusv01(filenames{:}); 

for ii = 1:length(filenames)
    W0 = data{ii}.gyroData * 180/pi;
    A0 = data{ii}.accelData;
    PERIOD =  mean(diff(data{ii}.time));  % 1.0/128.0;
    figure; plot(W0)
    
    %
    SECTION = SECTION(1):SECTION(end);
    BIAS = BIAS(1):BIAS(2);
    % debias
    W0 = W0 - repmat(mean(W0(BIAS,:),1),size(W0,1),1);
    % correct tilt with +z acceleration Up
    G0 = mean(A0(BIAS,:),1);
    G0dir = G0/normvec(G0);
    R = rodrigues(acos(dot(G0dir, [0 0 1])) *  cross(G0dir,[0 0 1])/normvec(cross(G0dir,[0 0 1])));
    A0 = A0*R';
    W0 = W0*R';
    A0 = A0*GRAVITY/normvec(G0);
    
    %% Process the Data (Lauro's Algorithm)
    USE_KF = 1;
    W_FF = 50;
    A_FF = 4; % 0;
    T_FF = 0.4;
    MAX_T_FF = T_FF*3;
    MIN_T_FF = 0;
    walk_info0 = compute_pos(W0(SECTION,:)*pi/180 *PERIOD ,A0(SECTION,:),PERIOD,USE_KF,W_FF,A_FF,T_FF,MAX_T_FF,MIN_T_FF); % note that W is to be given in finite integral form: Wrate*dT
    
    %% Find and Plot the Heel and MTP Joint based on foot-slap and heel-lift kinematics
    footLocationsZUpReferenceFrame(ii) = EstimateHeelAndMTPFromIMUMotionv01(walk_info0);
    
    % Transform all the results into the Raw IMU Reference Frame
    floc = structfun(@(a) a*R, footLocationsZUpReferenceFrame(ii), 'UniformOutput',0); % get it out into the RAW IMU COORDINATE SYSTEM
    footLocationsRawIMUReferenceFrame(ii) = floc;
    
    % Transform all the results into the Right-Forward-Up (RFU) reference
    % frame
    Rrfu = footLocationsZUpReferenceFrame(ii).Rrfu;
    floc = structfun(@(a) a*Rrfu', footLocationsZUpReferenceFrame(ii), 'UniformOutput',0); % get it out into the RAW IMU COORDINATE SYSTEM
    footLocationsRFUReferenceFrame(ii) = floc;
    
    
end

%% Display all the results. 
% for IMU Raw frame
filenames'
r0HeelIMU = cat(1,footLocationsRawIMUReferenceFrame.r0Heel)
r0HeelIMUMag = normvec(r0HeelIMU,2)
r0MTPIMU = cat(1,footLocationsRawIMUReferenceFrame.r0MTP)
r0MTPIMUMag = normvec(r0MTPIMU,2)
nullAxisHeelIMU = cat(1,footLocationsRawIMUReferenceFrame.nullAxisHeel)
nullAxisMTPIMU = cat(1,footLocationsRawIMUReferenceFrame.nullAxisMTP)

% for Right-Forward-Up (RFU) frame
filenames'
r0HeelRFU = cat(1,footLocationsRFUReferenceFrame.r0Heel)
r0HeelRFUMag = normvec(r0HeelRFU,2)
r0MTPRFU = cat(1,footLocationsRFUReferenceFrame.r0MTP)
r0MTPRFUMag = normvec(r0MTPRFU,2)
nullAxisHeelRFU = cat(1,footLocationsRFUReferenceFrame.nullAxisHeel)
nullAxisMTPRFU = cat(1,footLocationsRFUReferenceFrame.nullAxisMTP)

r0HeelToMTPRaw = [-r0HeelRFU+r0MTPRFU];
r0MTPToHeelRaw = [-r0MTPRFU+r0HeelRFU];

% Plot IMU's and MTP's with respect to the Heel
hfigHeelraw = figure; plotxyz([0 0 0],'b.'); hold on; plotxyz([-r0HeelRFU], 'g.'); plotxyz( r0HeelToMTPRaw,'r.'); axis equal; axis vis3d; rotate3d; title('Heel to IMU to MTP vectors, Raw');
 for ii = 1:size(r0HeelRFU,1)
     text(-r0HeelRFU(ii,1), -r0HeelRFU(ii,2), -r0HeelRFU(ii,3), sprintf('%d',ii));
     plotxyz( [-r0HeelRFU(ii,:)-0.02*nullAxisHeelRFU(ii,:);  -r0HeelRFU(ii,:)+0.02*nullAxisHeelRFU(ii,:)], 'k-');
     plotxyz( [r0HeelToMTPRaw(ii,:)-0.02*nullAxisMTPRFU(ii,:);  r0HeelToMTPRaw(ii,:)+0.02*nullAxisMTPRFU(ii,:)], 'k-');
     plotxyz([0 0 0; -r0HeelRFU(ii,:); r0HeelToMTPRaw(ii,:)],'k-');
 end
 covarianceOfMTPwrtHeelRaw = cov(r0HeelToMTPRaw);
 [v,d] = eig(covarianceOfMTPwrtHeelRaw);
 slopAxisHeelToMTPRaw = sqrt(d(end,end))*v(:,end)'
 plotxyz( [mean(r0HeelToMTPRaw) - slopAxisHeelToMTPRaw; mean(r0HeelToMTPRaw); mean(r0HeelToMTPRaw) + slopAxisHeelToMTPRaw], 'k-','LineWidth',1.5);
 legend({'Heel','IMU','MTP'});
 
% Plot IMU's and Heels with respect to the MTP
hfigMTPraw = figure; plotxyz([0 0 0],'r.'); hold on; plotxyz([-r0MTPRFU], 'g.'); plotxyz( r0MTPToHeelRaw,'b.'); axis equal; axis vis3d; rotate3d; title('MTP to IMU to Heel vectors, Raw');
 for ii = 1:size(r0MTPRFU,1)
     text(-r0MTPRFU(ii,1), -r0MTPRFU(ii,2), -r0MTPRFU(ii,3), sprintf('%d',ii));
     plotxyz( [-r0MTPRFU(ii,:)-0.02*nullAxisMTPRFU(ii,:);  -r0MTPRFU(ii,:)+0.02*nullAxisMTPRFU(ii,:)], 'k-');
     plotxyz( [r0MTPToHeelRaw(ii,:)-0.02*nullAxisHeelRFU(ii,:);  r0MTPToHeelRaw(ii,:)+0.02*nullAxisHeelRFU(ii,:)], 'k-');
     plotxyz([0 0 0; -r0MTPRFU(ii,:); r0MTPToHeelRaw(ii,:)],'k-');
 end
 covarianceOfHeelwrtMTPRaw = cov(r0MTPToHeelRaw);
 [v,d] = eig(covarianceOfHeelwrtMTPRaw);
 slopAxisMTPToHeelRaw = sqrt(d(end,end))*v(:,end)'
 plotxyz( [mean(r0MTPToHeelRaw) - slopAxisMTPToHeelRaw; mean(r0MTPToHeelRaw); mean(r0MTPToHeelRaw) + slopAxisMTPToHeelRaw], 'k-','LineWidth',1.5);
 legend({'MTP','IMU','Heel'});

 

% Remove Null Axis Offset from each
r0HeelRFUNullRemoved = r0HeelRFU - repmat(dot(r0HeelRFU,nullAxisHeelRFU,2),1,3).*nullAxisHeelRFU 
r0MTPRFUNullRemoved = r0MTPRFU - repmat(dot(r0MTPRFU,nullAxisMTPRFU,2),1,3).*nullAxisMTPRFU 

r0HeelToMTPNullRemoved = [-r0HeelRFUNullRemoved+r0MTPRFUNullRemoved];
r0MTPToHeelNullRemoved = [-r0MTPRFUNullRemoved+r0HeelRFUNullRemoved];

% Plot IMU's and MTP's with respect to the Heel
hfigHeelNulled = figure; plotxyz([0 0 0],'b.'); hold on; plotxyz([-r0HeelRFUNullRemoved], 'g.'); plotxyz( r0HeelToMTPNullRemoved,'r.'); axis equal; axis vis3d; rotate3d; title('Heel to IMU to MTP vectors, with Null Axis Removed');
 for ii = 1:size(r0HeelRFUNullRemoved,1)
     text(-r0HeelRFUNullRemoved(ii,1), -r0HeelRFUNullRemoved(ii,2), -r0HeelRFUNullRemoved(ii,3), sprintf('%d',ii));
     plotxyz( [-r0HeelRFUNullRemoved(ii,:)-0.02*nullAxisHeelRFU(ii,:);  -r0HeelRFUNullRemoved(ii,:)+0.02*nullAxisHeelRFU(ii,:)], 'k-');
     plotxyz( [r0HeelToMTPNullRemoved(ii,:)-0.02*nullAxisMTPRFU(ii,:);  r0HeelToMTPNullRemoved(ii,:)+0.02*nullAxisMTPRFU(ii,:)], 'k-');
     plotxyz([0 0 0; -r0HeelRFUNullRemoved(ii,:); r0HeelToMTPNullRemoved(ii,:)],'k-');
 end
 covarianceOfMTPwrtHeel = cov(r0HeelToMTPNullRemoved);
 [v,d] = eig(covarianceOfMTPwrtHeel);
 slopAxisHeelToMTP = sqrt(d(end,end))*v(:,end)'
 plotxyz( [mean(r0HeelToMTPNullRemoved) - slopAxisHeelToMTP; mean(r0HeelToMTPNullRemoved); mean(r0HeelToMTPNullRemoved) + slopAxisHeelToMTP], 'k-','LineWidth',1.5);
 legend({'Heel','IMU','MTP'});
 
% Plot IMU's and Heels with respect to the MTP
hfigMTPNulled = figure; plotxyz([0 0 0],'r.'); hold on; plotxyz([-r0MTPRFUNullRemoved], 'g.'); plotxyz( r0MTPToHeelNullRemoved,'b.'); axis equal; axis vis3d; rotate3d; title('MTP to IMU to Heel vectors, with Null Axis Removed');
 for ii = 1:size(r0MTPRFUNullRemoved,1)
     text(-r0MTPRFUNullRemoved(ii,1), -r0MTPRFUNullRemoved(ii,2), -r0MTPRFUNullRemoved(ii,3), sprintf('%d',ii));
     plotxyz( [-r0MTPRFUNullRemoved(ii,:)-0.02*nullAxisMTPRFU(ii,:);  -r0MTPRFUNullRemoved(ii,:)+0.02*nullAxisMTPRFU(ii,:)], 'k-');
     plotxyz( [r0MTPToHeelNullRemoved(ii,:)-0.02*nullAxisHeelRFU(ii,:);  r0MTPToHeelNullRemoved(ii,:)+0.02*nullAxisHeelRFU(ii,:)], 'k-');
     plotxyz([0 0 0; -r0MTPRFUNullRemoved(ii,:); r0MTPToHeelNullRemoved(ii,:)],'k-');
 end
 covarianceOfHeelwrtMTP = cov(r0MTPToHeelNullRemoved);
 [v,d] = eig(covarianceOfHeelwrtMTP);
 slopAxisMTPToHeel = sqrt(d(end,end))*v(:,end)'
 plotxyz( [mean(r0MTPToHeelNullRemoved) - slopAxisMTPToHeel; mean(r0MTPToHeelNullRemoved); mean(r0MTPToHeelNullRemoved) + slopAxisMTPToHeel], 'k-','LineWidth',1.5);
 legend({'MTP','IMU','Heel'});
 
 

