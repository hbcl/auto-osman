% Bibliography:
% - Ken Shoemake: Quaternions
% pp 7
% Q1=R & Q2=Q
% R*Q = rotation about the X, Y and Z navigation axis
% Q1=Q & Q2=R
% Q*R = rotation about the X, Y and Z body axis

function Q = qua_mul(Qleft,Qright)
a=Qleft(1); b=Qleft(2); c=Qleft(3); d=Qleft(4);
Qleft_sqw = [a,-b,-c,-d;
		 		b, a,-d, c;
		 		c, d, a,-b;
		 		d,-c, b, a];
Q=Qleft_sqw*Qright;
