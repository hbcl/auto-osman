classdef ReadDisplayParams
% Collection of all of the parameters for reading and displaying imu information
    properties
        % Parameters for reading in imus 
        read_imu_params;
        %Parameters for automatically detecting trials
        trial_detection_params;
    end

    methods
        % Default values
        function obj = ReadDisplayParams(obj)
            obj.read_imu_params = ReadImuParams();
            obj.trial_detection_params = AutoTrialDetectParams();
        end
    end
end