function [ euler_angles,                  ... % Matrix of Doubles (n_frames x 3)
           global_linear_accelerations ]  ... % Matrix of Doubles (n_frames x 3)
  = orient_raw_imu_data( ...
      sampling_period,                ... % Double, the IMU sampling period
      stationary_period_params,       ... % StationaryPeriodParams
      tilt_correction_params,         ... % TiltCorrectionKalmanFilterParams
      imu_local_angular_velocities,   ... % Matrix of Doubles (n_frames x 3)
      imu_local_linear_accelerations  ... % Matrix of Doubles (n_frames x 3)
      )

  % Integrate angular velocities to obtain the IMU orientations, with which the
  % accelerations are transformed into a common, global reference frame.

  % Optionally perform tilt correction of the orientations using the
  % acceleration during stationary periods, which can be assumed to point in
  % the direction of gravity. The tilt correction assumes a specific
  % correspondence between quaternions and Euler angles, which is implemented
  % by the functions `qua2eul` and `eul2qua`. If tilt correction is not used,
  % the orientation of the IMU at the start of the data must be roughly
  % horizontal so that the pitch and roll angles are close to zero.

  % Unpack parameters
  [n_frames, ~] = size(imu_local_angular_velocities);

  % Compute roll and pitch angles.
  [tilt_roll_angles, tilt_pitch_angles] = acc_tilt(imu_local_linear_accelerations);

  % Define stationary periods
  [stationary_frame_indices, ~] = stationary_periods_from_raw_imu_data( ...
    sampling_period, ...
    stationary_period_params, ...
    imu_local_angular_velocities, ...
    imu_local_linear_accelerations);
  stationary_frame_mask = mask_from_indices(n_frames, stationary_frame_indices);

  % Integrate angular velocities (using quaternions) with optional tilt correction
  if tilt_correction_params.apply_tilt_correction
    quaternions = integrate_angular_velocities_tilt_correction( ...
      sampling_period, imu_local_angular_velocities, ...
      tilt_pitch_angles, tilt_roll_angles, ...
      stationary_frame_mask, tilt_correction_params);
  else
    quaternions = integrate_angular_velocities_naive( ...
                    imu_local_angular_velocities);
  end

  % Obtain Euler angles angles (roll, pitch, yaw/heading).
  euler_angles = qua2eul(quaternions);

  % Map the IMU accelerations to a common reference frame using the IMU orientations.
  global_linear_accelerations = zipRowsWith( ...
    @(quat, vec) (qua2rot(quat) * vec'), ...
    quaternions, ...
    imu_local_linear_accelerations  ...
    );
end % function


% Helper functions

function result ... % Matrix of Doubles (n x m)
  = zipRowsWith( ...
      f,          ... % Function handle, taking two row vectors (lengths k, l) and producing a vector (length m)
      matrix1,    ... % Matrix of Doubles (n x k)
      matrix2     ... % Matrix of Doubles (n x l)
      )
  % Take a vector-valued function of two vector arguments, and apply it row-by-row
  % to two matrices of with the same number of rows.

  % Get dimensions
  n_rows = size(matrix1, 1);
  output_cols = length(f(matrix1(1, :), matrix2(1, :)));

  % Allocate outputs
  result = zeros(n_rows, output_cols);

  for row_no = 1:n_rows
    result(row_no, :) = f(matrix1(row_no, :), matrix2(row_no, :));
  end
end
