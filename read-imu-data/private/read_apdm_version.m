%% Takes the name of an apdm imu file as an input and returns the file format version.
%% FileFormat version is neccasary to determine the correct file usage.
function [version] = read_apdm_version(file)

    disp(file);
    file = convertStringsToChars(file);
    disp(file);
    try
        version = h5readatt(file, '/', 'FileFormatVersion');
    catch
        try
            version = h5readatt(file, '/', 'File_Format_Version');
        catch
            error("Couldn't read file format of " + file);
        end
    end
    disp("Version: " + version);
end