function [impulseResponse] =  predictSimulationParametersHuman(bumpHeights, numPOsBefore)%we dont need numPOsAfter, it should be symmetric



load dataAvgUneven1_1;
dataRef = dataOut; clear dataOut;
% figure; plot(dataRef.speed); hold on; plot(dataRef.speed, 'r.')
PR1Matrix = [];
bumpInd = find(0 == dataRef.time);

% xx = 5;
% dataRef.speed = dataRef.speed(xx+1:end);
% dataRef.time = dataRef.time(xx+1:end);


dataRef.speed = dataRef.speed(bumpInd-numPOsBefore:end);
dataRef.time = dataRef.time(bumpInd-numPOsBefore:end);


bumpInd = find(0 == dataRef.time);
lenBumpPR1 = length(dataRef.speed);
tempLen = length(bumpHeights)+bumpInd-1;
bumps = [zeros(1, bumpInd-1) bumpHeights zeros(1, lenBumpPR1-tempLen)];
tempInd = 1;

plusInd = bumpInd-1;
numSteps = 2*plusInd +1;


for kk = 1:lenBumpPR1-(plusInd)%plusInd, numSteps
    if plusInd+1 > kk%numSteps+1 > plusInd+i
          temp = [fliplr(bumps(1,tempInd:plusInd-1+kk)) zeros(1, numSteps-(plusInd-1+kk))];%works too, , this is this (epsilon -m to +m)  f[n-m] * h[m]
    else     
        temp = fliplr(bumps(1,tempInd:(numSteps+tempInd-1)))%works, this is this (epsilon -m to +m)  f[n-m] * h[m]
        tempInd = tempInd+1;
    end
    PR1Matrix = [PR1Matrix; temp];
end
PR1Matrix =  [PR1Matrix; zeros(plusInd,numSteps)]; 
paramPR1 = dataRef.speed;



paramInOffset = mean(dataRef.speed);
impulseResponse = PR1Matrix\(paramPR1-paramInOffset);
