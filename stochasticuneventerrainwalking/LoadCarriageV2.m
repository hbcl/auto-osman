close all
clear all

%% Files
% filename = 'E:\Load Carriage\S02\Trials\S5G1L1F0.txt';
filename = 'S5G1L1F0.txt'
% MVCs = xlsread('E:\Load Carriage\MVCs.xlsx');
% OnsetTimes = xlsread('E:\Load Carriage\OnsetTimes.xlsx');
% Anthro = xlsread('E:\Load Carriage\Subject_Anthro.xlsx');
%% Import Data
% Initialize variables
delimiter = 't\';
startRow = 10;
%% Format string for each line of text:
formatSpec = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%q%[^\n\r]';
%% Open the text file.
fileID = fopen(filename,'r');
%% Read columns of data according to format string.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
%% Allocate imported array to column variable names
ChannelTitle = dataArray{:, 1};
Fz = dataArray{:, 2};
Fy = dataArray{:, 3};
Tibia = dataArray{:, 4};
Sacrum = dataArray{:, 5};
SOL = dataArray{:, 6};
GM = dataArray{:, 7}; 
TA = dataArray{:, 8};
VM = dataArray{:, 9};
RF = dataArray{:, 10};
ST = dataArray{:, 11};
GMED = dataArray{:, 12};
ES = dataArray{:, 13};
FzP1 = dataArray{:, 14};
Comments = dataArray{:, 15};
%% Clear temporary variables
clearvars delimiter startRow formatSpec fileID dataArray ans;
% % Identify file
% subnum = str2num(filename(19:20));
% if subnum < 10
%     gait = str2num(filename(32));
%     load = str2num(filename(34));
%     steprate = str2num(filename(36));
% elseif subnum >= 10
%     gait = str2num(filename(33));
%     load = str2num(filename(35));
%     steprate = str2num(filename(37));
% end
%% Finding comments
index = strmatch('#',Comments);
start = index(1,1);
finish = index(2,1);
%% Cropping Variables
% Crop variable to length; Replace NaN with zero; Replace zero with previous number
ChannelTitle = ChannelTitle(start:finish,:);
ES = ES(start:finish,:);ES(isnan(ES))= 0; %ES = fillzero(ES);
Fy = Fy(start:finish,:);Fy(isnan(Fy))= 0;%Fy = fillzero(Fy);
Fz = Fz(start:finish,:);Fz(isnan(Fz))= 0;%Fz = fillzero(Fz);
FzP1 = FzP1(start:finish,:);FzP1(isnan(FzP1))= 0;%FzP1 = fillzero(FzP1);
GM = GM(start:finish,:);GM(isnan(GM))= 0;%GM = fillzero(GM);
GMED = GMED(start:finish,:);GMED(isnan(GMED))= 0;%GMED = fillzero(GMED);
RF = RF(start:finish,:);RF(isnan(RF))= 0;%RF = fillzero(RF);
Sacrum = Sacrum(start:finish,:);Sacrum(isnan(Sacrum))= 0;%Sacrum = fillzero(Sacrum);
SOL = SOL(start:finish,:);SOL(isnan(SOL))= 0;%SOL = fillzero(SOL);
ST = ST(start:finish,:);ST(isnan(ST))= 0;%ST = fillzero(ST);
TA = TA(start:finish,:);TA(isnan(TA))= 0;%TA = fillzero(TA);
Tibia = Tibia(start:finish,:);Tibia(isnan(Tibia))= 0;%Tibia = fillzero(Tibia);
VM = VM(start:finish,:);VM(isnan(VM))= 0;%VM = fillzero(VM);
%% Filter Data and Rectify Signal
% [Fz_filt] = filterdata1(Fz,2000,0,50,4,0);
% [Fy_filt] = filterdata1(Fy,2000,0,50,4,0);
% [FzP1_filt] = filterdata1(FzP1,2000,0,50,4,0);
[B,A] = butter(4,[0.02 0.5]);
ES_filt = abs(filter(B,A,ES));
GM_filt = abs(filter(B,A,GM));
GMED_filt = abs(filter(B,A,GMED));
RF_filt = abs(filter(B,A,RF));
SOL_filt = abs(filter(B,A,SOL));
ST_filt = abs(filter(B,A,ST));
TA_filt = abs(filter(B,A,TA));
VM_filt = abs(filter(B,A,VM));



figure; plot(VM_filt, 'r'); hold on;
Fs = 2000;
f_cutoff = 3; N = 2;
Wn = [2*f_cutoff/Fs];
[B,A] = butter(N, Wn,'low');
TA_filt2 = filtfilt(B, A, abs(VM));

plot(TA_filt2, 'b');


%% Load MVC data
for q = subnum-1;
    ES_MVC = MVCs(q,1);
    GM_MVC = MVCs(q,2);
    GMED_MVC = MVCs(q,3);
    RF_MVC = MVCs(q,4);
    SOL_MVC = MVCs(q,5);
    ST_MVC = MVCs(q,6);
    TA_MVC = MVCs(q,7);
    VM_MVC = MVCs(q,8);
end
%% Load Onset Times
for q = subnum -1;
    GM_onval = OnsetTimes(q,1);
    GMED_onval = OnsetTimes(q,2);
    RF_onval = OnsetTimes(q,3);
    SOL_onval = OnsetTimes(q,4);
    VM_onval = OnsetTimes(q,5); 
end  
%% Determine Walking or Running Trial
if gait == 1
% WALKING TRIALs
% Remove signal offset
offset1 = strrep(filename,'G1','G2');
delimiter = '\t';
startRow = 10;
% Format string for each line of text:
formatSpec = '%*q%*q%*q%*q%*q%*q%*q%*q%*q%*q%*q%*q%*q%f%*s%[^\n\r]';
% Open the text file.
fileID = fopen(offset1,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
% Close the text file.
fclose(fileID);
% Allocate imported array to column variable names
offset1 = dataArray{:, 1};
% Clear temporary variables
clearvars delimiter startRow formatSpec fileID dataArray ans;
%Mean value of offset
offset1 = mean(offset1);
%Subtract offset from Force data
FzP1_filt = (FzP1_filt) - (offset1);
%Remove force below zero
offseta = FzP1_filt<0;
offseta = FzP1_filt(offseta,1);
offseta = abs(mean(offseta));
offseta(isnan(offseta))= 0;
FzP1_filt = (FzP1_filt) + (offseta);
% clear unneeded variables
clearvars offset1 offseta;
%% Find peaks
[peaks,locs] = findpeaks(FzP1_filt,'MinPeakProminence',50,'MinPeakDistance',900);
%Remove Partial Peaks
peaks([1 end])=[];
locs([1 end])=[];
%% Finding Heel Strikes and Toe Offs
% Heel Strike
for i = 1:length(peaks)
    k = locs(i);
while k>1 && FzP1_filt(k)> 20 % goes back from the peak to when it is less than 20N then IDs transition here
    k = k-1;
end
HS(i,:) = k;
end
HS = unique(HS);
HSval = FzP1_filt(HS,:);
% Toe Off
for i = 1:length(peaks)
    k = locs(i);
while k>1 && FzP1_filt(k)> 20 % goes back from the peak  to when it is less than 20N then IDs transition here
    k = k+1;
end
TO(i,:) = k;
end
TO = unique(TO);
TOval = FzP1_filt(TO,:);
%% Use HS & TO to remove offset further
for aa = 1:(length(TO)-1)
    bb = aa + 1;
    offset2(aa,1) = mean(FzP1_filt(TO(aa):HS(bb)));
    offset2(isnan(offset2))= 0;
end
offset2 = mean(offset2);
FzP1_filt = FzP1_filt - offset2;

offsetb = abs(FzP1_filt(FzP1_filt < 0,1));
offsetb = mean(offsetb);
offsetb(isnan(offsetb))= 0;
FzP1_filt = FzP1_filt + offsetb;

for aa = 1:(length(TO)-1)
    bb = aa + 1;
    offset3(aa,1) = mean(FzP1_filt(TO(aa):HS(bb)));
    offset3(isnan(offset3))= 0;
end
offset3 = mean(offset3);
FzP1_filt = FzP1_filt - offset3;

offsetc = FzP1_filt < 0;
offsetc = abs(mean(offsetc));
offsetc(isnan(offsetc))= 0;
FzP1_filt = FzP1_filt + offsetc;
% clear unneeded variables
clearvars offset2 offset3 offsetb offsetc;
%% Find Peaks, HS & TO again with offset removed
[peaks,locs] = findpeaks(FzP1_filt,'MinPeakProminence',50,'MinPeakDistance',900);
%Remove Partial Peaks
peaks([1 end])=[];
locs([1 end])=[];
% Heel Strike
for i = 1:length(peaks)
    k = locs(i);
while k>1 && FzP1_filt(k)> 20 % goes back from the peak to when it is less than 20N then IDs transition here
    k = k-1;
end
HS(i,:) = k;
end
% Toe Off
for i = 1:length(peaks)
    k = locs(i);
while k>1 && FzP1_filt(k)> 20 % goes back from the peak  to when it is less than 20N then IDs transition here
    k = k+1;
end
TO(i,:) = k;
end
%Eliminate any extras
for aa=1:(length(HS)-1)
    bb = aa+1;
    if HS(bb) - HS(aa) < 50
        HS(aa) = 0;
    end
    if TO(bb) - TO(aa) < 50
        TO(aa) = 0;
    end
end
zeros =find(HS == 0);
HS(zeros) = [];
zeros =find(TO== 0);
TO(zeros) = [];

HS = unique(HS);
HSval = FzP1_filt(HS,:);
TO = unique(TO);
TOval = FzP1_filt(TO,:);
% %% Plotting HS and TO
% u = 0:(length(FzP1_filt)-1);
% U = u';
% plot(u,FzP1_filt,'b');
% hold on
% plot(locs,peaks,'mo');
% plot(HS,HSval,'ro');
% plot(TO,TOval,'ko');
% refline(0,0);

%% RUNNING TRIALS
elseif gait == 2
% Find Peaks for offsest removal
[peaks,locs] = findpeaks(Fz_filt,'MinPeakDistance',100,'MinPeakProminence',500);
%Remove Partial Peaks
peaks([1 end])=[];
locs([1 end])=[];
%Remove Offset
for aa = 1:(length(locs)-1)
    bb = aa + 1;
    offset1(aa,1) = min(Fz_filt(locs(aa):locs(bb)));
    offset1(isnan(offset1))= 0;
end

offset1 = mean(offset1);
Fz_filt = Fz_filt - offset1;

offseta = (Fz_filt(Fz_filt < 0,1));
offseta = abs(mean(offseta));
offseta(isnan(offseta))= 0;
Fz_filt = Fz_filt + offseta;

for aa = 1:(length(locs)-1)
    bb = aa + 1;
    offset2(aa,1) = min(Fz_filt(locs(aa):locs(bb)));
    offset2(isnan(offset2))= 0;
end

offset2 = mean(offset2);
Fz_filt = Fz_filt - offset2;

offsetb = (Fz_filt(Fz_filt < 0,1));
offsetb = abs(mean(offsetb));
offsetb(isnan(offsetb))= 0;
Fz_filt = Fz_filt + offsetb;
% clear unneeded variables
clearvars offset1 offseta offset2 offsetb;
%% Finding Heel Strikes and Toe Offs
% Heel Strike
for i = 1:length(peaks)
    k = locs(i);
while k>1 && Fz_filt(k)> 20 % goes back from the peak to when it is less than 20N then IDs transition here
    k = k-1;
end

HS(i,:) = k;

end
HS = unique(HS);
HSval = Fz_filt(HS,:);

% Toe Off
for i = 1:length(peaks)
    k = locs(i);
while k>1 && Fz_filt(k)> 20 % goes forward from the peak to when it is less than 20N then IDs transition here
    k = k+1;
end

TO(i,:) = k;

end
TO = unique(TO);
TOval = Fz_filt(TO,:);

%% Use HS and TO to further remove offset
for aa = 1:(length(TO)-1)
    bb = aa + 1;
    offset3(aa,1) = mean(Fz_filt(TO(aa):HS(bb)));
    offset3(isnan(offset3))= 0;
end

offset3 = mean(offset3);
Fz_filt = Fz_filt - offset3;

offsetc = abs(Fz_filt(Fz_filt < 0,1));
offsetc = mean(offsetc);
offsetc(isnan(offsetc))= 0;
Fz_filt = Fz_filt + offsetc;

for aa = 1:(length(TO)-1)
    bb = aa + 1;
    offset4(aa,1) = mean(Fz_filt(TO(aa):HS(bb)));
    offset4(isnan(offset4))= 0;
end

offset4 = mean(offset4);
Fz_filt = Fz_filt - offset4;

offsetd = Fz_filt < 0;
offsetd = abs(mean(offsetd));
offsetd(isnan(offsetd))= 0;
Fz_filt = Fz_filt + offsetd;
% clear unneeded variables
clearvars offset3 offsetc offset4 offsetd;
%% Find Peaks, HS & TO again with offset removed
[peaks,locs] = findpeaks(Fz_filt,'MinPeakDistance',100,'MinPeakProminence',600);
%Remove Partial Peaks
peaks([1 end])=[];
locs([1 end])=[];

%% New HS and TO
% Find HS and TO with offset removed
% Heel Strike
for i = 1:length(peaks)
    k = locs(i);
while k>1 && Fz_filt(k)> 20 % goes back from the peak to when it is less than 20N then IDs transition here
    k = k-1;
end

HS(i,:) = k;

end

% Toe Off
for i = 1:length(peaks)
    k = locs(i);
while k>1 && Fz_filt(k)> 20 % goes forward from the peak to when it is less than 20N then IDs transition here
    k = k+1;
end

TO(i,:) = k;

end

%Eliminate any extras
for aa=1:(length(HS)-1)
    bb = aa+1;
    if HS(bb) - HS(aa) < 50
        HS(aa) = 0;
    end
    if TO(bb) - TO(aa) < 50
        TO(aa) = 0;
    end
end

zeros =find(HS == 0);
HS(zeros) = [];
zeros =find(TO== 0);
TO(zeros) = [];

HS = unique(HS);
TO = unique(TO);
HSval = Fz_filt(HS,:);
TOval = Fz_filt(TO,:);


%% Plotting HS and TO
% u = 0:(length(Fz_filt)-1);
% U = u';
% figure(1)
% plot(u,Fz_filt,'b');
% hold on
% plot(locs,peaks,'ko');
% plot(GM_filt,'g');
% plot(HS,HSval,'ko');
% plot(TO,TOval,'ro');
% refline(0,0);

%% Determine Left Foot
HSodd = HS(1:2:end);
HSvalo = HSval(1:2:end);
HSeven = HS(2:2:end);
HSvale = HSval(2:2:end);

TOodd = TO(1:2:end);
TOvalo = TOval(1:2:end);
TOeven = TO(2:2:end);
TOvale = TOval(2:2:end);

locso = locs(1:2:end);
locse = locs(2:2:end);
peakso = peaks(1:2:end);
peakse = peaks(2:2:end);

for aa = 1:length(HSeven);
    if mean(GM_filt(HSodd(aa):TOodd(aa))) > mean(GM_filt(HSeven(aa):TOeven(aa)))
        HSLeft = HSodd;
        HSvalL = HSvalo;
        TOLeft = TOodd;
        TOvalL = TOvalo;
        locsL = locso;
        peaksL = peakso;
        
        HSRight = HSeven;
        HSvalR = HSvale;
        TORight = TOeven;
        TOvalR = TOvale;
        locsR = locse;
        peaksR = peakse;
        
    elseif  mean(GM_filt(HSeven(aa):TOeven(aa))) > mean(GM_filt(HSodd(aa):TOodd(aa)))
        HSLeft = HSeven;
        HSvalL = HSvale;
        TOLeft = TOeven;
        TOvalL = TOvale;
        locsL = locse;
        peaksL = peakse;
        
        HSRight = HSodd;
        HSvalR = HSvalo;
        TORight = TOodd;
        TOvalR = TOvalo;
        locsR = locso;
        peaksR = peakso;
    end
end

    
Fz_Left = Fz_filt;
for aa = 1:length(HSRight)
    Fz_Left(HSRight(aa):TORight(aa)) = 0;
end

% figure(2)
% u = 0:(length(Fz_filt)-1);
% U = u';
% plot(u,Fz_Left,'b');
% hold on
% plot(locsL,peaksL,'ko');
% plot(GM_filt,'g');
% plot(HSLeft,HSvalL,'ro');
% plot(TOLeft,TOvalL,'ko');

%% Onset Time
[GM_onsettime,GM_on,GM_mm] = GM_onset(GM_filt,GM_onval,HSLeft,TOLeft);
[GMED_onsettime,GMED_on,GMED_mm] = GMED_onset(GMED_filt,GMED_onval,HSLeft,TOLeft);
[RF_onsettime,RF_on,RF_mm] = RF_onset(RF_filt,RF_onval,HSLeft,TOLeft);
[SOL_onsettime,SOL_on,SOL_mm] = SOL_onset(SOL_filt,SOL_onval,HSLeft,TOLeft);
[VM_onsettime,VM_on,VM_mm] = VM_onset(VM_filt,VM_onval,HSLeft,TOLeft);

% % Plotting Onset
figure(1);plot(GM_filt,'g');hold on;plot(GM_on,GM_onval,'ro');plot(HSLeft,GM_onval,'ko');plot(TOLeft,GM_onval,'bo');refline(0,GM_onval);plot(GM_mm);title('GM');
figure(2);plot(GMED_filt,'g');hold on;plot(GMED_on,GMED_onval,'ro');plot(HSLeft,GMED_onval,'ko');plot(TOLeft,GMED_onval,'bo');refline(0,GMED_onval);plot(GMED_mm);title('GMED');
figure(3);plot(RF_filt,'g');hold on;plot(RF_on,RF_onval,'ro');plot(HSLeft,RF_onval,'ko');plot(TOLeft,RF_onval,'bo');refline(0,RF_onval);plot(RF_mm);title('RF');
figure(4);plot(SOL_filt,'g');hold on;plot(SOL_on,SOL_onval,'ro');plot(HSLeft,SOL_onval,'ko');plot(TOLeft,SOL_onval,'bo');refline(0,SOL_onval);plot(SOL_mm);title('SOL');
figure(5);plot(VM_filt,'g');hold on;plot(VM_on,VM_onval,'ro');plot(HSLeft,VM_onval,'ko');plot(TOLeft,VM_onval,'bo');refline(0,VM_onval);plot(VM_mm);title('VM');

%% Loading Rate
for q = subnum - 1;
    mass = Anthro(q,3);
end
for q = 1:length(HSLeft);
    if load == 1 && subnum == 4 && steprate == 0 && q==19
        continue;
    end
    [PIF(q,1) PIFindex(q,1) PAF(q,1) PAFindex(q,1) AVR(q,1) IVR(q,1)]= getImpacts((Fz_Left(HSLeft(q)+3:TOLeft(q)-3)),mass,2000);
end
%% Loading data into Excel for SS BME Presentation
%Average Onset time
% file1 = 'E:\Load Carriage\SS BME Presentation\OT.xlsx';
% 
% GMmean = mean(GM_onsettime);
% GMEDmean = mean(GMED_onsettime);
% RFmean = mean(RF_onsettime);
% SOLmean = mean(SOL_onsettime);
% VMmean = mean(VM_onsettime);
% 
% if load == 1 && steprate == 0
%     q = 'B2';
% elseif load==2 && steprate == 0
%     q = 'B3';
% elseif load == 3 && steprate == 0
%     q = 'B4';
% elseif load == 4 && steprate == 0
%     q = 'B5';
% elseif load == 1 && steprate == 1
%     q = 'B9';
% elseif load==2 && steprate == 1
%     q = 'B10';
% elseif load == 3 && steprate == 1
%     q = 'B11';
% elseif load == 4 && steprate == 1
%     q = 'B12';
% elseif load == 1 && steprate == 2
%     q = 'B16';
% elseif load==2 && steprate == 2
%     q = 'B17';
% elseif load == 3 && steprate == 2 
%     q = 'B18';
% elseif load == 4 && steprate == 2
%     q = 'B19';   
% end
% 
% T1 = table(GMmean,GMEDmean,RFmean,SOLmean,VMmean);
% writetable(T1,file1,'sheet',subnum,'writevariableNames',false,'Range',q);

% % Average loading rate
% file2 = 'E:\Load Carriage\SS BME Presentation\LoadingRate.xlsx';
% LR = AVR;
% zeros =find(LR == 0);
% LR(zeros) = [];
% LRmean = mean(LR);
% 
% % no steprate
% if load == 1 && steprate == 0
%     q = 'B2';
% elseif load==2 && steprate == 0
%     q = 'B3';
% elseif load == 3 && steprate == 0
%     q = 'B4';
% elseif load == 4 && steprate == 0
%     q = 'B5';
% % +10% steprate
% elseif load == 1 && steprate == 1
%     q = 'C2';
% elseif load==2 && steprate == 1
%     q = 'C3';
% elseif load == 3 && steprate == 1
%     q = 'C4';
% elseif load == 4 && steprate == 1
%     q = 'C5';
% % -10% steprate
% elseif load == 1 && steprate == 2
%     q = 'D2';
% elseif load==2 && steprate == 2
%     q = 'D3';
% elseif load == 3 && steprate == 2
%     q = 'D4';
% elseif load == 4 && steprate == 2
%     q = 'D5';
% end
% 
% T2 = table(LRmean);
% writetable(T2,file2,'sheet',subnum,'writevariableNames',false,'Range',q);

% %Loading rate vs onset time
% if load == 1
%     file3 = 'E:\Load Carriage\SS BME Presentation\L1_LRvsOT.xlsx';
% elseif load == 2
%     file3 = 'E:\Load Carriage\SS BME Presentation\L2_LRvsOT.xlsx';
% elseif load == 3
%     file3 = 'E:\Load Carriage\SS BME Presentation\L3_LRvsOT.xlsx';
% elseif load == 4
%     file3 = 'E:\Load Carriage\SS BME Presentation\L4_LRvsOT.xlsx';
% end
% 
% T3 = table(AVR,GM_onsettime,GMED_onsettime,RF_onsettime,SOL_onsettime,VM_onsettime);
% writetable(T3,file3,'sheet',subnum,'writevariableNames',false,'Range','A2');
% 
% % Load vs PAF
% file4 = 'E:\Load Carriage\SS BME Presentation\LoadvsPAF.xlsx';
% 
% PAFmean = mean(PAF);
% T4 = table(PAFmean);
% 
% if load == 1 && steprate == 0
%     q = 'B2';
% elseif load==2 && steprate == 0
%     q = 'B3';
% elseif load == 3 && steprate == 0
%     q = 'B4';
% elseif load == 4 && steprate == 0
%     q = 'B5';
% elseif load == 1 && steprate == 1
%     q = 'C2';
% elseif load==2 && steprate == 1
%     q = 'C3';
% elseif load == 3 && steprate == 1
%     q = 'C4';
% elseif load == 4 && steprate == 1
%     q = 'C5';
% elseif load == 1 && steprate == 2
%     q = 'D2';
% elseif load==2 && steprate == 2
%     q = 'D3';
% elseif load == 3 && steprate == 2
%     q = 'D4';
% elseif load == 4 && steprate == 2
%     q = 'D5';
% elseif load == 1 && steprate == 5
%     q = 'E2';
% end
% writetable(T4,file4,'sheet',subnum,'writevariableNames',false,'Range',q);
% 
%% Load data into Excel
file5 = 'E:\Load Carriage\OT&LRdata.xlsx';

GMmean = mean(GM_onsettime);
GMEDmean = mean(GMED_onsettime);
RFmean = mean(RF_onsettime);
SOLmean = mean(SOL_onsettime);
VMmean = mean(VM_onsettime);
LR = AVR;
LRmean = mean(LR)/mass;

if steprate == 6
    q = 'B2';
elseif steprate == 4
    q = 'B3';
elseif steprate == 2
    q = 'B4';
elseif steprate == 0
    q = 'B5';
elseif steprate == 1
    q = 'B6';
elseif steprate == 3
    q = 'B7';
elseif steprate == 5
    q = 'B8';
end

T5 = table(GMmean,GMEDmean,RFmean,SOLmean,VMmean,LRmean);
writetable(T5,file5,'sheet',subnum,'writevariableNames',false,'Range',q);
end