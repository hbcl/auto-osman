function [poWorkWalk poWorkWalkCum stepIndWalk poWorkRun poWorkRunCum stepIndRun] = getSimEnergy5(data, numPOsBefore, numPOsAfter, d, nominalPushOffWorkRW, dataStr, pushOffType)
                
%this function is to plot realtive to bf and af steps. Step 0 is on bump
%step with poush off 0
poWorkWalk = [];
poWorkRun = [];

poWorkRunCum = [];
poWorkWalkCum = [];

stepIndWalk = [];
stepIndRun = [];

mSize = 15;


if strcmp(pushOffType, 'preemptive')
    lineTypeStr = '-';
else
    lineTypeStr = '-.';
end


for k = 1:length(d)  
    str = strcat('numPOsBf', num2str(numPOsBefore(k)),'Af', num2str(numPOsAfter(k)), dataStr, num2str(d(k)));%<-----you may need to change this string
    stepNum = numPOsBefore(k) + numPOsAfter(k);
%        numPOsBf5Af5timeGain1_timeGainspeed1Percent
    if isfield(data,(str))       %if this steps with this delay exists                         
        if isempty(data.(str).jumpingSteps) %walk
            poWorkWalk = [poWorkWalk sum(data.(str).po_work(2:end-1))-stepNum*nominalPushOffWorkRW]; %relative
            poWorkWalkCum = [poWorkWalkCum sum(data.(str).po_work(2:end-1))];  %when cumulative substract the delayed step to get the real total work 
            stepIndWalk = [stepIndWalk k];            
        else%run or leap off
            poWorkRun = [poWorkRun sum(data.(str).po_work(2:end-1))-stepNum*nominalPushOffWorkRW];%relative
            poWorkRunCum = [poWorkRunCum sum(data.(str).po_work(2:end-1))];   
            stepIndRun = [stepIndRun k];
        end
    end 
end

