function deleteTrialFromGroup(data, trialNum, targetDirect)



if ~isempty(find(trialNum == data.savedTrials, 1))
    data.savedTrials(find(trialNum == data.savedTrials, 1)) = [];
end
data.(data.trialTypeString{trialNum}).trial{trialNum} = [];


tempind = find(trialNum == data.badTrials,1);
if isempty( tempind)
    data.badTrials = [data.badTrials trialNum];
end


currentFolder = cd;
cd(targetDirect);   
save(strcat(data.name,'_stochasticSegmented'),'data'); 
cd(currentFolder);  