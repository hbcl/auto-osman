% Author: Lauro Ojeda, 2012-2015
% Adapted by: Joel Dauter, 2019
% Use this function to get data from imu files. Now handles syncing variable number of imus
% In order to provide user defined sync values provide ReadImuParams object with modified sync values
function [WB, AB, MB, static_period, PERIOD] = get_imu_data(FILES, params); %<--------------static_period
  
    % If parameters not provided create default set of parameters
    if(~exist('params', 'var'))
          params = ReadImuParams();
      end

      SYNC = params.SYNC;
      FORCE_SYNC_VALUE = params.FORCE_SYNC_VALUE;


    % read h5 files and add data to info array
    [num_files, file_name, file_info, time, FREQ] = read_file_info(FILES);

    %Calculate the period
    PERIOD = 1/double(FREQ);

    %Trim indices for Sections so that all imus for section are the same length
    SECTION = sync_indices(time, PERIOD, num_files, SYNC, FORCE_SYNC_VALUE);

    %Load data from imu files using sections indices
    [WB, AB, static_period, MB] = load_imu_data(file_name, SECTION, params);

end

