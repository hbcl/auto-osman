clear ; close all; clc; beep off;
restoredefaultpath;
addpath('D:\spare\SteppingOnFoamImu\stride_estimation\');


addpath('C:\Program Files\Motion Studio\workspace\MyProject');


[left_Wb, left_Ab, right_Wb, right_Ab, PERIOD, ~, ~, static_period] = sync_apdm('20180312-133953_osmanRandomPilot_sensor_780_label_Left.h5','20180312-133955_osmanRandomPilot_sensor_1027_label_Right.h5');
% 20180312-133958_osmanRandomPilot_sensor_779_label_Left
% 20180312-133953_osmanRandomPilot_sensor_780_label_Left.h5
figname = 'fig1Frwrd_780left_Osman2'
%%
% Define the section (in seconds) that will need to be processed and segment the IMU data accordingly
SECTION = [550 596];
% 550 596
%460 540
%%
% [left_Wb,left_Ab] = getdata(left_Wb,left_Ab,PERIOD,SECTION);
% [right_Wb,right_Ab] = getdata(right_Wb,right_Ab,PERIOD,SECTION);
%%

[left_Wb,left_Ab] = getdata(left_Wb, left_Ab,PERIOD,SECTION);
left_walk_info = compute_pos(left_Wb, left_Ab,PERIOD);

[right_Wb,right_Ab] = getdata(right_Wb, right_Ab, PERIOD,SECTION);
right_walk_info = compute_pos(right_Wb, right_Ab, PERIOD);

% Process data from the two IMUs simultaneously 
% [left_walk_info,right_walk_info] = compute_pos_two_imus(left_Wb,left_Ab,right_Wb,right_Ab,PERIOD);

% Segment steps
left_strides = stride_segmentation(left_walk_info,PERIOD);
right_strides = stride_segmentation(right_walk_info,PERIOD);

% Plot results for left and right foot
fig1 = figure;
subplot(1,2,1);
plt_ltrl_frwd_strides(left_strides,0);
subplot(1,2,2);
plt_ltrl_frwd_strides(right_strides,0);
matchaxes;

figure;
subplot(2,1,1)
plt_frwd_elev_strides(left_strides,0);
subplot(2,1,2)
plt_frwd_elev_strides(right_strides,0);
matchaxes;

figure;
subplot(1,2,1)
plt_stride_var(left_strides,0);
subplot(1,2,2)
plt_stride_var(right_strides,0);
matchaxes;

figspeedH = figure; hold on
plot(left_strides.frwd_speed, 'b'); plot(left_strides.frwd_speed, 'b.');
plot(right_strides.frwd_speed, 'r'); plot(right_strides.frwd_speed, 'r.');

figure(fig1)
title(figname)
 saveOneFigure(fig1, figname, 'C:\Users\osman\Desktop\testOldImu')