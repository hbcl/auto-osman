function section_choice(imu, imu_choice, analysis, sections, imuAnalysisParameters)
    title = 'Choose Data to Analyze'

    window_width = 400;
    window_height = 300;
    screen = get(0, 'ScreenSize');
    position = [((screen(3) - window_width)/2) ((screen(4) - window_height)/2) window_width window_height];
    margin = 100;
    section_labels = cell(1,length(sections));
    indices = [];
    trials = [];
    trial = [];

    if(~exist('num_choices', 'var'))
        num_choices = 1;
    end

    for i = 1:length(sections)
        section_labels{i} = sections(i).name;
    end

    section_labels = [{""} section_labels];


    d = dialog('Visible', 'on',     ...
                'Name', title,      ...
                'Position', position);

    for i=1:num_choices
        uicontrol('Parent',d,...
                  'Style','text',...
                  'Position',[margin-50 210  300 50],...
                  'String', 'Choose section containing trial to analyze.');

        section = uicontrol('Parent', d,                ...
                   'Style', 'popupmenu',                ...
                   'Position', [margin, 185, 200, 50],  ...
                   'String', section_labels,            ...
                   'Callback', @chooseTrial_callback);
    end

    function chooseTrial_callback(~,~)
    
        section = sections(section.Value - 1)
        trials = section.trials
        trial_labels = cell(1, length(trials))

        for i=1:length(section.trials)
            trial_labels{i} = trials(i).name;
        end
        trial_labels = [{""} trial_labels];


        uicontrol('Parent',d,...
               'Style','text',...
               'Position',[margin 145  200 50],...
               'String', 'Choose trial to analyze.');

        trial = uicontrol('Parent', d,                ...
                'Style', 'popupmenu',                ...
                'Position', [margin, 120, 200, 50],  ...
                'String', trial_labels,              ...
                'Callback', @populateTrials_callback);
    end

    function populateTrials_callback(~,~)

         % OK
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'OK', ...
        'Position', [200, 30, 70, 30] , ...
        'Callback', @ok_Callback);

    % Cancel
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'Cancel', ...
        'Position', [280, 30, 70, 30] , ...
        'Callback', @cancel_Callback);
    end

    function ok_Callback(~,~)
        d.Visible = 'off';
        indices = [trials(trial.Value - 1).startTime trials(trial.Value - 1).endTime];
        analysis(imu, imu_choice, indices, imuAnalysisParameters);
        delete(d);
    end


    function cancel_Callback(~,~)
        delete(d);
    end


end



            
