% Class for creating a Parameters object, an object that has parameters as properties.
% Extending dynamicprops allows addprop function to be used to add properties dynamically to 
% an object of this class.
% Author Joel Dauter
classdef Parameters < dynamicprops
    properties
    end
    methods
        function obj = methodName(obj)
        end
        
        function out = deepCopy(obj)

            out = Parameters();
            props = properties(obj);

            for i=1:length(props)
                addprop(out, props{i});
                out.(props{i}) = obj.(props{i});
            end
        end
    end
end

