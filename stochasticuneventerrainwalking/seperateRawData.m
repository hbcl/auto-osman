function [LeftWbOut, LeftAbOut, RightWbOut, RightAbOut, HandWbOut, HandAbOut, WaistWbOut, WaistAbOut] =...
    seperateRawData(LeftWb, LeftAb, RightWb, RightAb, HandWb, HandAb, WaistWb, WaistAb, cutTime1, cutTime2, PERIOD)



strTitle = {'Left', 'Right', 'Hand', 'Waist'};



dataWb = {LeftWb, RightWb, HandWb, WaistWb};
dataAb = {LeftAb, RightAb, HandAb, WaistAb};


for i = 1:length(dataWb)
    realTimeTemp = (1:size(dataWb{i},1))*PERIOD;
    tempIndex1 = find(realTimeTemp >= cutTime1);
    tempIndex2 = find(realTimeTemp >= cutTime2);

    try
        dataWb2{i} = dataWb{i}(tempIndex1(1):tempIndex2(1),:);
        dataAb2{i} = dataAb{i}(tempIndex1(1):tempIndex2(1),:);
    catch
        warning('one imu should be out battery so getting what it has, check it')
        keyboard
        dataWb2{i} = dataWb{i}(tempIndex1(1):end,:);
        dataAb2{i} = dataAb{i}(tempIndex1(1):end,:);
    end

    realTime{i} = (1:size(dataWb2{i},1))*PERIOD;
    
    figure; 
    subplot(2,1,1); plot(realTime{i}, dataWb2{i}); ylabel('Wb');title(strTitle{i})
    subplot(2,1,2); plot(realTime{i}, dataAb2{i}); ylabel('Ab');
end



LeftWbOut =  dataWb2{1};
LeftAbOut =  dataAb2{1}; 

RightWbOut = dataWb2{2}; 
RightAbOut =  dataAb2{2};  

HandWbOut = dataWb2{3}; 
HandAbOut =  dataAb2{3};

WaistWbOut = dataWb2{4};  
WaistAbOut  =  dataAb2{4};

