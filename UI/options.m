
%% options %%
% Options gui to choose options for the analysis of code. If select is provided (true) than 
% an options dialog is shown. If select is not provided (false) then the parameters are returned.
% If there are previously loaded params in the project those are returned otherwise the default 
% parameters are returned. 
% Author Joel Dauter
%
function [params_out] = options(params, select)
%%%%%%%%%%%%%%%%%%%% BEGIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%
    addpath('../imu-data-analysis/stride_estimation')
    addpath('../analyse')
    
    
    % Default parameters
    if isempty(params)
        params = combineParams({IMUAnalysisParameters(), ReadDisplayParams()});
    end

    % Unless new params saved returned provided params, "Deep copy" is creating to maintain
    % this object seperate as operations are performed on params 
    % (neccasary as descendant from dynamicprops)
    params_out = params.deepCopy();
    
    params_inputElements = [];
    
    % Dialog layout parameters
    nParams = countParameters(params);
    
    %Number of rows in the options dialog (Including titles)
    nRows = nParams + length(properties(params));

    ok_button_height = 30;
    margin = 50;
   
    % Users screen size
    screensize = get(groot, 'Screensize');
  
    %%Size of options figure
    box_left = 360;
    box_top = 800;
    box_height = screensize(4) * 0.65;
    box_bottom = 100;
    box_width = 550;

    %Height of each label/text area
    element_height = 30;
    % Width of each label
    description_width = 300;
    %Height of the scrolling box within the panel
    scroll_box_height = nRows * element_height + 60;

    % Create dialog
    d = uifigure('Visible','off',...
        'Name', 'Parameter Options', ...
        'Position', [box_left, box_bottom, box_width, box_height]);

    % Panel to put labels/text areas in to allow scrolling
    panel = uipanel(d, ...
        'Position', [0, 50, box_width, box_height - 50], ...
        'Scrollable', 'on');

    function params = combineParams(param_arr)
      params = Parameters();
      for j=1:length(param_arr)
        param_class_names = properties(param_arr{j});
        for h=1:length(param_class_names)
          param_field_names = properties(param_arr{j}.(param_class_names{h}));
          addprop(params, param_class_names{h});
          params.(param_class_names{h}) = param_arr{j}.(param_class_names{h});
        end

      end
    end
    
    %%Inserts the provided vield into the panel at the given row
    function [input, newRow] = insertInputField(fieldName, fieldValue, row)
      uilabel(panel, ...
          'Position',[margin (scroll_box_height-(row*element_height)-30)  description_width element_height-5],...
          'Text', fieldName, ...
          'HorizontalAlignment', 'left');
      input = uitextarea(panel, ...
          'Value', fieldValue, ...
          'Position',[(margin + description_width) (scroll_box_height-(row*element_height) - 30)  150 element_height-5]);
      newRow = row+1;
    end

    function newRow = insertTitleField(title, row)
      uilabel(panel, ...
          'Position',[0 (scroll_box_height-(row*element_height) - 30) box_width element_height-5],...
          'Text', title, ...
          'HorizontalAlignment', 'center', ...
          'FontWeight', 'bold');
      newRow = row+1;
    end

    uibutton(d, 'push', ...
        'Text', 'Save', ...
        'Position', [margin + description_width + 100 10 70, 30] , ...
        'ButtonPushedFcn', @save_Callback);

    uibutton(d, 'push', ...
            'Text', 'Load', ...
            'Position', [margin + description_width, 10, 70, 30], ...
            'ButtonPushedFcn', @load_Callback);
    
    loadValues();
        
    % Run dialog
    if select
        d.Visible = 'on';
        uiwait(d);
    end
    
    % Save button
    function save_Callback(~,~)
        params_inputStrings = fmapParams(@(inputElem) inputElem.Value{1}, params_inputElements);
        params_out = fmapParams(@str2num, params_inputStrings);
        delete(d);
    end

    %Load button
    function load_Callback(~,~)
      [fn,pn] = uigetfile('*.mat');
      d.Visible = 'off';
      d.Visible = 'on';
      addpath(pn);
      info = load(fn, 'projectInfo');
      params = info.projectInfo.parameters;
      reloadValues();
    end

    %Reload parameter values
    function reloadValues()
        %Pass in loaded parameters delete panel and recreate panel calling
        %draw code
        % panel.Visible = "off";
        % params.foot_fall_detection_params.foot_fall_angular_velocity_maximum = 35;
        panel = uipanel(d, ...
          'Position', [0, 50, box_width, box_height - 50], ...
          'Scrollable', 'on');
        % panel.Visible = "on";
        loadValues();
    end

    function loadValues()
      params_strings = fmapParams(@num2str, params);
      [params_inputElements, ~] = fmapParamsWithFieldAndAcc(@insertInputField, @insertTitleField, 1, params_strings);
    end
end

%% Functional-style helpers

function nParams = countParameters(imuAnalysisParameters)
  nParams = foldParams(@(n, ~) n+1, 0, imuAnalysisParameters);
end

function acc = foldParams(f, z, imuAnalysisParameters)
  g = @(fieldName, fieldValue, acc) deal(fieldValue, f(acc, fieldValue));
  [~, acc] = fmapParamsWithFieldAndAcc(g, [], 0, imuAnalysisParameters)
end

function newParams = fmapParams(f, imuAnalysisParameters)
  g = @(fieldName, fieldValue, acc) deal(f(fieldValue), acc);
  [newParams, ~] = fmapParamsWithFieldAndAcc(g, [], 0, imuAnalysisParameters)
end

% This function assumes that `imuAnalysisParameters` is an object with fields
% holding objects, where those objects have fields holding (arrays of)
% numerical values.
function [imuAnalysisParameters, acc] = ...
  fmapParamsWithFieldAndAcc(f, g, z, imuAnalysisParameters)

  acc = z;
  paramClassNames = properties(imuAnalysisParameters);
  for i = 1:length(paramClassNames)
    paramObj = imuAnalysisParameters.(paramClassNames{i});
    fieldNames = properties(paramObj);
    
    % If g function provided pass title and row number
    if ~isempty(g)
      acc = g(paramClassNames{i}, acc);
    end
    
    for j = 1:length(fieldNames)
      [paramObj.(fieldNames{j}), acc] = ...
        f(fieldNames{j}, paramObj.(fieldNames{j}), acc);
    end
    imuAnalysisParameters.(paramClassNames{i}) = paramObj;
  end
end
