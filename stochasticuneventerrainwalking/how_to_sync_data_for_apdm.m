% DIR = '/home/lojeda/Samples/uofm/hbcl/exper/GaitVariabilityAndBalance/ElderSubjects/09072012/';
close all
DIR = 'D:\spare\stochasticTerrainUnevenWalking\somedata\FarukUneven\';
addpath('C:\Users\odarici\Desktop\spare\SteppingOnFoamImu\stride_estimation');

% L_FILE = '20180312-220802_pilotUnevenFaruk_sensor_1378_label_Left.h5';
% R_FILE = '20180312-220800_pilotUnevenFaruk_sensor_1390_label_Right.h5';

L_FILE = '20180312-220443_pilotUnevenFarukOldApdm_sensor_780_label_Left.h5';
R_FILE = '20180312-220437_pilotUnevenFarukOldApdm_sensor_1027_label_Right.h5';

figname = 'fig1Frwrd_780left_Faruk2'



FILE1 = sprintf('%s%s',DIR,L_FILE);
FILE2 = sprintf('%s%s',DIR,R_FILE);

DATA_IS_SYNCED = false

if(DATA_IS_SYNCED)

	[Wb1,Ab1,Wb2,Ab2,PERIOD] = sync_apdm(FILE1,FILE2); 
 

	% Verify that the data is synced by looking at Y-axis rates, you should find that the end of one stride coincides with the beginning of the next one on the other file
	% If the data is not synced, follow the next procedure
else
	% If it does not get synchronized:
	% 1: set  SYNC = 0 in sync_apdm
	% 2: Select a valid window of walking data and use:
	SYNC = 0;
	[W1,A1,W2,A2,PERIOD] = sync_apdm(FILE1,FILE2,SYNC);
%     [W1,A1,W2,A2,PERIOD] = sync_New_apdm(FILE1,FILE2,SYNC);

	%% IN MOST CASES SKIPPING THIS AND USING THE WHOLE FILE GIVES THE BEST RESULTS
	if 0% (1)
		SECTION = [1180 1240];%xlim;
		[W1,A1]= getdata(W1,A1,PERIOD,SECTION);
		[W2,A2]= getdata(W2,A2,PERIOD,SECTION);
	end;
	%% END OF SKIP 

	% In some cases, with SECTION  = [], I get better results
	FORCE_SYNC_VALUE = find_data_shift(W1,W2,PERIOD);
	% 4: set  SYNC = 1 
	% 5: Set the FORCE_SYNC_VALUE to the result of this function
	% Run this tool again;
	SYNC = 1;
	[Wb1,Ab1,Wb2,Ab2,PERIOD] = sync_apdm(FILE1,FILE2,SYNC,FORCE_SYNC_VALUE);

	% In the worst case, the FORCE_SYNC_VALUE can be defended manually as follows
	% FORCE_SYNC_VALUE = 832; % Override 
	% [Wb1,Ab1,Wb2,Ab2,PERIOD] = sync_apdm(FILE1,FILE2,SYNC,FORCE_SYNC_VALUE);
    
%      left_Wb = W1; left_Ab = A1; clear A Wb;
%      right_Wb = W2; right_Ab = A2; clear A Wb;
    
    left_Wb = Wb1; left_Ab = Ab1; clear A Wb;
    right_Wb = Wb2; right_Ab = Ab2; clear A Wb;
    
    % Define the section (in seconds) that will need to be processed and segment the IMU data accordingly
    SECTION = [1180 1240];%[1100 1200];%[1180 1240];%[1100 1200];%[1180 1240];
    [left_Wb,left_Ab] = getdata(left_Wb,left_Ab,PERIOD,SECTION);
    [right_Wb,right_Ab] = getdata(right_Wb,right_Ab,PERIOD,SECTION);

    % Process data from the two IMUs simultaneously 
    [left_walk_info,right_walk_info] = compute_pos_two_imus(left_Wb,left_Ab,right_Wb,right_Ab,PERIOD);

%     left_walk_info = compute_pos(left_Wb,left_Ab,PERIOD);
%     right_walk_info = compute_pos(right_Wb,right_Ab,PERIOD);

    % Segment steps
    left_strides = stride_segmentation(left_walk_info,PERIOD);
    right_strides = stride_segmentation(right_walk_info,PERIOD);

    % Plot results for left and right foot
    fig1 = figure;
    subplot(1,2,1);
    plt_ltrl_frwd_strides(left_strides,0);
    subplot(1,2,2);
    plt_ltrl_frwd_strides(right_strides,0);
    matchaxes;

    figure;
    subplot(2,1,1)
    plt_frwd_elev_strides(left_strides,0);
    subplot(2,1,2)
    plt_frwd_elev_strides(right_strides,0);
    matchaxes;

    figure;
    subplot(1,2,1)
    plt_stride_var(left_strides,0);
    subplot(1,2,2)
    plt_stride_var(right_strides,0);
    matchaxes;

    
    figure(fig1)
    title(figname)
     saveOneFigure(fig1, figname, 'C:\Users\osman\Desktop\testOldImu')

    
    W = Wb1; A = Ab1; FILE = [strcat(L_FILE(1:end-3),'new2')]; save(FILE,'W','A','PERIOD');
    W = Wb2; A = Ab2; FILE = [strcat(R_FILE(1:end-3),'new2')]; save(FILE,'W','A','PERIOD');
    
%     
    
    
    
end
 

