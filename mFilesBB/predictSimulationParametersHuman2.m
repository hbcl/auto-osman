function [impulseResponse refOffset] =  predictSimulationParametersHuman2(refSpeed, refTime, bumpHeights, numPOsBefore, bumpInd)%we dont need numPOsAfter, it should be symmetric




PR1Matrix = [];


dataRef.speed = refSpeed(bumpInd-numPOsBefore:end-5);



if isempty(find(0 == refTime))
    refTime = refTime - refTime(bumpInd)
end

dataRef.time = refTime(bumpInd-numPOsBefore:end-5)
bumpInd = find(0 == dataRef.time);
   




figure; plot(dataRef.speed); hold on; plot(dataRef.speed, '.') ; plot([bumpInd bumpInd], ylim, 'k-.')
refOffset = mean(dataRef.speed);

% figure; plot(dataRef.speed-refOffset, 'r'); hold on; plot(dataRef.speed-refOffset, 'r.')
% figure; plot(dataRef.speed-refOffset-0.05, 'r')



lenBumpPR1 = length(dataRef.speed);
tempLen = length(bumpHeights)+bumpInd-1;
bumps = [zeros(1, bumpInd-1) bumpHeights zeros(1, lenBumpPR1-tempLen)];
tempInd = 1;

plusInd = bumpInd-1;
numSteps = 2*plusInd +1;

% bumps = bumps-mean(bumps);

for kk = 1:lenBumpPR1-(plusInd)%plusInd, numSteps
    if plusInd+1 > kk%numSteps+1 > plusInd+i
          temp = [fliplr(bumps(1,tempInd:plusInd-1+kk)) zeros(1, numSteps-(plusInd-1+kk))];%works too, , this is this (epsilon -m to +m)  f[n-m] * h[m]
    else     
        temp = fliplr(bumps(1,tempInd:(numSteps+tempInd-1)))%works, this is this (epsilon -m to +m)  f[n-m] * h[m]
        tempInd = tempInd+1;
    end
    PR1Matrix = [PR1Matrix; temp];
end
PR1Matrix =  [PR1Matrix; zeros(plusInd,numSteps)]; 



% paramInOffset = mean(dataRef.speed);
impulseResponse = PR1Matrix\(dataRef.speed-refOffset);%PR1Matrix\(paramPR1-paramInOffset);

% impulseResponse = impulseResponse -mean(impulseResponse );
figure; plot(impulseResponse); hold on; plot(impulseResponse, '.')  ; plot([bumpInd bumpInd], ylim, 'k-.'); title(strcat(num2str(numPOsBefore), 'steps before after'))

 5