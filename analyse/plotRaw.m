function  [h]  = plotRaw(ax, static_period,PERIOD, Wb, Ab)
%PLOTRAW Summary of this function goes here
%   Plots angular acceleration, and acceleration, and maybe magnetometer
    W = Wb;          % Angular Accel
    A = Ab;          % Acceleation
    t = (1:size(W,1)) * PERIOD; % Time
    n = 2;
    %% Plot angular acceleration
    axes(ax);
    h(1) = subplot(n,1,1);
    hold on;
    plot(t(static_period),W(static_period,:)/PERIOD*180/pi,'.k');
    
    plot(t, W / PERIOD*180/pi);
    pb = pbaspect;
    grid on;
    ylabel('Angular Accel [deg/s]');
    title('Body referenced inertial signals');
    hold off;

    %% Plot acceleration
    h(2) = subplot(n,1,2);
    plot(t,A);
    grid on;
    ylabel('Acceleration [m/s^2]');
    %% If present, plot magnetometer data
%     if(n == 3)
%         M = varargin{ 3 };
%         h(3) = subplot(3,1,3);
%         plot(t,M);
%         grid on;
%         ylabel('Mag ');
%     end;

    xlabel('time [s]');
  

end

