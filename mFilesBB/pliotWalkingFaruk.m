

%use this m file to create the code to analyze the experiment
% you need to check how to fid the HS from acc
%this data uses the experiment that you've tested yourself on the hallway

clear all; clc
restoredefaultpath
addpath('D:\spare\SteppingOnFoamImu\stride_estimation')
addpath(genpath('D:\spare\GaitAnalysisToolBox_Son'));
addpath(genpath('D:\spare\stochasticTerrainUnevenWalking\faruKEMG'));


%%
% load trial1_speed1_0_01;
% load walk_1_25_v2_01
load walk_1_5_v1_01
% filename ='Run_number_41_Plot_and_Store_Rep_4.3.csv'%walk_1_25_v2_01**works
filename = 'Run_number_41_Plot_and_Store_Rep_5.4.csv'%walk_1_5_v1_01 works


close all
cut_off_freq = 25;
Force_freq = 960;
[Bf, Af] = butter(3, 25*2/Force_freq);
N = 1000;
F_factor = diag([500 500 1000]);
M_factor = diag([800 250 400]);

i = 1
LGRF{i} = filtfilt(Bf,Af,samples(:,[1 2 3]))*F_factor;
RGRF{i} = filtfilt(Bf,Af,samples(:,[9 10 11]))*F_factor;
%correct the 2nd dim
RGRF{i}(:,2) = -RGRF{i}(:,2);
LGRF{i}(:,2) = -LGRF{i}(:,2);  
figure; plot(LGRF{i}, 'b');  hold on;  plot(RGRF{i}, 'r'); hold on;
dataLenForce = length(RGRF{i});

%event indices
[RHS{i} LHS{i} RTO{i} LTO{i}] = findEventIndicesForWalkingOnTreadmill(RGRF{i}, LGRF{i}, Force_freq); 

figure;
plot(RGRF{i},'r'); hold on;  plot(LGRF{i},'b');
plot(RHS{i}, RGRF{i}(RHS{i},3),'k*', 'markersize', 10);
plot(LHS{i}, LGRF{i}(LHS{i},3),'k*', 'markersize', 10); xlabel('filtered');

%find the offset and substract from forces
tempSize = length(RTO{i})-1
offsetRZ = []; offsetLZ = []; 
offsetRY = []; offsetLY = [];
offsetRX = []; offsetLX = [];
stepsTOofset = [ 2:4  tempSize-4:tempSize-1];
for j = 1 : tempSize
    tempR = [RTO{i}(j):RHS{i}(j+1)];
    tempL = [LTO{i}(j):LHS{i}(j+1)];
    plot(tempR,  RGRF{i}(tempR, 3),'g*'); plot(tempL,  LGRF{i}(tempL, 3),'g*');

    if find(j == stepsTOofset )
        offsetRZ = [offsetRZ; mean(RGRF{i}(tempR, 3))];
        offsetLZ = [offsetLZ; mean(LGRF{i}(tempL, 3))];

        offsetRY = [offsetRY; mean(RGRF{i}(tempR, 2))];
        offsetLY = [offsetLY; mean(LGRF{i}(tempL, 2))];

        offsetRX = [offsetRX; mean(RGRF{i}(tempR, 1))];
        offsetLX = [offsetLX; mean(LGRF{i}(tempL, 1))];
    end
end

RGRF_offset{i} = RGRF{i} - [mean(offsetRX)*ones(dataLenForce,1) mean(offsetRY)*ones(dataLenForce,1) mean(offsetRZ)*ones(dataLenForce,1)];
LGRF_offset{i} = LGRF{i} - [mean(offsetLX)*ones(dataLenForce,1) mean(offsetLY)*ones(dataLenForce,1) mean(offsetLZ)*ones(dataLenForce,1)];

figure;
plot(RGRF_offset{i},'r'); hold on;  plot(LGRF_offset{i},'b');
plot(RHS{i}, RGRF_offset{i}(RHS{i},3),'k*', 'markersize', 10);
plot(LHS{i}, LGRF_offset{i}(LHS{i},3),'k*', 'markersize', 10); xlabel('filtered offsetter');

%event indices
clear RHS LHS RTO LTO
[RHS{i} LHS{i} RTO{i} LTO{i}] = findEventIndicesForWalkingOnTreadmill(RGRF_offset{i}, LGRF_offset{i}, Force_freq); 

figure;
plot(RGRF_offset{i},'r'); hold on;  plot(LGRF_offset{i},'b');
plot(RHS{i}, RGRF_offset{i}(RHS{i},3),'k*', 'markersize', 10);
plot(LHS{i}, LGRF_offset{i}(LHS{i},3),'k*', 'markersize', 10); xlabel('filtered offsetter');

% figure; plot(RGRF_offset{i}(:,3),'r'); hold on; plot(LGRF_offset{i}(:,3),'b');   title('force 3d')
% figure; plot(right_Ab(:,3),'r'); hold on; plot(left_Ab(:,3),'b'); title(' imu 3d')
%% process DELSYS IMU's on the feet
close all;clc


allDelsysData  = csvread(filename, 1, 0); 
sizeData = size(allDelsysData); rowSize = sizeData(1); colSize = sizeData(2); 
timeEMG = allDelsysData(:,1); 
PERIOD_Del_emg = timeEMG(2);
% muscle = allDelsysData(:,42); % 22 RRF 42 RGM 62 LSOL 82LRF  102 L GM

muscleR{1} = allDelsysData(:,2);%RTA
muscleR{2} = allDelsysData(:,22);%RSOL
muscleR{3} = allDelsysData(:,42);%RRF

muscleR{4} = allDelsysData(:,62);%RGM
muscleR{5} = allDelsysData(:,82);%RGL
muscleR{6} = allDelsysData(:,102);%RVM
muscleR{7} = allDelsysData(:,122);%RVL
muscleR{8} = allDelsysData(:,162);%RMH

muscleR_name = {'RTA', 'RSOL', 'RRF', 'RGM', 'RGL', 'RVM', 'RVL', 'RMH'}; 
muscleNum = length(muscleR_name);

%indexSignal = find(muscle ~=0); %RTA RMV LTA LVM 2 22 42 62
% muscle = muscle(indexSignal); sizeSignal = length(muscle)

%time and foot signals
time_imu_del = allDelsysData(:,183);
PERIOD_Del_imu = time_imu_del(2);
GRAVITY = 9.80297286843;

right_Ab_Del = [allDelsysData(:,184) allDelsysData(:,186) allDelsysData(:,188)]*GRAVITY;%m/s^2
right_Wb_Del = [allDelsysData(:,190) allDelsysData(:,192) allDelsysData(:,194)]*pi/180;%rad/sec

left_Ab_Del = [allDelsysData(:,204) allDelsysData(:,206) allDelsysData(:,208)]*GRAVITY;
left_Wb_Del = [allDelsysData(:,210) allDelsysData(:,212) allDelsysData(:,214)]*pi/180;

tap_del = [allDelsysData(:,144)  allDelsysData(:,146) allDelsysData(:,148)];%TAP

%% find the sections for force and delsys
figure; plot(tap_del(:,2),'r'); %should be the greast pick
clear sect_Del_imu PKS
[PKS, sect_Del_imu] = findpeaks(-tap_del(:,2),'MinPeakDistance', 10000)
% sect_Del_imu(2) = 17011;%16685;
% sect_Del_imu = [sect_Del_imu(1):sect_Del_imu(2)];
% sect_Del_imu = [2313:14237]; %Run_number_41_Plot_and_Store_Rep_4.3
sect_Del_imu = [ 4397:17010]; % Run_number_41_Plot_and_Store_Rep_5.4.csv'

figure;  plot(LGRF_offset{1}(:,3),'r*'); hold on;plot(LGRF_offset{1}(:,3),'b'); %left force plate is tapped
clear sect_Force
% [PKS, sect_Force(1)] = findpeaks(LGRF_offset{1}(1:6000,3),'MinPeakHeight',410)
% [PKS, sect_Force(2)] = findpeaks(LGRF_offset{i}((7.95*1e+4:8.15*1e+4),3),'MinPeakHeight',410)
% sect_Force(2) = sect_Force(2) + 7.95*1e+4 - 1;
% sect_Force(2) = 84695;
% sect_Force = [sect_Force(1):sect_Force(2)];

% sect_Force = [5326:82595]; %walk_1_25_v2_01
sect_Force = [2957:84695]; %walk_1_5_v1_01

tempIndex1 = find(timeEMG >= time_imu_del(sect_Del_imu(1)));
tempIndex2 = find(timeEMG >= time_imu_del(sect_Del_imu(end)));
sect_Del_emg = [tempIndex1(1):tempIndex2(1)];
%%
%get the section data
close all
LGRF_sect = LGRF_offset{i}(sect_Force,:);
RGRF_sect = RGRF_offset{i}(sect_Force,:);


%imu Delsys section
right_Ab_Del_sect = right_Ab_Del(sect_Del_imu,:);
left_Ab_Del_sect =  left_Ab_Del(sect_Del_imu,:);
right_Wb_Del_sect = right_Wb_Del(sect_Del_imu,:);
left_Wb_Del_sect = left_Wb_Del(sect_Del_imu,:);
time_imu_del_sect = time_imu_del(sect_Del_imu,:);

%emg Delsys section
for i = 1:length(muscleR)
    muscleR_sect{i} = muscleR{i}(sect_Del_emg);
end

timeEMG_sect = timeEMG(sect_Del_emg);


% clear tempAb
tempAb = [-right_Ab_Del_sect(:,2) -right_Ab_Del_sect(:,1) -right_Ab_Del_sect(:,3)];
right_Ab_Del_sect = tempAb;
% ylimVal = 50%0.04
% for dim = 1:3
%     figure; plot(right_Ab_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('A right Del dim',num2str(dim)));
%     figure; plot(right_Ab_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('A right Apdm dim',num2str(dim)));
% end

clear tempAb
tempAb = [-left_Ab_Del_sect(:,2) -left_Ab_Del_sect(:,1) -left_Ab_Del_sect(:,3)];
left_Ab_Del_sect = tempAb;
% for dim = 1:3
%     figure; plot(left_Ab_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('A left Del dim',num2str(dim)));
%     figure; plot(left_Ab_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('A left Apdm dim',num2str(dim)));
% end

clear tempWb
tempWb = -[right_Wb_Del_sect(:,2) right_Wb_Del_sect(:,1) right_Wb_Del_sect(:,3)];
right_Wb_Del_sect = tempWb;
% ylimVal = 0.08%0.04
% for dim = 1:3
%     figure; plot(right_Wb_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('W right Del dim',num2str(dim)));
%     figure; plot(right_Wb_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('W right Apdm dim',num2str(dim)));
% end

clear tempWb
tempWb = -[left_Wb_Del_sect(:,2) left_Wb_Del_sect(:,1) left_Wb_Del_sect(:,3)];
left_Wb_Del_sect = tempWb;
% for dim = 1:3
%     figure; plot(left_Wb_Del_sect(:,dim), 'b'); ylim([-ylimVal ylimVal ]); title(strcat('W left Del dim',num2str(dim)));
%     figure; plot(left_Wb_sect(:,dim),'r'); ylim([-ylimVal ylimVal ]); title(strcat('W left Apdm dim',num2str(dim)));
% end

figure; plot(right_Ab_Del(:,3),'r'); hold on; plot(tap_del(:,1),'b'); plot(right_Ab_Del_sect(:,3),'g');
figure; plot(RGRF_sect(:,3),'r');
figure; plot(right_Ab_Del_sect(:,3),'g');


%%

RGRF_sect_interPlt =  interpGaitCycle(RGRF_sect, length(right_Ab_Del_sect));
LGRF_sect_interPlt  =  interpGaitCycle(LGRF_sect, length(right_Ab_Del_sect));
[RHS_sect_interPlt LHS_sect_interPlt RTO_sect_interPlt LTO_sect_interPlt] = findEventIndicesForWalkingOnTreadmill(RGRF_sect_interPlt, LGRF_sect_interPlt, round(1/PERIOD_Del_imu)); 

close all
figHS = figure;
plot(RGRF_sect_interPlt(:,3),'r'); hold on;  plot(LGRF_sect_interPlt(:,3),'b');
plot(RHS_sect_interPlt, RGRF_sect_interPlt(RHS_sect_interPlt,3),'k*', 'markersize', 10);
plot(LHS_sect_interPlt, LGRF_sect_interPlt(LHS_sect_interPlt,3),'k*', 'markersize', 10); xlabel('filtered offsetter');
% plot(RTO_sect_interPlt, RGRF_sect_interPlt(RTO_sect_interPlt,3),'k*', 'markersize', 10);
% plot(LTO_sect_interPlt, LGRF_sect_interPlt(LTO_sect_interPlt,3),'k*', 'markersize', 10);

figure; plot(RGRF_sect_interPlt(:,3),'r'); hold on;  
plot(right_Ab_Del_sect(:,3),'b');

%% find HSs for Delsys 

left_Wb_Del_sect_Amp = (sum((left_Wb_Del_sect.^2)')).^.5;
right_Wb_Del_sect_Amp = (sum((right_Wb_Del_sect.^2)')).^.5;
right_Ab_Del_sect_Amp = (sum((right_Ab_Del_sect.^2)')).^.5;
left_Ab_Del_sect_Amp = (sum((left_Ab_Del_sect.^2)')).^.5;

[HS TO] = findGaitEventUsingFooTAccelerometer(right_Ab_Del_sect_Amp , left_Ab_Del_sect_Amp, PERIOD_Del_imu , 2);
HS_imu_R = HS.right;
HS_imu_L = HS.left;
figure(figHS)
plot(HS_imu_R, RGRF_sect_interPlt(HS_imu_R,3),'m*', 'markersize', 10);
plot(HS_imu_L, LGRF_sect_interPlt(HS_imu_L,3),'m*', 'markersize', 10);

amp_A = right_Ab_Del_sect_Amp;
amp_A_filt = filtfilt(Bf, Af, amp_A);
diff_amp_A = [0 diff(amp_A_filt)];
zc_diff_amp_A = findZeroCrossings(diff_amp_A);
plot(amp_A_filt,'g'); 
plot(diff_amp_A*10,'m')
plot(zc_diff_amp_A, diff_amp_A(zc_diff_amp_A)*10,'m')
% saveOneFigure(figHS, strcat('HSimu'), path)

%% static periosds if necessary
% static period IMU
static_period_Del_imu  =  detect_quite_time(right_Wb_Del_sect*PERIOD_Del_imu, PERIOD_Del_imu); %<----------be careful while using Lauor's code APDM output and                                                                                               %delsys output is not same so you multipled the data by period
figure; 
plot(time_imu_del_sect, right_Wb_Del_sect(:,3)); hold on
%static period EMG
static_period_Del_emg = []; 
for z = 1:length(static_period_Del_imu)
    temp = find(timeEMG_sect >= time_imu_del_sect(static_period_Del_imu(z)));
    static_period_Del_emg = [static_period_Del_emg temp(1)];
end
plot(time_imu_del_sect(static_period_Del_imu ), right_Wb_Del_sect(static_period_Del_imu ), 'k*')

%find non static period and eliminate HS
index = 1:length(right_Wb_Del_sect);  indexNotStatic = [];
for i = 1:index(end)
    if isempty(find(index(i) == static_period_Del_imu))
        indexNotStatic = [indexNotStatic index(i)];
    end
end
plot(time_imu_del_sect(indexNotStatic), right_Wb_Del_sect( indexNotStatic), 'r*')
%% find EMG HS 
% figure; plot(time_imu_del_sect, right_Ab_Del_sect(:,3)); hold on; plot(time_imu_del_sect(HS_imu), zeros(1, length(HS_imu)),'k*');
% plot(time_imu_del_sect, left_Ab_Del_sect(:,3)); plot(time_imu_del_sect(HS_imu), zeros(1, length(HS_imu)),'k*');

paramHS = RHS_sect_interPlt;%HS_imu_R;
HS_emg_R = []; HS_emg_L = [];
for z = 1:length(paramHS)   
    temp = find(timeEMG_sect >= time_imu_del_sect(paramHS(z)));
    HS_emg_R = [HS_emg_R temp(1)];
end
 
paramHS = LHS_sect_interPlt;%HS_imu_L;
for z = 1:length(paramHS) 
    temp = find(timeEMG_sect >= time_imu_del_sect(paramHS(z)));
    HS_emg_L = [HS_emg_L temp(1)];  
end
HS_emg = sort([HS_emg_R HS_emg_L]);
plot(timeEMG_sect(HS_emg), zeros(1, length(HS_emg)), 'bo', 'markersize', 5);

%%
close all; clc
%plot raw emg's to check whether static EMG time is good
fig1 = figure; title('raw EMG with HS')
k = 0; yVal = 500;scale = 1e+6;
base_Param1 = HS_emg_R;
for i = 1:muscleNum
%     subplot(3,2,i+k); 
    subplot(4, 2, i);
    plot(muscleR_sect{i}*scale); hold on
%     plot(static_period_Del_emg, muscleR_sect{i}(static_period_Del_emg)*scale, 'k*')
    plot(base_Param1, muscleR_sect{i}(base_Param1)*scale, 'b*'); ylabel(muscleR_name{i});
    ylim([-yVal yVal]) 
    
%     subplot(3,2,i+k+1); 
%     plot(muscleL_sect{i}*scale); hold on
%     plot(static_period_Del_emg, muscleL_sect{i}(static_period_Del_emg)*scale, 'k*')
%     plot(FF_emg_L, muscleL_sect{i}(FF_emg_L)*scale, 'b*'); ylabel(muscleL_name{i});
%     ylim([-yVal yVal]) 

%     k = k +1;
end
speedStr = '1_50'
path = 'D:\spare\stochasticTerrainUnevenWalking\faruKEMG';
saveOneFigure(fig1, strcat('rawEMGwithHS', speedStr), path)


% Remove baseline offset and abs
offsetRange = 1:round(length(static_period_Del_emg)/5); %this should be enough for offseting
fig2 = figure; title('offsetted and rectifed EMG')
for i = 1:muscleNum
    baseline_offset = mean(muscleR_sect{i}(static_period_Del_emg(offsetRange))); %this should be enough for offseting
    muscleR_sect_offset{i} = abs(muscleR_sect{i} - baseline_offset);
    subplot(4, 2, i); hold on
    plot(muscleR_sect_offset{i}*scale); ylabel(muscleR_name{i});
    plot(base_Param1, muscleR_sect_offset{i}(base_Param1)*scale, 'b*'); 
    %left
%     baseline_offset = mean(muscleL_sect{i}(static_period_Del_emg(offsetRange))); %this should be enough for offseting
%     muscleL_sect_offset{i} = abs(muscleL_sect{i} - baseline_offset);
%     figure; plot(muscleL_sect_offset{i})
end
saveOneFigure(fig2, strcat('offsettedAndRectifedEMGwithHS', speedStr), path)

f_cutoff = 3; N = 2;
Fs = 1/PERIOD_Del_emg;
Wn = [2*f_cutoff/Fs];
[B,A] = butter(N, Wn,'low');
for i = 1:muscleNum
%     muscleL_sect_offset_filt{i} = filtfilt(B, A, muscleL_sect_offset{i});
    muscleR_sect_offset_filt{i} = filtfilt(B, A, muscleR_sect_offset{i});
end




%interpolate right
param = base_Param1;%FF_emg_R;
range =  mean(diff(param));
fig3 = figure; title('interpolated EMG');
for i = 1:muscleNum
    muscleR_sect_avg{i} = [];
    for j = 1 : length(param)-1
        muscleR_sect_avg{i} =  [muscleR_sect_avg{i} interpGaitCycle(muscleR_sect_offset_filt{i}(param(j):param(j+1),:), range)];
    end
    subplot(4, 2, i); hold on;
    plot(muscleR_sect_avg{i}*scale, 'r', 'linewidth',2); ylabel(strcat(muscleR_name{i}, '(MuV)'));
    muscleR_sect_avg{i} = mean(muscleR_sect_avg{i},2);
    plot(muscleR_sect_avg{i}*scale, 'k','linewidth', 4); 
end
saveOneFigure(fig3, strcat('HSinterplolatedEMG', speedStr), path)

% muscleR_sect_avg_1_50 = muscleR_sect_avg;
% save('muscleR_sect_avg_1_50','muscleR_sect_avg_1_50')






%interpolate left
% param = FF_emg_L;
% range =  mean(diff(param));
% for i = 1:length(muscleL)
%     muscleL_sect_avg{i} = [];
%     for j = 1 : length(param)-1
%         muscleL_sect_avg{i} =  [muscleL_sect_avg{i} interpGaitCycle(muscleL_sect_offset_filt{i}(param(j):param(j+1),:), range)];
%     end 
%     figure; plot(muscleL_sect_avg{i}, 'r', 'linewidth',2); title(muscleL_name{ii});
%     muscleL_sect_avg{i} = mean(muscleL_sect_avg{i},2);
% end

% load muscleR_sect_avg_1_50; load muscleR_sect_avg_1_25; 
% muscleAll{1} = muscleR_sect_avg_1_25;
% muscleAll{2} = muscleR_sect_avg_1_50;
% 
% colorStr = {'b','r'}
% 
% fig4 = figure;
% k = 0; yVal = 50;scale = 1e+6;
% for k = 1:2
%     muscleR_sect_avg = muscleAll{k};
%     for i = 1:muscleNum  
%         %right   
%         tempEMG = muscleR_sect_avg{i};
%         subplot(4,2,i); 
%         plot(tempEMG*scale, 'color', colorStr{k}); hold on; ylabel(muscleR_name{i});
% 
%         %left
%     %     tempEMG = muscleL_sect_avg{i};
%     %     subplot(3,2,i+k+1); 
%     %     plot(tempEMG*scale); hold on; ylabel(muscleL_name{i});
% %         k = k +1;
%     end
% end
% subplot(4,2,1); 
% legend('1.25', '1.50')
% saveOneFigure(fig4, 'HSinterplolatedAvgEMG', path)

%%
% Normalize EMG to max signal
% EMG_norm = EMG_filt(:,m)/EMG_MVCmax;
    close all  
new_left_ab = left_Ab(7000:18400,3);
new_left_ab = interpGaitCycle(new_left_ab, sizeSignal); 

figure; hold on;
plot(EMG_filt*5e+5,'r');
plot(new_left_ab,'b')

[acor,lag] = xcorr(EMG_filt, new_left_ab);
[~,I] = max(abs(acor));
lagDiff = lag(I)

figure; hold on;
plot(EMG_filt(1:end)*5e+5,'r');
plot(new_left_ab(-lagDiff:end),'b')
%%
SECTION = [2480 2550 ]; %5840 5960 6020 6140    6200 6330  6350 6500  
[left_Wb,left_Ab] = getdata(left_Wb,left_Ab,PERIOD,SECTION);
[right_Wb,right_Ab] = getdata(right_Wb,right_Ab,PERIOD,SECTION);

%%
% Process data from the two IMUs simultaneously 
[left_walk_info,right_walk_info] = compute_pos_two_imus(left_Wb,left_Ab,right_Wb,right_Ab,PERIOD);

% Perform sperately
% left_walk_info = compute_pos(left_Wb,left_Ab,PERIOD);
% right_walk_info = compute_pos(right_Wb,right_Ab,PERIOD);

%%
% Segment steps
% out_L = [];%[23 26]
% out_R = out_L
% left_strides = stride_segmentation(left_walk_info,PERIOD, out_L);
% right_strides = stride_segmentation(right_walk_info,PERIOD, out_R);  
% 
% % Plot results for left and right foot
% figure;
% subplot(1,2,1);
% plt_ltrl_frwd_strides(left_strides,0);
% subplot(1,2,2);
% plt_ltrl_frwd_strides(right_strides,0);
% matchaxes;
% 
% figure;
% subplot(2,1,1)
% plt_frwd_elev_strides(left_strides,0);
% subplot(2,1,2)
% plt_frwd_elev_strides(right_strides,0);
% matchaxes;
% 
% figure;
% subplot(1,2,1)
% plt_stride_var(left_strides,0);
% subplot(1,2,2)
% plt_stride_var(right_strides,0);
% matchaxes;
% %end of APDM


%%%%%%HS STUFF
% close all
% figure
% plot(right_Ab_Del_sect(:,3),'r'); hold on; plot(left_Ab_Del_sect(:,3),'b');
% %filter the dim3 acc from IMU's
% [Bf, Af] = butter(2, 5*2/(1/PERIOD_Del_imu));
% right_Ab_newDim3_filt = filtfilt(Bf, Af, right_Ab_Del_sect(:,3));
% left_Ab_newDim3_filt = filtfilt(Bf, Af, left_Ab_Del_sect(:,3));
% plot(right_Ab_newDim3_filt,'r.'); hold on; plot(left_Ab_newDim3_filt,'b.');
% [~,locsR] = findpeaks(right_Ab_newDim3_filt,'MinPeakHeight',0);
% [~,locsL] = findpeaks(left_Ab_newDim3_filt,'MinPeakHeight',0);

% %**************
% [~,posPeaks] = findpeaks(diff_amp_A,'MinPeakHeight', 0.5);
% [~,negPeaks] = findpeaks(-diff_amp_A,'MinPeakHeight', 0.5);
% plot(posPeaks, diff_amp_A(posPeaks)*tempScale,'k*');
% plot(negPeaks, diff_amp_A(negPeaks)*tempScale,'k*');
% 
% %**trying to do it from seond derivative of acc amplitude******************
% %the negative peaks of the second derivate of foot acc amplitude corresponds to the HS's and To's
% [~,peak_diff2_right_Ab] = findpeaks(-diff2_amp_A_filt,'MinPeakHeight', 0.1);
% tempPeak_diff = diff(peak_diff2_right_Ab);
% diff_peak_diff2_right_Ab = tempPeak_diff(5:end-5);%eliminate the first and last 5
% 
% locsR = peak_diff2_right_Ab(find(diff_peak_diff2_right_Ab > mean(diff_peak_diff2_right_Ab)));
% locsR_TO = peak_diff2_right_Ab(find(diff_peak_diff2_right_Ab < mean(diff_peak_diff2_right_Ab)));
% % figure
% tempScale = 100
% plot(diff_amp_A*tempScale,'m');hold on
% plot(diff2_amp_A_filt*tempScale,'r');hold on
% plot(locsR, RGRF_sect_interPlt(locsR,3),'b*', 'markersize', 10);
% plot(peak_diff2_right_Ab, diff2_amp_A_filt(peak_diff2_right_Ab)*tempScale,'b*', 'markersize', 10);
% % plot(peak_diff2_right_Ab, zeros(1,length(peak_diff2_right_Ab)),'b*');
% % plot(zc_diff_amp_A, zeros(1,length(zc_diff_amp_A)),'g*');
% % zc_diff2_amp_A_filt = findZeroCrossings(diff2_amp_A_filt);
% % plot(peak_diff2_right_Ab, diff2_amp_A_filt(peak_diff2_right_Ab),'b*');
% % diff_peak_diff2_right_Ab = tempPeak_diff;
% %***************************************************