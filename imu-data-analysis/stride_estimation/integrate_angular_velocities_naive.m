function quaternion_orientations ... % Matrix of Doubles (n_frames x 4)
  = integrate_angular_velocities_naive( ...
      angular_velocities ... % Matrix of Doubles (n_frames x 3)
      )
  % Integrate IMU angular velocities.

  % Unpack arguments
  n_frames = size(angular_velocities, 1);

  % Initial state
  orientation = [1 0 0 0]; % quaterion

  % Allocate outputs
  quaternion_orientations = zeros(n_frames, 4);

  for frame_no = 1:n_frames
    orientation = qua_est(angular_velocities(frame_no, :), orientation);
    quaternion_orientations(frame_no, :) = orientation;
  end
end
