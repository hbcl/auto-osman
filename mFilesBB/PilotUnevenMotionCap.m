
close all; clear all;
restoredefaultpath
addpath('D:\spare\stochasticTerrainUnevenWalking\somedata\pilotDarian2\mocap')
targetDirect = 'D:\spare\stochasticTerrainUnevenWalking\somedata\pilotDarian2';
%First we read in the motion capture data
cm = 10;
stepOnFoamTimes = [];
str = 'UNEVEN2'%'UnD'%'UNEVEN2'%'levelEnd'
initLoop = 2
MPH = -30%-30%-20%-30%-25
MPD = 220%200;
YTickLabelVal = 0:5:60;
thresholdOnHS = 7.5%10;%7.5%12.5%to find the rist HS on uneven terrain
thresholdElimHS = 90;%90;%109;%95;%109%90 %this is to eliminate HS's that are too close to each other



ind = 1;
eliminate1stHS = zeros(1,12); 

%'UNEVEN2' 2
eliminate1stHS(12) = 1

eliminateHSFlag = 1





for i = initLoop:2:12;%1:5%1:2:12%2:2:12
    
    uw = dlmread(strcat(str, num2str(i),'.trc'),'\t',6,0);

    %Isolating for when heelstrikes appear
    iso = find(uw(:,22)>0 & uw(:,25)>0);
    uw_iso = uw(iso(:),:);
    %Converting to centimeters and creating variables  
    uwcm = uw_iso/cm;

    LCalcX = uwcm(:,21);
    LCalcY = uwcm(:,22);
    LCalcZ = uwcm(:,23);
    RCalcX = uwcm(:,24);
    RCalcY = uwcm(:,25);
    RCalcZ = uwcm(:,26);
    time = uw_iso(:,2);

    %correcting for calcaneus marker height
%     H = repmat(1.5,length(uwcm),1);

    HLCalcZ = LCalcZ;%-H;
    HRCalcZ = RCalcZ;%-H;


    % Position Data Plot
    fig1 = figure; hold on;
    plot(time,HRCalcZ,'r');plot(time,HLCalcZ,'b'); ylabel('Calc Z Position');
    
    
    %flipping in Z direction and finding peaks to find heel strikes
    [Lpks,L_HS] = findpeaks(HLCalcZ*-1,'MinPeakDistance',MPD, 'MinPeakHeight', MPH);
    [Rpks,R_HS] = findpeaks(HRCalcZ*-1,'MinPeakDistance',MPD, 'MinPeakHeight', MPH);
    
    plot(time(R_HS), HRCalcZ(R_HS),'k*'); plot(time(L_HS), HLCalcZ(L_HS),'k*');
    set(gca, 'YTick', YTickLabelVal,'YTickLabel', YTickLabelVal); box off; 
    HS = sort([R_HS; L_HS]);
        
    if eliminateHSFlag        
        elimInd = find(diff(HS) < thresholdElimHS);   
        if ~isempty(elimInd)
            elimHS = HS(elimInd);
            %eliminate HS
            HS(elimInd) = [];
            %elininate the same thing from L_HS or R_HS
            tempR = []; tempL = [];
            for zz = 1:length(elimHS)
                tempR = [tempR find(elimHS(zz) == R_HS)];
                tempL = [tempL  find(elimHS(zz) == L_HS)];
            end
            if ~isempty(tempR)
                R_HS(tempR) = [];
            end
            if ~isempty(tempL)
                L_HS(tempL) = [];
            end
        end
    end
       
    if eliminate1stHS(i) 
        if R_HS(1) > L_HS(1)       
            L_HS = L_HS(2:end);
        else
            R_HS = R_HS(2:end);          
        end

        HS = sort([R_HS; L_HS]);  
    end

    %Finding Forward direction position data of heel strikes
    Lstep = LCalcX(L_HS);
    Rstep = RCalcX(R_HS);
    
    %sort step locations in time******************************************
%     steplengths = diff(sort([Rstep;Lstep])) %<-------this is not true
%     because test subject walks back and forth so the relative values of
%     locations to the origin change sign
    stepLocs = [];
    tempInd = min(length(Rstep), length(Lstep));
    for j = 1 : tempInd
        if R_HS(1) < L_HS(1)%<--compare only the first
            stepLocs  = [stepLocs Rstep(j) Lstep(j)];
        else
            stepLocs  = [stepLocs  Lstep(j) Rstep(j)];
        end
    end
    %add the last step ASSUME only one step Left
    if length(R_HS) > tempInd
        stepLocs = [stepLocs  Rstep(end)];
    elseif length(L_HS) > tempInd
        stepLocs  = [stepLocs  Lstep(end)];
    else
        %
    end
    steplengths = abs(diff(stepLocs));
    %*********************************************************************
    
%     [maxStepH_R, maxStepHind_R] = max(RCalcZ(R_HS));
%     [maxStepH_L, maxStepHind_L] = max(LCalcZ(L_HS));
%     if maxStepH_R > maxStepH_L
%        stepOnFoamInd =  R_HS(maxStepHind_R)
%        stepOnFoamHS = find(HS == R_HS(maxStepHind_R));
%     else
%        stepOnFoamInd =  L_HS(maxStepHind_L);
%        stepOnFoamHS = find(HS == L_HS(maxStepHind_L));
%     end
    
    %fin on foam HS
    [indR] = find(RCalcZ(R_HS)> thresholdOnHS);
    [indL] = find(LCalcZ(L_HS)> thresholdOnHS);
    if ~isempty(indR) && isempty(indL)%indR(1) > indL(1)
       stepOnFoamInd =  R_HS(indR(1))
       stepOnFoamHS = find(HS == R_HS(indR(1)));
    elseif ~isempty(indL) && isempty(indR)
       stepOnFoamInd =  L_HS(indL(1));
       stepOnFoamHS = find(HS == L_HS(indL(1)));
    elseif ~isempty(indR) && ~isempty(indL)
        if R_HS(indR(1)) < L_HS(indL(1))
             stepOnFoamInd =  R_HS(indR(1))
             stepOnFoamHS = find(HS == R_HS(indR(1)));
        else
             stepOnFoamInd =  L_HS(indL(1));
             stepOnFoamHS = find(HS == L_HS(indL(1)));
        end
    else
        error('here')
    end
    plot([time(HS(stepOnFoamHS)) time(HS(stepOnFoamHS))], ylim, 'g--');

    %Time instances of heel strikes
    Tsteps = sort([time(R_HS);time(L_HS)]);
    stepTime = diff(Tsteps);
    speed = steplengths'./stepTime;

    strideLen = []; strideTime = [];
    for k = 1:length(steplengths)-1  
        strideLen = [strideLen steplengths(k)+steplengths(k+1)];
        strideTime = [strideTime stepTime(k)+stepTime(k+1)];
        if (k+1) == stepOnFoamHS
            strideOnFoam = k+1;
        end
    end
    speedStride = strideLen./strideTime;

%     subplot(4,1,2); plot(speed,'r'); hold on; plot(speed, 'r*');title('speed: step len/time'); hold on; plot([stepOnFoamHS stepOnFoamHS], ylim, 'k--');
%     subplot(4,1,3); plot(speedStride,'r'); hold on; plot(speedStride, 'r*');title('speed Stride')
%     subplot(4,1,4); plot(steplengths,'k'); hold on; plot(steplengths, 'k*');title('step len'); hold on; plot([stepOnFoamHS stepOnFoamHS], ylim,'k--');
%     
    %store
    stepTimeAll{ind} = stepTime;
    speedAll{ind} = speed;
    steplenAll{ind} = steplengths;
    strideLenAll{ind} = strideLen;
    strideTimeAll{ind} = strideTime; 
    time_all{ind} = time;
    stepOnFoamTimes = [stepOnFoamTimes time(stepOnFoamInd)]; 
    stepOnFoamIndAll(ind) = stepOnFoamInd;
    stepOnFoamHSAll(ind) = stepOnFoamHS;
%     strideOnFoamAll(ind) = strideOnFoam;

    close(fig1)
    fig2 = figure; hold on;
    plot(time,HRCalcZ,'r');plot(time,HLCalcZ,'b'); ylabel('Calc Z Position');
    plot(time(R_HS), HRCalcZ(R_HS),'k*'); plot(time(L_HS), HLCalcZ(L_HS),'k*');
    set(gca, 'YTick', YTickLabelVal,'YTickLabel', YTickLabelVal); box off; 
    plot([time(HS(stepOnFoamHS)) time(HS(stepOnFoamHS))], ylim, 'g--');
    
    ind = ind + 1;
   
    clear R_HS L_HS Tsteps stepTime speed steplengths Lstep Rstep strideLen strideTime uw time stepOnFoamInd stepLocs stepOnFoamHS strideOnFoam
end


%shift first 
shift_HS = max(stepOnFoamHSAll) - stepOnFoamHSAll;
steplenAll_shift = []; speedAll_shift = []; appendLen = [];
for i = 1:length(shift_HS)
    steplenAll_shift{i} = [ones(shift_HS(i),1)*steplenAll{i}(1); steplenAll{i}'];    
    speedAll_shift{i} =   [ones(shift_HS(i),1)*speedAll{i}(1); speedAll{i}]; 
    
    appendLen = [appendLen  length(steplenAll_shift{i})];
end
%%
colorStr = {'r', 'b', 'k' ,'g', 'm' ,'c'}
% close all
%now append
appendLen2 = max(appendLen) - appendLen;
steplenAll_cum = []; speedAll_cum = [];
stepOnFoamHS_final = max(stepOnFoamHSAll);
% figure; hold on; ylabel('step Len cm'); xlabel('speed')
MS = 15
stepsToPlot = max(appendLen); 

figStepLenVsSpeed = figure;hold on
figStepLenAndSpeed = figure;

for i = 1:length(appendLen)
    figure(figStepLenAndSpeed); 
    steplenAll_cum = [steplenAll_cum  [steplenAll_shift{i}; steplenAll_shift{i}(end)*ones(appendLen2(i),1)]];    
    speedAll_cum =   [speedAll_cum    [speedAll_shift{i};   speedAll_shift{i}(end)*ones(appendLen2(i),1)]]; 
    subplot(1,2,1); hold on
    plot(speedAll_cum(:,i), 'color', colorStr{i}); hold on; plot(speedAll_cum(:,i), '.', 'color', colorStr{i}); 
    plot([max(stepOnFoamHSAll) max(stepOnFoamHSAll)], ylim, 'k--');
    ylabel('speed')
    subplot(1,2,2) ; hold on
    plot(steplenAll_cum(:,i), 'color', colorStr{i}); hold on;
    plot(steplenAll_cum(:,i), '.', 'color', colorStr{i}); plot([max(stepOnFoamHSAll) max(stepOnFoamHSAll)], ylim, 'k--');
    plot([max(stepOnFoamHSAll) max(stepOnFoamHSAll)], ylim, 'k--')
    ylabel('step len')
    figure(figStepLenVsSpeed); 
    plot(speedAll_cum(:,i),steplenAll_cum(:,i), '--', 'color', colorStr{i}); hold on;
     
     for k = 1:stepsToPlot
         if k == stepOnFoamHS_final-2
             text(speedAll_cum(k,i),steplenAll_cum(k,i), 'BF2')
         elseif k == stepOnFoamHS_final-1
             text(speedAll_cum(k,i),steplenAll_cum(k,i), 'BF')
         elseif k == stepOnFoamHS_final
             text(speedAll_cum(k,i),steplenAll_cum(k,i), 'ON')
         elseif k == stepOnFoamHS_final+1
             text(speedAll_cum(k,i),steplenAll_cum(k,i), 'AF')
         elseif k == stepOnFoamHS_final+2
             text(speedAll_cum(k,i),steplenAll_cum(k,i), 'AF2')
         end
     end
     ylabel('step len'); xlabel('speed')
%      plot(speedAll_cum(stepOnFoamHS_final-1,i),steplenAll_cum(stepOnFoamHS_final-1,i), '.', 'color', colorStr{1}, 'markersize', MS); 
%      plot(speedAll_cum(stepOnFoamHS_final,i),steplenAll_cum(stepOnFoamHS_final,i), '.', 'color', colorStr{2}, 'markersize', MS); 
%      plot(speedAll_cum(stepOnFoamHS_final+1,i),steplenAll_cum(stepOnFoamHS_final+1,i), '.', 'color', colorStr{3}, 'markersize', MS); 
%      plot(speedAll_cum(stepOnFoamHS_final+2,i),steplenAll_cum(stepOnFoamHS_final+2,i), '.', 'color', colorStr{4}, 'markersize', MS); 
%      plot(speedAll_cum(stepOnFoamHS_final+3,i),steplenAll_cum(stepOnFoamHS_final+3,i), '.', 'color', colorStr{5}, 'markersize', MS); 
%      plot(speedAll_cum(stepOnFoamHS_final+4,i),steplenAll_cum(stepOnFoamHS_final+4,i), '.', 'color', colorStr{6}, 'markersize', MS); 
%      plot(speedAll_cum(stepOnFoamHS_final+5,i),steplenAll_cum(stepOnFoamHS_final+5,i), '.', 'color', colorStr{1}, 'markersize', MS);
end

saveOneFigure(figStepLenAndSpeed, strcat(str, '_', num2str(initLoop), 'SteplenNSpeed_mocap'), targetDirect)
saveOneFigure(figStepLenVsSpeed, strcat(str, '_', num2str(initLoop), 'SteplenVsSpeed_mocap'), targetDirect)


% hold on
% figure
% plot(Tsteps,[Rstep Lstep]);
% hold on
% plot(Tsteps(2:end,:),steplengths);
