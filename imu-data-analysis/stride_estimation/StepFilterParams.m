classdef StepFilterParams
  % Parameters for filtering out outlier steps.

  % Note: The program is currently a bit messy as filtering is performed in
  % three stages: (1) filtering out steps by their duration before computing step
  % parameters, (2) filtering out steps in user-defined sections after
  % computing step parameters, and (3) filtering out steps by their computed
  % parameter values.

  properties
    % Filter out steps with duration exceeding `max_step_duration`. This filter
    % is performed before the computation of step parameters.
    max_step_duration; % Double, seconds

    % Eliminate steps that lie (at least partially) within user-specified sections.
    % This filter is performed after the computation of step parameters.
    exclude_sections; % Matrix of Doubles (nOutlierSections x 2), seconds

    % Whether or not to perform step filtering based on the computed step
    % parameters.
    do_post_filtering; % Boolean

    % Eliminate steps with forward displacement greater than
    % `post_filter_max_step_length`. Useful for eliminating mis-detected steps.
    post_filter_max_step_length; % Double, meters

    % Eliminate steps with forward displacement greater than
    % `post_filter_min_step_length`. Useful for eliminating mis-detected steps.
    post_filter_min_step_length; % Double, meters

    % Eliminate steps with "extreme" forward, lateral, or vertical
    % displacement, that is, displacements greater than
    % `post_filter_max_stdevs` standard deviations away from the respective
    % median. Quirk: The median lateral displacement is assumed to be zero.
    post_filter_max_stdevs; % Double, Unitless

    % Whether or not to eliminate steps with "extreme" vertical displacement.
    post_filter_do_filter_elevation; % Boolean
  end

  methods
    % Default values
    function obj = StepFilterParams(obj)
      obj.max_step_duration = 2.0;
      obj.exclude_sections = [];
      obj.do_post_filtering = false;
      obj.post_filter_max_step_length = 1.8;
      obj.post_filter_min_step_length = 0.5;
      obj.post_filter_max_stdevs = 2.0;
      obj.post_filter_do_filter_elevation = false;
    end
  end
end
