function [EMG_maxtorque, torque_max, pulseloc_comp, torque_maxloc_biodex] = NBA_EMGextensionProcessing(filename,trials,num_muscles,window_time,lowpass_cutoff,EMG_MVCmax,joint)

% This function calculates the maximum EMG signal for the NBA flexion or
% dorsiflexion trials at either the knee or ankle.

% Written by Colin Firminger, Winter 2017

% Inputs: filename = complete csv filename (including directory)
%         trials = trial numbers to be processed
%         num_muscles = number of muscles to find max EMG
%         window_time = width of moving average filter in milliseconds
%         lowpass_cutoff = cutoff frequency for lowpass filter
%         EMG_MVCmax = maximum EMG for each muscle
%         joint = 'ankle' or 'knee'
%
% Outputs: EMG_MVCmaxtorque = normalized EMG signal at timepoint of maximum torque
%          torque_max = maximum torque (Nm)
%          relative to pulse (positive = # of frames after pulse, negative = # of frames before pulse)
%          pulseloc_biodex = location of sync pulse on biodex (not ultrasound)

for t = 1:6
    clear maxlag
    
    disp(['Processing extension trial ' num2str(t)]);
    
    % Set up filename
    file = [filename '0' num2str(trials(t)) '.csv'];
    
    % Obtain sampling rate from the file
    file_Fs = fopen(file);
    Fs = textscan(file_Fs,'%s %s %s %s %f');
    Fs = cell2mat(Fs(1,5));
    Fs = Fs(1)/6;
    fclose(file_Fs);
    
    % Read csv file
    data = csvread(file,4,0); % for subject 10 and 11: reads in 7 columns 1(time), 2(torque), 3(angle), 4(VL), 5(VM), 6(BF), 7(sync)
    
    % Extract Torque, Angle, EMG, and Sync voltage
    torque = data(:,1)*144.71774; %conversion factor from V to Nm
    angle = data(:,2);
    EMG = data(:,3:5); %Order: VL, VM, BF
    sync = data(:,6);

    % Remove offset caused by weight of limb in biodex
    offset = mean(torque(1:1000));
    torque = torque - offset;
    
    % Filter torque data
    torque_filt = filterdata1(torque,Fs,0,5,4,0);
    
    % Finding maximum torque during the trial
    if strcmp(joint,'ankle') == 1
        [torque_max(t),torque_maxloc_biodex(t)] = min(torque_filt);
    elseif strcmp(joint,'knee') == 1
        [torque_max(t),torque_maxloc_biodex(t)] = max(torque_filt);
    end
    
    % Make sync pulse positive and find location of pulse
    sync = abs(sync);
    pulseloc = find(sync>0.02,1);
    if isempty(pulseloc)
        pulseloc_comp(t) = 0;
        display(['no sync pulse for trial ' num2str(t)]) 
    else
        pulseloc_comp(t) = pulseloc(1)-1;
    end
    
    % Convert max torque indices to ultrasound frame rate (78 Hz)
    torque_diffloc(t) = torque_maxloc_biodex(t) - pulseloc_comp(t);
    
    for m = 1:num_muscles
        % Remove baseline offset
        baseline_offset = mean(EMG(1:1000,:));
        EMG(:,m) = EMG(:,m) - baseline_offset(m);
        
        % Making entire signal positive
        EMG = abs(EMG);
        
        % Calculating the moving average and filtering data
        window_length = round(window_time/1000*Fs);
        EMG_mean(:,m) = movmean(EMG(:,m),[window_length/2 window_length/2]);
%         EMG_mean(:,m) = movingmean(EMG(:,m),window_length); %For Matlab versions older than 2016
        EMG_filt(:,m) = filterdata1(EMG_mean(:,m),Fs,0,lowpass_cutoff,4,0);
        
        % Normalize EMG to max signal
        EMG_norm(:,m) = EMG_filt(:,m)/EMG_MVCmax(m);
        
        % Cross correlate the torque and EMG to find electromechanical delay
        [cor,lag] = xcorr(torque_filt,EMG_norm(:,m));
        
        if m <= 2 % Find the lag for the two protagonist muscles (VL/VM)
            if strcmp(joint,'ankle') == 1
                [~,maxoffset_ind] = min(cor);  %min for everyone but p03
            elseif strcmp(joint,'knee') == 1
                [~,maxoffset_ind] = max(cor);  %max for everyone but p03
            end
            maxlag(m) = lag(maxoffset_ind); 
        else % Apply the average lag from the protagonist muscles to the antagonist muscle (BF)
            maxlag(m) = round(mean(maxlag));
        end
        
        % Shift EMG by max lag
        temp = vertcat(zeros(maxlag(1),1),EMG_norm(:,m));
        temp = temp(1:length(EMG_norm(:,m)));
        EMG_shift(:,m) = temp;
    end
    
    % Determine EMG at max torque
    EMG_maxtorque(t,:) = EMG_shift(torque_maxloc_biodex(t),:);
    
    % Plotting
    plot(EMG_shift)
    hold on
    plot(torque_filt/torque_max(t))
    plot([torque_maxloc_biodex(t) torque_maxloc_biodex(t)],[0 1],'-r')
    title(['Trial ' num2str(trials(t))])
    legend('VL/Gastroc','VM/Soleus','BF/TA','Normalized Torque')
    hold off
    
end