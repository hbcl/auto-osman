function analyze_choice(files, imu, sections, imuAnalysisParameters)
    title = 'Analyze Walking Data'
    one_foot_button_label = 'Analyze 1 Foot';
    two_feet_button_label = 'Analyze 2 Feet';

    window_width = 400;
    window_height = 300;
    screen = get(0, 'ScreenSize');
    position = [((screen(3) - window_width)/2) ((screen(4) - window_height)/2) window_width window_height];
    margin = 100;

    d = dialog('Visible', 'on',     ...
                'Name', title,      ...
                'Position', position);

    % Button for selecting to analyze 1 imu
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', one_foot_button_label, ...
        'Position', [margin, 185, 200, 50] , ...
        'Callback', @one_foot_Callback)

    % Button for selecting to analyze 2 imu
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', two_feet_button_label, ...
        'Position', [margin, 90, 200, 50] , ...
        'Callback', @two_feet_Callback)

    
    % Cancel
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'Cancel', ...
        'Position', [280, 30, 70, 30] , ...
        'Callback', @cancel_Callback);

    
    function cancel_Callback(~,~)
        delete(d)
    end
        

    function one_foot_Callback(~,~)
        analyze_one_foot(files, imu, sections, imuAnalysisParameters);
        delete(d);
    end

    function two_feet_Callback(~,~)
        analyze_two_feet(files, imu, sections, imuAnalysisParameters);
        delete(d);
    end
end
