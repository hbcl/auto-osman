%% OSMAN DARICI 2018
clear ; close all; clc; beep off;
% restoredefaultpath;
firstRunFlag = true
if firstRunFlag
    % addpath('C:\Users\Big Boy Bail Bonds\Desktop\spare\HBCL\yourResearch\stride_estimation');
    addpath('C:\Users\adkillic\Documents\MATLAB\stride_estimation');
    % 
    name = 'joel'
    type = 'longUneven'
    targetDirect = strcat('C:\Users\adkillic\Documents\MATLAB\', type);
    % topdir = pwd;
    % tempFileIndex = strfind(topdir,'SteppingOnFoamImu');
    % targetDirect1 = strcat(topdir(1:tempFileIndex(1)-1),'SteppingOnFoamImu\v2\v3_ledStrip\',name,'_ledStrip');
    % if isempty(ls(targetDirect1))
    %     mkdir(targetDirect1);
    % end
    % addpath(genpath(targetDirect1));
    % 
    % targetDirect = strcat(topdir(1:tempFileIndex(1)-1),'SteppingOnFoamImu\v2\v3_ledStrip\',name,'_ledStrip\analyzedData');
    % if isempty(ls(targetDirect))
    %     mkdir(targetDirect);
    % end
    addpath(targetDirect);
end


%%
%RUN THIS PART JUST ONCE AND COMMENT OUT, HERE SAMPLE RATE (PERIOD) IS
%ASSUMED TO BE CONSTANT PERIOD = 0.0078
% % Syncronize and save
% % [LeftWb, LeftAb, RightWb, RightAb, PERIOD, ~, ~, static_period] = sync_New_apdm('20180703-110839_pyramidPilot_sensor_1369_label_Left.h5',...
% %     '20180703-110840_pyramidPilot_sensor_1378_label_Right.h5');
% waistImuFlag = true;
% L_FILE = '20180718-100126_samAlexJoelPyramidLongUneven_sensor_1324_label_Left_Foot.h5';
% R_FILE = '20180718-100126_samAlexJoelPyramidLongUneven_sensor_1316_label_Right_Foot.h5';
% H_FILE = '20180718-100126_samAlexJoelPyramidLongUneven_sensor_1369_label_Hand.h5';
% W_FILE = '20180718-100126_samAlexJoelPyramidLongUneven_sensor_1326_label_Waist.h5';
% [LeftWb, LeftAb, RightWb, RightAb, HandWb, HandAb, WaistWb, WaistAb, ~, PERIOD] = sync_New_apdmWithHandImu(L_FILE, R_FILE, H_FILE, W_FILE, waistImuFlag); 

% % sam
% % pyramid: 1290 2500
% % long uneven 2500 3745
% % alex 
% % pyramid: 4730 5720
% % long uneven 5700 6920
% % joel
% % pyramid: 7690 8749
% % long uneven8744 9927
% % 
% 
% cutTime1 =  8744; cutTime2 = 9940
% [LeftWb, LeftAb, RightWb, RightAb, HandWb, HandAb, WaistWb, WaistAb] =...
%     seperateRawData(LeftWb, LeftAb, RightWb, RightAb, HandWb, HandAb, WaistWb, WaistAb, cutTime1, cutTime2, PERIOD);
% % 
% clear data;
% data.LeftWb = LeftWb;
% data.LeftAb = LeftAb;
% data.RightWb = RightWb;
% data.RightAb = RightAb;
% data.HandWb = HandWb;
% data.HandAb = HandAb;
% data.WaistWb = WaistWb;
% data.WaistAb = WaistAb;
% 
% 
% currentFolder = cd;
% cd(targetDirect);
% save(strcat(name,'_Raw', type), 'data');
% cd(currentFolder)
%%
load PERIOD;
handImuFlag = true;
load(strcat(name,'_Raw', type), 'data');

realTimeTemp = (1:size(data.LeftWb,1))*PERIOD;
figure; plot(realTimeTemp,data.LeftWb)

LeftWb = data.LeftWb;
LeftAb = data.LeftAb;
RightWb = data.RightWb;
RightAb = data.RightAb;
HandWb = data.HandWb;
HandAb = data.HandAb;
WaistWb = data.WaistWb;
WaistAb = data.WaistAb;
clear data

%%
try
%     load (strcat('imuData',name), 'imuData');
    load (strcat(name,'_', type, 'Segmented'));
    who;
catch
    % FILL MANUALLY*************************************
    %     test subject specific parameters***********FILL IT JUST ONCE**
    data.name = name;
    data.date = '18_07_2018';                
    data.approxTrialDur = 15;  
    data.approxTrialSampleNum =  data.approxTrialDur/PERIOD; %samples

    MAX_TRIAL_NUM = 100;
    
    grNum_val = 5;%fill the gorup number
    %end outliers
    for i = 1:grNum_val
        [data.trialEndOutlier{i}(1:MAX_TRIAL_NUM)] =  deal(3/PERIOD); %samples
        [data.trialBeginOutlier{i}(1:MAX_TRIAL_NUM)] =  deal(19/PERIOD); %samples
    end
    
%      [data.trialEndOutlier{group Number}(overall trail number)]<------
    
%     [data.trialEndOutlier{1}(1)] =  deal(3/PERIOD);
%     [data.trialEndOutlier{1}(4)] =  deal(9/PERIOD);
% %   
%     [data.trialEndOutlier{2}(6)] =  deal(4/PERIOD);
%     [data.trialEndOutlier{2}(7)] =  deal(3/PERIOD);
%     [data.trialEndOutlier{2}(8)] =  deal(4/PERIOD);
%     
%     [data.trialEndOutlier{3}(11)] =  deal(4/PERIOD);
%     [data.trialBeginOutlier{2}(8)] =  deal(35/PERIOD);
% %     
% %     [data.trialEndOutlier{4}(15)] =  deal(3/PERIOD);
% %     [data.trialEndOutlier{4}(18)] =  deal(4/PERIOD);
% %     
%     [data.trialEndOutlier{5}(20)] =  deal(6/PERIOD);
%     
    data.groupIndices = [];
    data.savedTrials = [];
    data.badTrials = [];

    %define main sections     
    %if you want to rerun a saved suject enter the trialindex here********
    %     for i = 1:length(data.startGrIndex)
    %         data.startGrIndex{i} = data.startGrIndex{i}-10; 
    %     end
    %     data.startGrIndex = datastartGrIndex;
    %     data.endGrIndex = dataendGrIndex;
    %************************************************************************
   

    %
    data.startGrIndex{1} = [1];     data.endGrIndex{1} =  [240];    %normal 
    data.startGrIndex{2} = [230];     data.endGrIndex{2} =  [570];    %longUneven
    data.startGrIndex{3} = [580];     data.endGrIndex{3} =  [690];    %normal
    data.startGrIndex{4} = [690];     data.endGrIndex{4} =  [1060];    %longUneven
    data.startGrIndex{5} = [1060];     data.endGrIndex{5} =  [1185];    %normal
     
    %FILL MANUALLY***********************************************************************


    trialNum = 1;
    for grNum = 1:length(data.startGrIndex)  

        data.groupIndices{grNum} = [];

        for k = 1:length(data.startGrIndex{grNum}) 

            if handImuFlag;
                try
                [imuData.(strcat('gr',num2str(grNum))).Hand_Wb{k},  imuData.(strcat('gr',num2str(grNum))).Hand_Ab{k},  ~] = getdata(HandWb, HandAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
                close(gcf);
                end
            end
            [imuData.(strcat('gr',num2str(grNum))).Waist_Wb{k},  imuData.(strcat('gr',num2str(grNum))).Waist_Ab{k},  ~] = getdata(WaistWb, WaistAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
    %         close(gcf);
            [imuData.(strcat('gr',num2str(grNum))).Left_Wb{k},  imuData.(strcat('gr',num2str(grNum))).Left_Ab{k},  ~] = getdata(LeftWb, LeftAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
            close(gcf);
            [imuData.(strcat('gr',num2str(grNum))).Right_Wb{k}, imuData.(strcat('gr',num2str(grNum))).Right_Ab{k},  static_period] = getdata(RightWb, RightAb, PERIOD, [data.startGrIndex{grNum}(k) data.endGrIndex{grNum}(k)]);
            t = (1:size(imuData.(strcat('gr',num2str(grNum))).Left_Wb{k}, 1)) * PERIOD; 

            %find the trial intervals and update the trial index  *************************  
            temp_ylim = ylim;
            subplot(2,1,1); hold on;
            for i = 1:length(static_period)-1
                if (static_period(i+1) - static_period(i)) > data.approxTrialSampleNum
                    trialIndex{trialNum} = [static_period(i) - data.trialBeginOutlier{grNum}(trialNum)    static_period(i+1) - data.trialEndOutlier{grNum}(trialNum)];             
                    trialIndex{trialNum} = trialIndex{trialNum} *  PERIOD; %convert to secs  
                    
                    if  trialIndex{trialNum}(1) < 0
                        trialIndex{trialNum}(1) = PERIOD; %lowest initial time possible
                        keyboard
                    end
                    
                    plot([trialIndex{trialNum}(1) trialIndex{trialNum}(1)], ylim, 'r--', 'linewidth', 2);
                    plot([trialIndex{trialNum}(2) trialIndex{trialNum}(2)], ylim, 'b--', 'linewidth', 2);
                    temp_ylim = ylim;
                    h = text(trialIndex{trialNum}(1) + 2 ,temp_ylim(2)*0.75 ,num2str(trialNum)); set(h,'fontsize',25);
                    data.groupIndices{grNum} = [data.groupIndices{grNum} trialNum];
                    trialNum = trialNum+1;
                end
            end
            data.trialIndex = trialIndex; 
            figname = strcat('figureMainIndex', num2str(grNum), '_', num2str(k),'_', data.name);
            currentFolder = cd;
            cd(targetDirect);   
            saveas(gcf, figname, 'fig')
            cd(currentFolder);      
            %*******************************************************
        end

    end

    currentFolder = cd;
    cd(targetDirect);   
    save(strcat('imuData', '_', type, data.name),'imuData');
    cd(currentFolder);   

    %FILL MANUALLY***********************************************************************
    %adjust this part based on gorup numbers
    %gr1
    grIn = data.groupIndices{1}; 
    [data.trialTypeString{grIn}] =  deal('Normal');

    %gr2
    grIn = data.groupIndices{2}; 
%     [data.trialTypeString{grIn}] =  deal('pyramid');
    [data.trialTypeString{grIn(1):2:grIn(end)}]=  deal('longUnevenA');
    [data.trialTypeString{grIn(1)+1:2:grIn(end)}]=  deal('longUnevenB')
        
    %gr3
    grIn = data.groupIndices{3}; 
    [data.trialTypeString{grIn}] =  deal('Normal');
       
    %gr4
    grIn = data.groupIndices{4}; 
%     [data.trialTypeString{grIn}] =  deal('pyramid');
    [data.trialTypeString{grIn(1):2:grIn(end)}]=  deal('longUnevenA');
    [data.trialTypeString{grIn(1)+1:2:grIn(end)}]=  deal('longUnevenB')
    
    %gr5
    grIn = data.groupIndices{5}; 
    [data.trialTypeString{grIn}] =  deal('Normal');
    
    currentFolder = cd;
    cd(targetDirect);   
    save(strcat(data.name,'_', type,'Segmented'),'data'); 
    cd(currentFolder);
    fprintf('all initial params saved *****\n'); 
end

%***********************************************

%% ANALYZE
%no filter ever
% clear all
load (strcat(data.name,'_', type,'Segmented'),'data')
load (strcat('imuData', '_', type, data.name),'imuData');
handImuFlag = true
load PERIOD


FILTER = 0; OutlierSecL = []; OutlierSecR = OutlierSecL; % outliers are already handled

grNum_val_begin = 1;
grNum_val_end = length(data.groupIndices);  
runSingleTrial = 0;
if runSingleTrial
    % make sure first to save all, you can go to the past trials before
    % saving all, but can not go to future trials, you need to save all first
    grNum_val = 2%4%2;
    singleTrial = 7%28%15;%this the overall trail number
    grNum_val_end = grNum_val;
end 
close all
autoRecordFlag = 0;
for grNum = 5:5%grNum_val_begin:grNum_val_end %<---------fill this manually if you want to go for one group at once     
    for k = 1:length(data.startGrIndex{grNum}) 

        clear Left_Wb Right_Wb Left_Ab Right_Ab
        Left_Wb = imuData.(strcat('gr',num2str(grNum))).Left_Wb{k};
        Right_Wb = imuData.(strcat('gr',num2str(grNum))).Right_Wb{k};
%         Waist_Wb = imuData.(strcat('gr',num2str(grNum))).Waist_Wb{k};
        
        
        Left_Ab = imuData.(strcat('gr',num2str(grNum))).Left_Ab{k};
        Right_Ab = imuData.(strcat('gr',num2str(grNum))).Right_Ab{k};
%         Waist_Ab = imuData.(strcat('gr',num2str(grNum))).Waist_Ab{k};
        
        
        if handImuFlag
            Hand_Wb = imuData.(strcat('gr',num2str(grNum))).Hand_Wb{k};
            Hand_Ab = imuData.(strcat('gr',num2str(grNum))).Hand_Ab{k};
        end
        

%         figname = strcat('figureMainIndex', num2str(grNum), '_', num2str(k), '_', data.name);
%         openfig(figname);

        trialIndex = data.trialIndex;   
        trialNumstart = data.groupIndices{grNum}(1);
        trialNumEnd  = data.groupIndices{grNum}(end);
        
        if runSingleTrial
            trialNumstart =  singleTrial(1);
            trialNumEnd = singleTrial(end);           
        end

        % set the parameters
        for i = trialNumstart : trialNumEnd 
            
            if ~runSingleTrial && ~isempty(find(i ==  data.savedTrials, 1))
                fprintf('trial is already saved, passing to next trial \n');
                continue;
            end

            clear trial;
            trial.trialType = data.trialTypeString(i);
            trial.trialNum = i;

            %get the data
            [trial.left_Wb,  trial.left_Ab] =  getdata(Left_Wb,  Left_Ab, PERIOD, trialIndex{i});
            [trial.right_Wb, trial.right_Ab] = getdata(Right_Wb, Right_Ab, PERIOD, trialIndex{i});
%             [trial.waist_Wb, trial.waist_Ab] = getdata(Waist_Wb, Waist_Ab, PERIOD, trialIndex{i});
            if handImuFlag
                [trial.hand_Wb, trial.hand_Ab] = getdata(Hand_Wb, Hand_Ab, PERIOD, trialIndex{i});
            end
            
            % Process data from the two IMUs on the feet simultaneously 
            [trial.Left_foot,trial.Right_foot] = compute_pos_two_imus(trial.left_Wb, trial.left_Ab, trial.right_Wb, trial.right_Ab, PERIOD);

            % Segment steps
            trial.left_strides = stride_segmentation(trial.Left_foot, PERIOD, FILTER, OutlierSecL);
            trial.right_strides = stride_segmentation(trial.Right_foot, PERIOD, FILTER, OutlierSecR);

            % close all; clc;
            plotOnlyStrideVelswithNormals = 0;
            [trial.rightOnFoamIndex  trial.leftOnFoamIndex trial.rightOnFoamIndex2  trial.leftOnFoamIndex2 figMeanHandle] = plotNanalyseStrides(trial);
            
            axes(figMeanHandle(2));
            if ~runSingleTrial
                text(25, 0.3, strcat('trial ', num2str(i-trialNumstart+1)),'fontsize',25); %this not the overall trail number
            else
                text(25, 0.3, strcat('trial ', num2str(trialNumstart)),'fontsize',25); %this  the overall trail number
            end

            keyboard;
            pause('on');
            if ~autoRecordFlag
                result = input('store the trial  using elev data?','s');
                if ~strcmp(result,'n')                                
                    trial.rightOnFoamIndex = trial.rightOnFoamIndex2;
                    trial.leftOnFoamIndex = trial.leftOnFoamIndex2;

                    tempind = find(i == data.badTrials,1);
                    if ~isempty( tempind)
  
                        data.badTrials( tempind) = [];
                    end                      
                    data = storeTrial(data, trial, i, targetDirect, type);
                    disp('trial stored using elev data')
                else
                     result = input('store the trial  using acc data?','s');
                     if ~strcmp(result,'n')                 
                        tempind = find(i == data.badTrials,1);
                        if ~isempty( tempind)
                            data.badTrials( tempind) = [];
                        end
                        data = storeTrial(data, trial, i, targetDirect, type);
                        disp('trial stored using acc data')
                     else                          
                         result = input('do you want to store it manually?','s');
                         if  ~strcmp(result,'n') 
                             templeg = input(' put it manually \n enter leg (red is right)', 's');
                             tempIndex = input('enter number');        
                             if strcmp(templeg ,'r')
                                 trial.rightOnFoamIndex = tempIndex;
                                 trial.leftOnFoamIndex = [];
                             else
                                 trial.leftOnFoamIndex = tempIndex;
                                 trial.rightOnFoamIndex = [];
                             end
                             
                             tempind = find(i == data.badTrials,1);
                            if ~isempty( tempind)
                                data.badTrials( tempind) = [];
                            end 
                             
                             data = storeTrial(data, trial, i, targetDirect, type);
                             disp('trial data stored manually'); 
                         else
                             trial = [];
                             data.badTrials = [data.badTrials i];
                             data = storeTrial(data, trial, i, targetDirect, type);
                             disp('trial data NOT stored');
                         end
                     end  
                end
            else            
                tempind = find(i == data.badTrials,1);
                if ~isempty( tempind)
                    data.badTrials( tempind) = [];
                end
                data = storeTrial(data, trial, i, targetDirect, type);
                disp('trial data stored aut omatically'); 
            end
% %             keyboard;
            close all; clc;
        end
    end
end
%%

