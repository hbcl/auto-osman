function analyze_one_foot(files, imu, sections, imuAnalysisParameters)
    title = 'Analyze Walking Data One Foot';
    prompt = 'Please choose imu to analyze.';
    window_width = 400;
    window_height = 300;
    screen = get(0, 'ScreenSize');
    position = [((screen(3) - window_width)/2) ((screen(4) - window_height)/2) window_width window_height];
    margin = 100;
    placeholder = {'Left', 'Right', 'Waist', 'Hand'};

    d = dialog('Visible', 'on',     ...
                'Name', title,      ...
                'Position', position);

    uicontrol('Parent', d,          ...
                'Style', 'text',    ...
                'Position', [100 (window_height-100) 200 60],    ...
                'String', prompt);

    chosen_imu = uicontrol('Parent', d,             ...
                            'Style', 'popupmenu',   ...
                            'String', files,  ...
                            'Position', [100 (window_height - 150) 200 60]);
    

     % OK
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'OK', ...
        'Position', [200, 30, 70, 30] , ...
        'Callback', @ok_Callback);

    % Cancel
    uicontrol('Parent', d, 'Style', 'pushbutton', ...
        'String', 'Cancel', ...
        'Position', [280, 30, 70, 30] , ...
        'Callback', @cancel_Callback);

    
    function cancel_Callback(~,~)
        delete(d)
    end

    function ok_Callback(~,~)
        section_choice( ...
            imu, chosen_imu.Value, @analysis_one, sections, ...
            imuAnalysisParameters);
        delete(d);
    end

    function resume()
        uiresume;
    end


end
