% Author: Lauro Ojeda, 2012-2015
% A helper function for uniformly removing a set of indices from each of the
% result arrays.
function [stride, number_of_steps] = cut_step_section(stride, indices, number_of_steps)
  if ~isempty(indices)
    stride.frwd_swing(:, indices) = [];
    stride.ltrl_swing(:, indices) = [];
    stride.frwd(      :, indices) = [];
    stride.ltrl(      :, indices) = [];
    stride.abs_ltrl(  :, indices) = [];
    stride.elev(      :, indices) = [];
    stride.theta(     :, indices) = [];
    stride.start_end( :, indices) = [];

    stride.foot_heading(          indices) = [];
    stride.diff_foot_heading(     indices) = [];
    stride.step_samples(          indices) = [];
    stride.time(                  indices) = [];
    stride.frwd_speed_compensated(indices) = [];
    stride.frwd_speed(            indices) = [];

    new_nember_of_steps = number_of_steps - length(indices);
    disp(sprintf('New number of steps %d out of %d', new_nember_of_steps, number_of_steps));
    number_of_steps = new_nember_of_steps;
  end
end
