%% Imu_SectionParser%% 
% Author: Anthony Killick
% Open data, parse into structures 
%% GUI %%


function Imu_SectionParser
%%%%%%%%%%%%%%%%%%%% BEGIN FUNCTION %%%%%%%%%%%%%%%%%%%%%%%

%% For reading in data from imu
addpath('../read-imu-data/')


screensize = get(groot, 'Screensize');
winsize = screensize .* [ 1, 1, 0.85, 0.85];  % Adjust slightly smaller than screen
f = figure('Visible','off', 'ToolBar', 'none', 'Position', winsize);
centerfig(f);
subplt1 = {};
subplt2 = {};

%% Tabs: plots, project info and Sections Info
tabgrp = uitabgroup('Parent', f ,'Position',[0, 0.2, 1, 0.8], 'Visible', 'off');
infoTabs = uitabgroup('Parent', f, 'Position', [0.2, 0, 0.8, 0.2]);
% Project Tab
projectTab = uitab('Parent', infoTabs, 'Title', 'Project Information');
projectFieldNames = {'Project Name', 'Date', 'Description'};
projFieldHandles = inputFields(projectTab, projectFieldNames);
for i = 1:length(projFieldHandles)
   set(projFieldHandles(i),'Enable', 'on')
end
uicontrol('Parent', projectTab, 'Style', 'pushbutton', ...
        'String', 'Options' , ...
        'Position', [75, 15, 100, 30], ...
        'Callback', {@openOptions_Callback});



%% Tab for current section 
sectionTab = uitab('Parent', infoTabs, 'Title', 'Section Information');
sectionFieldNames = {'Section Name', 'Start Time', 'End Time', 'Notes'};
secFieldHandles = inputFields(sectionTab, sectionFieldNames);
saveSecBtn = uicontrol('Parent', sectionTab, 'Style', 'pushbutton',   ...
                        'Units', 'normalized',                        ...
                        'Position', [0.8, 0.8, 0.1, 0.25],            ...
                        'String', 'Save Changes',                     ...
                        'Callback', @saveSection_callback,            ...
                        'Enable', 'on'                               ...
                        );
delSecBtn  = uicontrol('Parent', sectionTab, 'Style', 'pushbutton',   ...
                        'Units', 'normalized',                        ...
                        'Position', [0.8, 0.6, 0.1, 0.25],            ...
                        'String', 'Delete Section',                   ...
                        'Callback', @deleteSection_callback,          ...
                        'Enable', 'off'                               ...
                        );
editSection = uicontrol('Parent', sectionTab, 'Style', 'pushbutton',  ...
                        'Units', 'normalized',                        ...
                        'String', 'Edit Section',                     ... 
                        'Position',[0.8,  0.4, 0.1, 0.25],            ...
                        'Callback', @editSection_Callback,            ...
                        'Enable', 'off'                               ...
                        ); 
%%  Buttons:
buttonPanel =  uipanel('Parent', f,                   ...
                        'Position', [0, 0, 0.2, 0.2]   ...
                        );
nButton = 1/6; % 1/number of buttons

openImuH5 = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',      ...
                        'String', 'Open from .h5', 'Units', 'normalized',...
                        'Position',[0, 0 * nButton, 1, nButton],         ...
                        'Callback',@openImu_Callback                     ...
                        );

loadMat = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',            ...
                    'String', 'Load from .mat file' , 'Units', 'normalized', ...
                    'Position',[0, 1 * nButton, 1, nButton],                 ...
                    'Callback', @loadMat_Callback                            ...
                    );

selectData = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',        ...
                        'String', 'Select Data', 'Units', 'normalized',     ...
                        'Position',[0, 2 * nButton, 1, nButton],            ...
                        'Callback', @selectData_Callback                    ...
                        );

saveProject = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',       ...
                        'String', 'Save Project', 'Units', 'normalized',    ... 
                        'Position',[0, 3 * nButton, 1, nButton],            ...
                        'Callback', @saveProject_Callback ...
                    );
                    
exportOsman = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',      ...
                        'String', 'Export for Osman', 'Units', 'normalized',...
                        'Position',[0, 4 * nButton, 1, nButton],         ...
                        'Callback',@exportOsman_Callback                 ...
                        );                      


analyze = uicontrol('Parent', buttonPanel, 'Style', 'pushbutton',      ...
                        'String', 'Analyze', 'Units', 'normalized',...
                        'Position',[0, 5 * nButton, 1, nButton],         ...
                        'Callback',@analyze_Callback                 ...
                        );                      

%% Containers for project and section data
projectInfo = struct();
projectInfo.parameters = options([], false);
imu = ImuData; % Make data storage object
sections = []; %struct created on first selection
sectionHandles = {};
numSections = 0; % keeps track of number of sections created to id them in the
                    % cell array. When sections are deleted, this remains unchanged

f.Name = 'Data Parser';
f.Visible = 'on';


%% Handles stored in struct when axes plotted:
handles =struct();  
selectInd = 0; %currently selected section number

%%  Callbacks
function openImu_Callback(~, ~)
   
    [wd, files, plots, selection_complete] = open_Imu(); % dlg to select IMU files
    
    if selection_complete
        % waistImuFlag = true; 
        [imu.Wb, imu.Ab, imu.Mb, imu.StaticSamples, imu.SamplePeriod] = get_imu_data(           ...
                                 files, projectInfo.parameters.('read_imu_params'));
        addpath('../analyse')
        projectInfo.sourceFiles = files;
        projectInfo.sourceDir = wd;
        projectInfo.plots = plots; 
        makeDataPlots(plots); %%Show the imu data
    end
end

function openOptions_Callback(~,~)
    projectInfo.parameters = options(projectInfo.parameters, true);
    disp("Options opened");
end

function analyze_Callback(~, ~)
    analyze_choice(projectInfo.plots, imu, sections, projectInfo.parameters);
end


function makeDataPlots(plots)
    a = zeros(1,length(plots));
    numImus = length(plots);
    wait = waitbar(0.02, 'Plotting data from IMUs.','WindowStyle', 'modal'); %Left Foot Tab
    yRange{1} = [min(imu.Wb{1}(:)), max(imu.Wb{1}(:))]; 
    yRange{2} = [min(imu.Ab{1}(:)), max(imu.Ab{1}(:))];
    for i = 1:numImus
        waitbar(i*0.25, wait); %Right Foot Tab 
        tabs(i) = uitab('Parent', tabgrp, 'Title', plots{i});
        ax = axes('parent', tabs(i));
        h = imu.plotData(ax, i);
        a(2*i-1) = h(1); %store handle of 1st subplot
        a(2*i) = h(2);   %store handle of 2nd subplot
    end
    waitbar(1, wait);
    delete(wait);
    tabgrp.Visible = 'off';
    
    %%New Plotting
    plotPanel = uipanel('Parent', f, 'Position', [0, 0.2, 1, 0.8])
    [subplt1, subplt2] = imu.plotWithSelectData(plotPanel, plots, 1, length(imu.Wb{1}))

    %%Linking the subplot axes synchronizes zooming on the x axis
    linkaxes([subplt1, subplt2], 'x');
    handles.a = a; %[a1, a2, a3, a4];
end

function selectData_Callback(~,~)
    a.Color = [0.4 0.9 1];
    rect = getrect();
    a.Color = [1 1 1];
    x1 = rect(1);
    x2 = x1 + rect(3);
    makeSection(x1,x2);
end

function makeSection(x1,x2)
    numSections = numSections + 1;
    new = size(sections, 2) +1;
    sections(new).id = numSections;
    sections(new).name = strcat('Section ', num2str(numSections));
    sections(new).startTime = x1;
    sections(new).endTime = x2;
    sections(new).notes = 'Enter any notes here';
    sections(new).trials = []; % section editor makes this in to a struct array of trials
    plotSection(new)
    clickSection_Callback(sections(new).id); % make new section the selected section
end

function plotSection(new)
    ax = [subplt1, subplt2];
    x1 = sections(new).startTime; 
    x2 = sections(new).endTime;
    for i = [1,2]
        axes(ax(i))
        hold on;
        y = ylim;
        sectionHandles{new}(i) = fill([x1, x1, x2, x2], ...
                                [y(1), y(2), y(2), y(1)], 'g', ...
                                'EdgeColor', 'none', 'FaceAlpha', 0.2);
        set(gca, 'ylim', y);
        set(sectionHandles{new}(i), 'ButtonDownFcn', ...
                    @(~,~)clickSection_Callback(sections(new).id));
    end
end

function clickSection_Callback(id)
    if selectInd ~= 0 
        for i = 1:length(sectionHandles{selectInd})
            set(sectionHandles{selectInd}(i), 'FaceColor', 'g');
        end
    end
    ind = find([sections.id]==id);
    selectInd = ind;   % alter selectInd at the scope of the gui
    for i = 1:length(sectionHandles{selectInd})
        set(sectionHandles{selectInd}(i), 'FaceColor', 'r');
    end
    % enable editing and put info in fields UI
    for i = 1: length(secFieldHandles)
        set(secFieldHandles(i), 'Enable', 'on');
    end
    set(delSecBtn, 'Enable', 'on');
    set(editSection, 'Enable', 'on');
    loadSectionVals();
end

function saveSection_callback(~,~)
    x1 = str2double(get(secFieldHandles(2),'String'));
    x2 = str2double(get(secFieldHandles(3),'String'));
    sections(selectInd).name = get(secFieldHandles(1), 'String');
    sections(selectInd).startTime = x1;
    sections(selectInd).endTime = x2;
    sections(selectInd).notes = get(secFieldHandles(4), 'String');
    % Display altered time:
    for i = 1: length(handles.a)
        axes(handles.a(i));
        hold on;
        new = get(sectionHandles{selectInd}(i), 'Vertices');
        new(1,1) = x1; new(2,1) = x1; new(3,1) = x2; new(4,1) = x2;
        set(sectionHandles{selectInd}(i), 'Vertices', new);
    end
end

function deleteSection_callback(~,~)
    % delete fill objects and handles for section
    for i = 1: length(sectionHandles{ selectInd })
        delete(sectionHandles{ selectInd }(i))
    end

    % delete section handle
    sectionHandles(selectInd) = [];
    
    % Delete section information
    sections(selectInd) = [];
    
    % set selectInd to 0
    selectInd = 0;
    % turn off text fields
    for i = 1: length(secFieldHandles)
        set(secFieldHandles(i), 'Enable', 'off');
    end
    set(delSecBtn, 'Enable', 'off');
    set(editSection, 'Enable', 'off');
end

function loadSectionVals()
    set(secFieldHandles(1), 'String', sections(selectInd).name);
    set(secFieldHandles(2), 'String', sections(selectInd).startTime);
    set(secFieldHandles(3), 'String', sections(selectInd).endTime);
    set(secFieldHandles(4), 'String', sections(selectInd).notes);
end

function saveProject_Callback(~,~)
    projectInfo.Name = get(projFieldHandles(1),'String');
    projectInfo.Date = get(projFieldHandles(2), 'String');
    projectInfo.Description = get(projFieldHandles(3),'String');
    [fn,pn] = uiputfile(strcat(projectInfo.Name, '.mat'));
    if length(fn) > 1 & length(pn) > 1
        save(strcat(pn,fn), 'sections', 'projectInfo', 'imu');
    end
end

function exportOsman_Callback(~,~)
    export_osman(projectInfo, imu, sections);
    disp('Worked');
end
    
function loadMat_Callback(~,~)
    addpath('../analyse/')
    [matFile, filePath] = uigetfile('*.mat');
    filePath = strcat(filePath, matFile);
    load(filePath);
    makeDataPlots(projectInfo.plots);
    for i = 1: length(sections)
        plotSection(i);
    end
    set(projFieldHandles(1), 'String', projectInfo.Name);
    set(projFieldHandles(2), 'String', projectInfo.Date);
    set(projFieldHandles(3), 'String', projectInfo.Description);
end

function editSection_Callback(~,~)
    x1 = round(sections(selectInd).startTime/imu.SamplePeriod);
    x2 = round(sections(selectInd).endTime/imu.SamplePeriod);
    
    %Save output from trial window to temp in case of emtpy output
    temp_trials = Imu_TrialParser(sections(selectInd).trials, imu, ...
                            projectInfo.plots, x1,x2, projectInfo.parameters.('trial_detection_params'));
    if ~isempty(temp_trials)                        
        sections(selectInd).trials = temp_trials;
    end
end




%%%%%%%%%%%%%%%%%%%%%% END FUNCTION %%%%%%%%%%%%%%%%%%%%%%
end
