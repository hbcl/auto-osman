function [WB, AB, static_period, MB] = load_imu_data(file_name, SECTION, ReadImuParams)

    wait = waitbar(0, 'Loading Data');

    numFiles = length(file_name);

    % Load data
    for i = 1:numFiles

        [Wb, Ab, PERIOD, Mb] = read_apdm_data(file_name{i}, 1);
        [WB{i}, AB{i}, static_temp, MB{i}] = trim_data(Wb, Ab, PERIOD, SECTION{i}, [], Mb, ReadImuParams);

        if(i==1)
            static_period = static_temp;
        end
    
        if(static_temp(end) < static_period(end))
             static_period = static_temp;
        end
    
     
        %Percent complete
        comp = i / numFiles;
        waitbar(comp);
    
        %Set the name of the wait be to the current imu name
        set(gcf, 'Name', file_name{i});
    
    end

    delete(wait)