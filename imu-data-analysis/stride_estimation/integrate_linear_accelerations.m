function [ global_linear_velocities, ... % Matrix of Doubles (n_frames x 3)
           horizontal_linear_velocity_magnitudes, ... % Row vector of Doubles (n_frames)
           positions ... % Matrix of Doubles (n_frames x 3)
          ] ...
  = integrate_linear_accelerations( ...
      sampling_period, ... % Double, the IMU sampling period
      global_linear_accelerations ... % Matrix of Doubles (n_frames x 3)
      )

  global_linear_velocities = ...
    cumsum(global_linear_accelerations) * sampling_period;

  horizontal_linear_velocity_magnitudes = ...
    (sum((global_linear_velocities(:,1:2).^2)')).^.5; % row vector

  positions = cumsum(global_linear_velocities) * sampling_period;
end
