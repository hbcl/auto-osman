
function tempF = plotShiftedElevation(currentData, timeL, fwrdShiftL_All, shiftStrideFwdL, stridesFrwdL, ...
                                                                                       timeR, fwrdShiftR_All, shiftStrideFwdR, stridesFrwdR)

    xlimVal2 = 45%25;
    tempF = figure;
    ystrTemp = 0.1;%0.15;
    for j = 1:length(timeL{1})
        FF_L = find(currentData.Left_foot.FF_walking);
        strideTimeL = linspace(0,timeL{1}(j),length(FF_L(j):FF_L(j+1)));        

        plot(fwrdShiftL_All{1} + shiftStrideFwdL{1}(j) + stridesFrwdL{1}(1:length(strideTimeL),j),...               
        -currentData.left_strides.elev(1:length(strideTimeL),j),'bo'); hold on;    
    
        tempy =  -currentData.left_strides.elev(1:length(strideTimeL),j);
    
        h = text(fwrdShiftL_All{1} + shiftStrideFwdL{1}(j) + stridesFrwdL{1}(length(strideTimeL),j),...
            max(tempy)*1.05, num2str(j)); set(h,'fontsize',15); set(h,'color','b');
    
     end
     for j = 1:length(timeR{1})
        FF_R = find(currentData.Right_foot.FF_walking);
        strideTimeR = linspace(0,timeR{1}(j),length(FF_R(j):FF_R(j+1)));        

        plot(fwrdShiftR_All{1} + shiftStrideFwdR{1}(j) + stridesFrwdR{1}(1:length(strideTimeR),j),...                
        -currentData.right_strides.elev(1:length(strideTimeR),j),'ro'); hold on;   
    
        tempy = -currentData.right_strides.elev(1:length(strideTimeR),j);
    
        h = text(fwrdShiftR_All{1} + shiftStrideFwdR{1}(j) + stridesFrwdR{1}(length(strideTimeR),j),...
                max(tempy)*1.05, num2str(j)); set(h,'fontsize',15); set(h,'color','r');
    
    
     end  
     xlim([0 xlimVal2]);     
     pos1 = 50; pos2 = 50; width1 = 1500; height1 = 1400;          
     drawnow;
%      set(get(handle(gcf),'JavaFrame'),'Maximized',1);
     set (gcf, 'Units', 'normalized', 'Position', [0,0,1,1])
     title(strcat(currentData.trialType, ' trial'),'Fontsize', 17); 