classdef AutoTrialDetectParams
  % Parameters for automatically detecting trials in a section of data

  properties
    % Minimum length a trial can be 
    trial_minimum_length; % seconds

    trial_begin_outlier;

    trial_end_outlier;
  end

  methods
    function obj = AutoTrialDetectParams(obj)
      obj.trial_minimum_length = 14.976;
      obj.trial_begin_outlier = 1.9968;
      obj.trial_end_outlier = 17.9712;
    end
  end
end
