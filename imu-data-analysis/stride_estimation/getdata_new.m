function [W,A,M]  =  getdata_new(W,A,PERIOD,SECTION_SECONDS,BIAS)
% BIAS, is the static time at the beggining of the experiment
% leave BIAS the parameter empty to make the system detect the static time automatically
GRAVITY = 9.80297286843;
	number_of_sections = size(SECTION_SECONDS,1);
	SECTION_SAMPLES = [];
	for(ii=1:number_of_sections)
		SECTION_SAMPLES  =  [SECTION_SAMPLES,(floor(SECTION_SECONDS(ii,1)/PERIOD):floor(SECTION_SECONDS(ii,2)/PERIOD))];
	end;
	W = W(SECTION_SAMPLES,:);
	A = A(SECTION_SAMPLES,:);
	static_period = (1:BIAS/PERIOD);
% Apply gyro static bias compensation
W = W-ones(size(W,1),1)*mean(W(static_period,:));

% Normalizes accelerometer during static time using a 1-G compensation
static_acceleration = sum((A(static_period,:).^2)').^.5;
gravity_measurment = mean(static_acceleration);
A = A*GRAVITY/gravity_measurment;



