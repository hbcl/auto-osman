function [HS TO] = findGaitEventUsingFooTAccelerometer(Right_Acc_Amplitude, Left_Acc_Amplitude, PERIOD, HS_GRAV_COEF)

tempScale = 100;
GRAVITY = 9.80297286843;

foot = {'right', 'left'}
clear HS TO
[Bf, Af] = butter(2, 5*2/(1/PERIOD));

acc.right = Right_Acc_Amplitude;
acc.left =  Left_Acc_Amplitude;

%HS's are the zero corrings of derivateice off acc Amp but not all of them
%so eliminate the bad zc's with checking the acc amplitude
%check the zero corossings of dericative off  and also amplitude of
%Acc

for i = 1:length(foot)
    amp_A = acc.(foot{i});
    
    amp_A_filt = filtfilt(Bf, Af, amp_A);
    

    diff_amp_A = [0 diff(amp_A_filt)];
    zc_diff_amp_A = findZeroCrossings(diff_amp_A);
    tempHS_TO = zc_diff_amp_A(find(amp_A_filt(zc_diff_amp_A) > HS_GRAV_COEF*GRAVITY));
    tempHS_TO = tempHS_TO(4:end-4);

    HS.(foot{i}) = tempHS_TO(diff(tempHS_TO) > mean(diff(tempHS_TO)));
    TO.(foot{i}) = tempHS_TO(diff(tempHS_TO) < mean(diff(tempHS_TO)));
    clear amp_A amp_A_filt diff_amp_A zc_diff_amp_A tempHS_TO
    
%     plot(amp_A_filt*tempScale/10,'g'); 
%     plot(diff_amp_A*tempScale,'m')

end


%begin with RHS and end with RTO
TO.right = TO.right(HS.right(1) < TO.right & (HS.right(end)> TO.right));
HS.left =  HS.left((HS.left > HS.right(1)) & (HS.left < TO.right(end)));
TO.left =  TO.left((HS.right(1) < TO.left)  & (TO.left < TO.right(end)));
HS.right = HS.right(1:end-1);


if ~((length(HS.right) == length(HS.left)) && (length(TO.left) == length(TO.right)) && (length(HS.left) == length(TO.right)))
    error('check the number of gait events')
end


for i = 1:length(TO.right)
    
    if ~((HS.right(i) < TO.left(i)) &&  (TO.left(i) < HS.left(i)) && (HS.left(i) < TO.right(i)))
        i
        error('gait even order not true for step above')
    end
    
end


